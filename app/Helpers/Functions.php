<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Helpers;

use Session;
/**
 * Description of GetLastSurveyedPage
 *
 * @author Kayode
 */
class Functions {

    //put your code here
    public static function getLastSurveyedPage($pageNumber) {
        $routeName = "";
        switch ($pageNumber) {
            case 0:
                $routeName = route('survey.home');
                break;
            case 1:
                $routeName = route('survey.questions');
                break;
            case 2:
                $routeName = route('survey.household');
                break;
            case 3:
                $routeName = route('survey.questions');
                break;
            case 4:
                $routeName = route('survey.questions');
                break;
            case 4:
                $routeName = route('survey.questions');
                break;
            default:
                break;
        }
        return $routeName;
    }

    public static function clearSurveySessions() {

        Session::forget("survey_id");
        Session::forget("survey_page");
        Session::forget("enumeration_area_id");
        Session::forget("page1_response");
    }

    public static function getHighestPermission(array $roles) {
        $highestPermission = -1;
        for ($i = 0; $i < count($roles); $i++) {
            if ($roles[$i] < $highestPermission || $highestPermission < 0) {
                $highestPermission = $roles;
            }
        }
        return $highestPermission;
    }

}
