<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    protected $defaultRoles = ["admin" => 1, "user" => 2, "surveyor" => 3];

    protected function isValidUserRole(\Illuminate\Contracts\Auth\Guard $guard, array $acceptedRole) {
        $roles = $guard->user()->roles();
        $rolesId = $roles->pluck('roles_id')->toArray();
        if (in_array($this->defaultRoles["admin"], $rolesId)) {
            return true;
        }
        
        $commonRoles = array_intersect($rolesId, $acceptedRole);
        if (count($commonRoles) > 0) {
            return true;
        }
        return false;
    }

}
