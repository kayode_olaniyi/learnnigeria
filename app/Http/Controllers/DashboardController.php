<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    //
    public function get() {

        $data = array();

        $data["dashboard_menu"] = "active";
        $data["enumerations"] = \App\Model\EnumerationArea::all()->count();
        $data["volunteers"] = \App\Model\Volunteer::all()->count();

        return view('admin.dashboard', compact('data'));
    }

    public function downloadReport(){
        
    }
}
