<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadReportController extends Controller {

//

    private $downloadPath = "/download/";

    public function __construct() {
        
        $this->middleware('auth');
        
    }

    public function get(Request $request) {
        (new \App\Jobs\DownloadReportJob())->handle();
        \App\Jobs\DownloadReportJob::dispatch()->onConnection('database');

        $reportJobQueue = \App\Model\DownloadReportQueue::create([
                    'state' => $request->state,
                    'local_government' => $request->local_government,
                    'enumeration_area' => $request->ea
        ]);


        return redirect(route('dashboard'))->with('queue_id', $reportJobQueue->id);
    }

    private function getSurveyDetails($survey) {

        $EnumerationArea = \App\Model\EnumerationArea::where("id", $survey->enumeration_areas_id)->first();

        $stateName = $EnumerationArea->localGovernment->state->name;
        $stateCode = $EnumerationArea->localGovernment->state->code;
        $localGovernmentName = $EnumerationArea->localGovernment->name;
        $localGovernmentCode = $EnumerationArea->localGovernment->code;
        $EnumerationAreaName = $EnumerationArea->name;
        $EnumerationAreaCode = $EnumerationArea->code;

        $surveyDetails = array(
            $stateName,
            $stateCode,
            $localGovernmentName,
            $localGovernmentCode,
            $EnumerationAreaName,
            $EnumerationAreaCode
        );
        return $surveyDetails;
    }

    private function getHeaders() {
        return array(
            "Content-type" => "application/zip",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
    }

    private function getSurveys($state, $localGovernment, $EA) {
        $surveys = \App\Model\Survey::where('status', 1)->get();
        $surveysArray = [];
        $surveysArray["householdObservation"] = array();
        $surveysArray["householdInformation"] = array();
        $surveysArray["schoolInfo"] = array();
        $count = 1;
        foreach ($surveys as $survey) {

            $eaObservationResult = $this->getObservationArea($survey);
            $households = $this->getHouseholdObservations($survey);
            $schoolInfo = $this->getSchoolResult($survey);

            $surveysArray["eaObservation"][$count] = $eaObservationResult;
            $surveysArray["householdObservation"] += $households["householdObservations"];
            $surveysArray["householdInformation"] += $households["householdInformations"];
            $surveysArray["schoolInfo"][] = $schoolInfo;

            $count++;
        }



        return $surveysArray;
    }

    private function getQuestions() {

        $surveyDetailsQuestions = ["State Name", "State Code",
            "Local Government Name", "Local Government Code",
            "EA Name", "EA Code"];
        $questions = [];
        $EAObservationQuestions = \App\Model\EAObservationSheetQuestion::orderBy('id')->get()->pluck("question")->toArray();
        $questions["eaObservation"] = array_merge($surveyDetailsQuestions, $EAObservationQuestions);


        $HouseholdObservationQuestion = \App\Model\HouseholdQuestion::all()->pluck("question")->toArray();

        $HouseholdObservationQuestion = array_merge($surveyDetailsQuestions, $HouseholdObservationQuestion);
        $HouseholdInformationQuestion = \App\Model\HouseholdInformationQuestion::orderBy('id')->get()->pluck("question")->toArray();

        $questions["householdInformation"] = array_merge($HouseholdObservationQuestion, $HouseholdInformationQuestion);

        $SchoolQuestion = \App\Model\SchoolSurveyQuestion::orderBy('id')->get()->pluck("question")->toArray();

        $questions["schoolInfo"] = array_merge($surveyDetailsQuestions, $SchoolQuestion);

        return $questions;
    }

    private function getObservations() {
        
    }

    private function getObservationArea($survey) {
        $result = \App\Model\EAObservationResult::where('surveys_id', $survey->id)->get();
        $resultArray = [];
        $count = 0;
        $surveyId = 0;

        foreach ($result as $res) {
            if ($res->answer->id == 10) {
                $resultArray[] = $res->text;
            } else {
                $resultArray[] = $res->answer->answer;
            }
        }
        $surveyDetails = $this->getSurveyDetails($survey);
        /**  $volunteers = $this->getVolunteers($survey);
          $resultArrayCount = count($resultArray);

          foreach ($volunteers as $vol) {
          $a = array($vol);
          $resultArray[$resultArrayCount] = $vol->name;
          $resultArrayCount++;
          }* */
        $resultArray = array_merge($surveyDetails, $resultArray);

        return $resultArray;
    }

    private function getVolunteers($survey) {
        $result = \App\Model\Volunteer::where("surveys_id", $survey->id)->get();
        return $result;
    }

    private function getHouseholds($survey) {
        $households = \App\Model\Household::where("surveys_id", $survey->id)->get();
        dd($households);
    }

    private function getHouseholdObservations($survey) {

        $householdObservation = \App\Model\HouseholdObservation::where("surveys_id", $survey->id)->get();
        //$households = $this->getHouseholds($survey);
        //dd($households);

        $householdInformations = [];
        $surveyDetails = $this->getSurveyDetails($survey);
        $results = [];
        $housholdInformationQuestions = \App\Model\HouseholdInformationQuestion::all()->pluck('id')->toArray();
        $count = 0;
        $householdChildIdCount = 1;

        $mergedHouseholdInformations = [];
        $i = 0;
        foreach ($householdObservation as $observation) {
            $householdInfoResult = $observation->householdInformationResult()->orderBy('child_id')->orderBy('question_id', 'ASC')->get();
            $householdResult = $observation->householdResult()->orderBy('household_questions_id', 'ASC')->get();
            if (count($householdInfoResult) <= 0 || count($householdResult) <= 0) {
                continue;
            }
            $householdObservationCount = 0;
            $householdObservations = [];
            foreach ($householdResult as $hr) {
                if ($hr->answer) {
                    if ($hr->answer->id == 10) {
                        $householdObservations[] = $hr->text;
                    } else {
                        $householdObservations[] = $hr->answer->answer;
                    }
                } else {
                    $householdObservations[] = "";
                }

                $householdObservationCount++;
            }

            $householdInformations[$count] = $this->getHouseholdInformationData($householdInfoResult, $housholdInformationQuestions);
            foreach ($householdInformations[$count] as $children) {
                $mergedHouseholdInformations[] = array_merge($surveyDetails, $householdObservations, $children);
            }

            $count++;
        }
        $results["householdObservations"] = $householdObservations;
        $results["householdInformations"] = $mergedHouseholdInformations;

        return $results;
    }

    private function getHouseholdInformationData($householdInfoResult, $questions) {
        $children = array();
        $childrenCount = 1;
        $filledQuestions = [];
        $childCount = 0;
        $questionAnswered = [];

        foreach ($householdInfoResult as $householdInfo) {
            if ($householdInfo->child_id > $childrenCount) {
                $childCount = 0;
                $childrenCount = $householdInfo->child_id;
                $filledQuestions = [];
            }

            $childData = $this->getChildData($householdInfo);

            if (array_key_exists($childData["question_id"], $filledQuestions)) {
                $count = $filledQuestions[$childData["question_id"]];
                $children[$childrenCount][$count] = $children[$childrenCount][$count] . "," . $childData["answer"];
            } else {
                $children[$childrenCount][$childCount] = $childData["answer"];
                $filledQuestions[$childData["question_id"]] = $childCount;
                $questionAnswered[$childrenCount][$childCount] = $householdInfo->question_id;
            }

            $childCount++;
        }

        for ($i = 1; $i <= $childrenCount; $i++) {

            $missingQuestions = array_diff($questions, $questionAnswered[$i]);

            foreach ($missingQuestions as $mpubQ) {
                array_splice($children[$i], $mpubQ, 0, "");
            }
        }

        return $children;
    }

    private function getChildData($householdInfo) {
        $childAnswer = "";

        if ($householdInfo->answer) {
            if ($householdInfo->answer->id == 10) {
                $childAnswer = $householdInfo->text;
            } elseif ($householdInfo->question->type == 3) {
                $childAnswer = $householdInfo->answer->id;
            } else {
                $childAnswer = $householdInfo->answer->answer;
            }
        } else {
            $childAnswer = "";
        }

        return ["question_id" => $householdInfo->question_id, "answer" => $childAnswer];
    }

    private function getSchoolResult($survey) {
        $surveyDetails = $this->getSurveyDetails($survey);
        $schoolResult = \App\Model\SchoolSurveyResult::where("surveys_id", $survey->id)->get();

        $schoolResultArray = [
            "public" => [],
            "private" => []
        ];
        $alreadyAnsweredQuestion = [
            "public" => [],
            "private" => []
        ];
        $questionsAnswered = [
            "public" => [],
            "private" => []
        ];
        $resultArray = array();
        $count = 0;
        $questions = \App\Model\SchoolSurveyQuestion::all()->pluck('id')->toArray();

        foreach ($schoolResult as $result) {
            $answer = "";
            if ($result->answer) {
                if ($result->answer->id == 10) {
                    $answer = $result->text;
                } else {
                    $answer = $result->answer->answer;
                }
            } else {
                $answer = "";
            }

            $resultArray[$count] = $answer;

            if ($result->school_type == 1) {

                if (array_key_exists($result->question->id, $alreadyAnsweredQuestion["public"])) {
                    $countVal = $alreadyAnsweredQuestion["public"][$result->question->id];
                    $schoolResultArray["public"][$countVal] = $schoolResultArray["public"][$countVal] . ", " . $resultArray[$count];
                } else {

                    $questionsAnswered["public"][] = $result->question->id;
                    $alreadyAnsweredQuestion["public"][$result->question->id] = $count;
                    $schoolResultArray["public"][$count] = $resultArray[$count];
                }
            } elseif ($result->school_type == 2) {
                if (array_key_exists($result->question->id, $alreadyAnsweredQuestion["private"])) {
                    $countVal = $alreadyAnsweredQuestion["private"][$result->question->id];
                    $schoolResultArray["private"][$countVal] = $schoolResultArray["private"][$countVal] . ", " . $resultArray[$count];
                } else {
                    $questionsAnswered["private"][] = $result->question->id;
                    $alreadyAnsweredQuestion["private"][$result->question->id] = $count;
                    $schoolResultArray["private"][$count] = $resultArray[$count];
                }
            }

            $count++;
        }

        $missingPublicQuestions = array_diff($questions, $questionsAnswered["public"]);
        $missingPrivateQuestions = array_diff($questions, $questionsAnswered["private"]);
        foreach ($missingPublicQuestions as $mpubQ) {
            array_splice($schoolResultArray["public"], $mpubQ, 0, "");
        }
        foreach ($missingPrivateQuestions as $mprivQ) {
            array_splice($schoolResultArray["private"], $mprivQ, 0, "");
        }
        $schoolResultArray["public"] = array_merge($surveyDetails, $schoolResultArray["public"]);
        $schoolResultArray["private"] = array_merge($surveyDetails, $schoolResultArray["private"]);

        return $schoolResultArray;
    }

}
