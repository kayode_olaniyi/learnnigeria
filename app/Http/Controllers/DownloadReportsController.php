<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ZipArchive;

class DownloadReportController extends Controller {

//
    private $downloadPath = "/download/";

    public function get(Request $request) {
        $surveys = $this->getSurveys($request->state, $request->local_government, $request->ea);

        $headers = $this->getHeaders();

        $folderPath = "";
        $questions = $this->getQuestions();

        /**
         * Generate EA Observation
         */
        $EAObservationColumn = $questions["eaObservation"];
        $EAFileName = $folderPath . "eaObservation.csv";
        $EAFile = fopen($EAFileName, "w");
        fputcsv($EAFile, $EAObservationColumn);

        foreach ($surveys["eaObservation"] as $information) {

            fputcsv($EAFile, $information);
        }
        fclose($EAFile);

        /**
         * Generate Household Observation
         */
        $HouseholdInformationColumn = $questions["householdInformation"];
        for ($i = 41; $i <= 53; $i++) {
            unset($HouseholdInformationColumn[$i]);
        }

        $HouseholdFileName = $folderPath . "householdInformation.csv";
        $HouseholdFile = fopen($HouseholdFileName, "w");
        fputcsv($HouseholdFile, $HouseholdInformationColumn);
        foreach ($surveys["householdInformation"] as $hoinformation) {
            fputcsv($HouseholdFile, $hoinformation);
        }
        fclose($HouseholdFile);

        /**
         * school details
         */
        $schoolSurveyQuestions = $questions["schoolInfo"];

        /**
         * Public school
         */
        $publicSchoolInfoFilePath = $folderPath . "publicSchoolInfo.csv";
        $publicSchoolInfoFile = fopen($publicSchoolInfoFilePath, "w");
        fputcsv($publicSchoolInfoFile, $schoolSurveyQuestions);
        fputcsv($publicSchoolInfoFile, $surveys["schoolInfo"]["public"]);
        fclose($publicSchoolInfoFile);

        /**
         * Private
         */
        $privateSchoolInfoFilePath = $folderPath . "privateSchoolInfo.csv";
        $privateSchoolInfoFile = fopen($privateSchoolInfoFilePath, "w");
        fputcsv($privateSchoolInfoFile, $schoolSurveyQuestions);
        fputcsv($privateSchoolInfoFile, $surveys["schoolInfo"]["private"]);
        fclose($privateSchoolInfoFile);

        /**
         * Download All Files as ZIP
         */
        $files = array($EAFileName, $HouseholdFileName, $publicSchoolInfoFilePath, $privateSchoolInfoFilePath);
        $zipname = $folderPath . 'file.zip';
        $zip = new ZipArchive();

        $zip->open($zipname, ZipArchive::CREATE);
        foreach ($files as $file) {
            $zip->addFile($file);
        }

        $zip->close();
        unlink($EAFileName);
        unlink($HouseholdFileName);
        unlink($publicSchoolInfoFilePath);
        unlink($privateSchoolInfoFilePath);
        return response()->download($zipname, null, $headers)->deleteFileAfterSend(TRUE);
    }

    private function getSurveyDetails($survey) {

        $EnumerationArea = \App\Model\EnumerationArea::where("id", $survey->enumeration_areas_id)->first();

        $stateName = $EnumerationArea->localGovernment->state->name;
        $stateCode = $EnumerationArea->localGovernment->state->code;
        $localGovernmentName = $EnumerationArea->localGovernment->name;
        $localGovernmentCode = $EnumerationArea->localGovernment->code;
        $EnumerationAreaName = $EnumerationArea->name;
        $EnumerationAreaCode = $EnumerationArea->code;

        $surveyDetails = array(
            $stateName,
            $stateCode,
            $localGovernmentName,
            $localGovernmentCode,
            $EnumerationAreaName,
            $EnumerationAreaCode
        );
        return $surveyDetails;
    }

    private function getHeaders() {
        return array(
            "Content-type" => "application/zip",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
    }

    private function getSurveys($state, $localGovernment, $EA) {
        $surveys = \App\Model\Survey::where('status', 1)->get();
        $surveysArray = [];
        $surveysArray["householdObservation"] = array();
        $surveysArray["householdInformation"] = array();
        $surveysArray["schoolInfo"] = array();
        $count = 1;
        foreach ($surveys as $survey) {
            if ($count >= 100) {
                break;
            }
            $eaObservationResult = $this->getObservationArea($survey);

            $households = $this->getHouseholdObservations($survey);
            $schoolInfo = $this->getSchoolResult($survey);

            $surveysArray["eaObservation"][$count] = $eaObservationResult;
            $surveysArray["householdObservation"] += $households["householdObservations"];
            $surveysArray["householdInformation"] += $households["householdInformations"];
            $surveysArray["schoolInfo"] += $schoolInfo;

            $count++;
        }



        return $surveysArray;
    }

    private function getQuestions() {

        $surveyDetailsQuestions = ["State Name", "State Code",
            "Local Government Name", "Local Government Code",
            "EA Name", "EA Code"];
        $questions = [];
        $EAObservationQuestions = \App\Model\EAObservationSheetQuestion::orderBy('id')->get()->pluck("question")->toArray();
        $questions["eaObservation"] = $surveyDetailsQuestions + $EAObservationQuestions;


        $HouseholdObservationQuestion = \App\Model\HouseholdQuestion::all()->pluck("question")->toArray();
        $HouseholdObservationQuestion = $surveyDetailsQuestions + $HouseholdObservationQuestion;
        $HouseholdInformationQuestion = \App\Model\HouseholdInformationQuestion::orderBy('id')->get()->pluck("question")->toArray();
        $questions["householdInformation"] = array_merge($HouseholdObservationQuestion, $HouseholdInformationQuestion);

        $SchoolQuestion = \App\Model\SchoolSurveyQuestion::orderBy('id')->get()->pluck("question")->toArray();

        $questions["schoolInfo"] = array_merge($surveyDetailsQuestions, $SchoolQuestion);

        return $questions;
    }

    private function getObservations() {
        
    }

    private function getObservationArea($survey) {
        $result = \App\Model\EAObservationResult::where('surveys_id', $survey->id)->get();
        $resultArray = [];
        $count = 0;
        $surveyId = 0;

        foreach ($result as $res) {
            if ($res->answer->id == 10) {
                $resultArray[] = $res->text;
            } else {
                $resultArray[] = $res->answer->answer;
            }
        }
        $surveyDetails = $this->getSurveyDetails($survey);
        /**  $volunteers = $this->getVolunteers($survey);
          $resultArrayCount = count($resultArray);

          foreach ($volunteers as $vol) {
          $a = array($vol);
          $resultArray[$resultArrayCount] = $vol->name;
          $resultArrayCount++;
          }* */
        $resultArray = $surveyDetails + $resultArray;

        return $resultArray;
    }

    private function getVolunteers($survey) {
        $result = \App\Model\Volunteer::where("surveys_id", $survey->id)->get();
        return $result;
    }

    private function getHouseholds($survey) {
        $households = \App\Model\Household::where("surveys_id", $survey->id)->get();
        dd($households);
    }

    private function getHouseholdObservations($survey) {

        $householdObservation = \App\Model\HouseholdObservation::where("surveys_id", $survey->id)->get();
        //$households = $this->getHouseholds($survey);
        //dd($households);
        $householdObservations = [];
        $householdInformations = [];
        $surveyDetails = $this->getSurveyDetails($survey);
        $results = [];
        $count = 0;
        $householdObservationCount = 0;
        foreach ($householdObservation as $observation) {
            $householdInfoResult = $observation->householdInformationResult()->orderBy('child_id')->orderBy('question_id', 'ASC')->get();
            $householdResult = $observation->householdResult()->orderBy('household_questions_id', 'ASC')->get();
            if (count($householdInfoResult) <= 0 || count($householdResult) <= 0) {
                continue;
            }
            $householdChildIdCount = 1;
            foreach ($householdResult as $hr) {

                if ($hr->answer) {
                    if ($hr->answer->id == 10) {
                        $householdObservations[$householdObservationCount][] = $hr->text;
                    } else {
                        $householdObservations[$householdObservationCount][] = $hr->answer->answer;
                    }
                } else {
                    $householdObservations[$householdObservationCount][] = "";
                }
            }

            foreach ($householdInfoResult as $hi) {
                if ($hi->child_id > $householdChildIdCount) {
                    //create a new row here
                    $householdInformations[$count] = array_merge($householdObservations[$householdObservationCount], $householdInformations[$count]);
                    dd($householdInformations);
                    $count++;
                    $householdChildIdCount++;
                }


                if ($hi->answer) {
                    if ($hi->answer->id == 10) {
                        $householdInformations[$count][] = $hi->text;
                    } else {
                        $householdInformations[$count][] = $hi->answer->answer;
                    }
                } else {
                    $householdInformations[$count][] = "";
                }
            }

            $householdObservations[$count] = $surveyDetails + $householdObservations[$householdObservationCount];
            $householdInformations[$count] = array_merge($householdObservations[$householdObservationCount], $householdInformations[$count]);
            if ($count == 2) {
                dd($householdInformations);
            }
            $count++;
            $householdObservationCount++;
        }

        $results["householdObservations"] = $householdObservations;
        $results["householdInformations"] = $householdInformations;

        return $results;
    }

    private function getSchoolResult($survey) {
        $surveyDetails = $this->getSurveyDetails($survey);
        $schoolResult = \App\Model\SchoolSurveyResult::where("surveys_id", $survey->id)->get();

        $schoolResultArray = [
            "public" => [],
            "private" => []
        ];
        $alreadyAnsweredQuestion = [
            "public" => [],
            "private" => []
        ];
        $questionsAnswered = [
            "public" => [],
            "private" => []
        ];
        $resultArray = array();
        $count = 0;
        $questions = \App\Model\SchoolSurveyQuestion::all()->pluck('id')->toArray();

        foreach ($schoolResult as $result) {
            $answer = "";
            if ($result->answer) {
                if ($result->answer->id == 10) {
                    $answer = $result->text;
                } else {
                    $answer = $result->answer->answer;
                }
            } else {
                $answer = "";
            }

            $resultArray[$count] = $answer;

            if ($result->school_type == 1) {

                if (array_key_exists($result->question->id, $alreadyAnsweredQuestion["public"])) {
                    $countVal = $alreadyAnsweredQuestion["public"][$result->question->id];
                    $schoolResultArray["public"][$countVal] = $schoolResultArray["public"][$countVal] . ", " . $resultArray[$count];

                    file_put_contents("a.txt", $countVal);
                } else {

                    $questionsAnswered["public"][] = $result->question->id;
                    $alreadyAnsweredQuestion["public"][$result->question->id] = $count;
                    $schoolResultArray["public"][$count] = $resultArray[$count];
                }
            } elseif ($result->school_type == 2) {
                if (array_key_exists($result->question->id, $alreadyAnsweredQuestion["private"])) {
                    $countVal = $alreadyAnsweredQuestion["private"][$result->question->id];
                    $schoolResultArray["private"][$countVal] = $schoolResultArray["private"][$countVal] . ", " . $resultArray[$count];
                } else {
                    $questionsAnswered["private"][] = $result->question->id;
                    $alreadyAnsweredQuestion["private"][$result->question->id] = $count;
                    $schoolResultArray["private"][$count] = $resultArray[$count];
                }
            }

            $count++;
        }

        $missingPublicQuestions = array_diff($questions, $questionsAnswered["public"]);
        $missingPrivateQuestions = array_diff($questions, $questionsAnswered["private"]);
        foreach ($missingPublicQuestions as $mpubQ) {
            array_splice($schoolResultArray["public"], $mpubQ, 0, "");
        }
        foreach ($missingPrivateQuestions as $mprivQ) {
            array_splice($schoolResultArray["private"], $mprivQ, 0, "");
        }
        $schoolResultArray["public"] = array_merge($surveyDetails, $schoolResultArray["public"]);
        $schoolResultArray["private"] = array_merge($surveyDetails, $schoolResultArray["private"]);

        return $schoolResultArray;
    }

}
