<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;

class EACompilationController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        return view("admin.ea_compilation");
    }

    public function createEACompilation(Request $request) {
        if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        $totalQuestions = 42;
        $page = 10;
        $numberedQuestion = [1, 2, 3, 4, 5, 6, 7];
        $householdId = 1;
        if (Session::get("ea_comp_household_id")) {
            $householdId = Session::get("ea_comp_household_id");
        }
        for ($i = 1; $i <= $totalQuestions; $i++) {
            $questionVar = "question" . $i;
            $answer = $request->$questionVar;
            if (!$answer) {
                continue;
            }
            $question_id = $i;
            
            $theAns = 10;
            $text = $answer;

            $this->createAnswer($theAns, $question_id, $householdId, $text);
        }
        Session::put("ea_comp_household_id", $householdId + 1);
        Session::put("page", $page);
        $surveyLog = \App\Model\SurveyLog::create([
                    "page" => $page,
                    "user_id" => auth()->user()->id,
                    "survey_id" => Session::get("survey_id")
        ]);

        $finishRoute = Input::get('finish');

        if (isset($finishRoute)) {
            return redirect()->route('survey.success');
        }

        return redirect()->route('survey.ea_compilation');
    }

    private function createAnswer($answer, $question_id, $householdId, $text = '') {

        $result = \App\Model\EACompilationResult::create([
                    'text' => $text,
                    'question_id' => $question_id,
                    'answer_id' => $answer,
                    'household_id' => $householdId,
                    'survey_id' => Session::get("survey_id")
        ]);
        return $result;
    }

}
