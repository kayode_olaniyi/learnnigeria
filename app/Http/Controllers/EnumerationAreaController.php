<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\User;

/**
 * Description of EnumerationAreaController
 *
 * @author Kayode
 */
class EnumerationAreaController extends Controller {

    //put your code here
    //put your code here
    //
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Guard $guard) {
       
        if (!$this->isValidUserRole($guard, [1])) {
            return redirect()->route("dashboard");
        }
        $data = [];

        $enumerationAreas = \App\Model\EnumerationArea::all();

        $data["enumeration_areas"] = $enumerationAreas;
        return view('admin.enumeration_area', $data);
    }

    public function changeStatus($id) {
        $enumerationArea = \App\Model\EnumerationArea::findOrFail($id);

        if ($enumerationArea->status == 0) {
            $enumerationArea->status = 1;
        } elseif ($enumerationArea->status == 1) {
            $enumerationArea->status = 0;
        }

        $enumerationArea->save();

        return redirect(route('enumeration_area.index'))->with('message', 'The EA\'s status has been changed');
    }

}
