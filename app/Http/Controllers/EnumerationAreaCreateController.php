<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

/**
 * Description of EnumerationAreaCreateController
 *
 * @author Kayode
 */
class EnumerationAreaCreateController extends Controller {

    //put your code here

    public function __construct() {
        $this->middleware('auth');
    }

    public function index($id, \Illuminate\Contracts\Auth\Guard $guard) {
        if (!$this->isValidUserRole($guard, [1])) {
            return redirect()->route("dashboard");
        }
        $data = array();
        $enumerationArea = \App\Model\EnumerationArea::all();
        $localGovernments = \App\Model\LocalGovernment::where('id', $id)->get();

        $data["local_governments"] = $localGovernments;
       
        $data["enumeration_area"] = $enumerationArea;
        return view('admin.enumeration_area_create', $data);
    }

    public function create(\Illuminate\Http\Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'code' => 'required|unique:enumeration_areas|max:255',
                    'local_government_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $EAData = [
            'name' => $request->name,
            'code' => $request->code,
            'local_government_id' => $request->local_government_id,
            'users_id' => auth()->user()->id,
            'status' => 1
        ];

        $enumerationArea = \App\Model\EnumerationArea::create($EAData);

        $data = ["enumeration_area" => $enumerationArea];
        $request->session()->flash("enumeration_area_created", true);
        $request->session()->flash('enumeration_area', $enumerationArea);

        return redirect(route('enumeration_area.create',[$request->state_id]))->with("enumeration_area", $enumerationArea);
    }

    public function editIndex($id) {
        $data = array();

        $enumerationArea = \App\Model\EnumerationArea::find($id);
        if (!$enumerationArea) {
            return redirect(route('enumeration_area.index'))->with("error", "Invalid Enumeration Area");
        }
        $data["enumerationArea"] = $enumerationArea;

        $data["edit"] = true;
        $data["local_governments"] = \App\Model\LocalGovernment::all();

        return view('admin.enumeration_area_create', $data);
    }

    public function edit(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'code' => 'required',
                    'state_id' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $enumerationArea = \App\Model\EnumerationArea::find($request->id);

        $enumerationArea->name = $request->name;


        $enumerationArea->local_government_id = $request->local_government_id;

        $enumerationArea->save();
        $request->session()->flash("enumeration_area_editted", true);
        $request->session()->flash("enumeration_area", $enumerationArea);
        return redirect(route('enumeration_area.index'));
    }

}
