<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class HouseholdController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function loadIndex() {
        if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        return view("admin.household");
    }

    public function createHousehold(Request $request) {
        if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        $households = $this->buildHouseholds($request->household);
        $surveyId = Session::get("survey_id");
        $page = 3;
        $enumerationAreaId = Session::get("enumeration_area_id");
        $survey = \App\Model\Survey::findOrFail($surveyId);
        $survey->ric = $request->survey['ric'];
        $survey->locality = $request->survey['locality'];
        $survey->save();
        $householdIds = [];
        foreach ($households as $household) {
            $householdId = \App\Model\Household::create([
                        'address' => $household['address'],
                        'building_no' => $household['building_no'],
                        'hh_number' => $household['hh_number'],
                        'household_head' => $household['household_head'],
                        'surveys_enumeration_areas_id' => $enumerationAreaId,
                        'surveys_id' => $surveyId]);
            array_push($householdIds, $householdId);
        }
        Session::put("page", $page);
        Session::put("houseHolds", $householdIds);

        $surveyLog = \App\Model\SurveyLog::create([
                    "page" => $page,
                    "user_id" => auth()->user()->id,
                    "survey_id" => Session::get("survey_id")
        ]);
        return redirect()->route('survey.public_school_survey');
    }

    protected function buildHouseholds($households) {

        $hHold = [];
        $houseHoldAddress = $households["address"];
        $houseHoldBuildingNo = $households["building_no"];
        $houseHoldHHNo = $households["hh_number"];
        $houseHoldHead = $households["household_head"];
        $noOfLoops = count($houseHoldHHNo);
        for ($i = 0; $i < $noOfLoops; $i++) {
            $hHold[$i]["address"] = $houseHoldAddress[$i];
            $hHold[$i]["building_no"] = $houseHoldBuildingNo[$i];
            $hHold[$i]["hh_number"] = $houseHoldHHNo[$i];
            $hHold[$i]["household_head"] = $houseHoldHead[$i];
        }
        return $hHold;
    }

    public function loadObservationIndex() {
        if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        return view('admin.household_observation');
    }

    public function submitHouseholdObservationSurvey(Request $request) {
        if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        $buildingNo = $request->question5;
        $surveyId = Session::get("survey_id");
        $page = 8;
        $enumerationAreaId = Session::get("enumeration_area_id");
        $household = \App\Model\HouseholdObservation::create([
                    'building_no' => $buildingNo,
                    'surveys_id' => $surveyId,
                    'surveys_enumeration_areas_id' => $enumerationAreaId
        ]);

        $householdId = $household->id;
        Session::put("household_id", $householdId);
        $totalQuestions = 53;
        for ($i = 1; $i <= $totalQuestions; $i++) {
            $questionVar = "question" . $i;
            $answer = $request->$questionVar;
            if (!$answer) {
                continue;
            }
            $question_id = $i;
            if (is_array($answer)) {
                //loop through answer and input all result
                foreach ($answer as $ans) {
                    $theAns = '';
                    $text = '';

                    if (is_numeric($ans)) {
                        $theAns = $ans;
                    } else {
                        $theAns = 22;
                        $text = $ans;
                    }
                    $this->createAnswer($theAns, $question_id, $householdId, $text);
                }
            } else {
                $theAns = '';
                $text = '';
                $question = \App\Model\HouseholdQuestion::find($i);
                if (is_numeric($answer) && $question->type != 1) {
                    $theAns = $answer;
                } else {
                    $theAns = 10;
                    $text = $answer;
                }

                $this->createAnswer($theAns, $question_id, $householdId, $text);
            }
        }
        
        $guardianMothers = $this->buildHouseholdsInfo($request->guardian_mother);

        foreach ($guardianMothers as $guardianMother) {
            $householdInfo = \App\Model\HouseholdGuardian::create([
                        "name" => $guardianMother["name"],
                        "children_number" => $guardianMother["children_number"],
                        "age" => $guardianMother["age"],
                        "ever_attended_school" => $guardianMother["ever_attended_school"],
                        "highest_level_completed" => $guardianMother["highest_level_completed"],
                        "type" => 1,
                        "household_id" => $householdId
            ]);
        }
        if ($request->guardian_father != null) {
            $guardianFathers = $this->buildHouseholdsInfo($request->guardian_father);

            foreach ($guardianFathers as $guardianFather) {
                $householdInfoFather = \App\Model\HouseholdGuardian::create([
                            "name" => $guardianFather["name"],
                            "children_number" => $guardianFather["children_number"],
                            "age" => $guardianFather["age"],
                            "ever_attended_school" => $guardianFather["ever_attended_school"],
                            "highest_level_completed" => $guardianFather["highest_level_completed"],
                            "type" => 2,
                            "household_id" => $householdId
                ]);
            }
        }

        Session::put("page", $page);
        $surveyLog = \App\Model\SurveyLog::create([
                    "page" => $page,
                    "user_id" => auth()->user()->id,
                    "survey_id" => Session::get("survey_id")
        ]);

        return redirect()->route('survey.household_information');
    }

    private function createAnswer($answer, $school_survey_question_id, $householdId, $text = '') {
        $surveyId = Session::get("survey_id");
        $enumerationAreaId = Session::get("enumeration_area_id");
        $result = \App\Model\HouseholdResult::create([
                    'text' => $text,
                    'household_questions_id' => $school_survey_question_id,
                    'answer_id' => $answer,
                    'household_observation_sheet_surveys_enumeration_areas_id' => $enumerationAreaId,
                    'household_observation_sheet_surveys_id' => $surveyId,
                    'household_observation_sheet_id' => $householdId
        ]);
        return $result;
    }

    protected function buildHouseholdsInfo($hInfo) {

        $vltrs = [];
        $volNames = $hInfo["name"];
        $volSex = $hInfo["children_number"];
        $age = $hInfo["age"];
        $volCode = $hInfo["ever_attended_school"];
        $volMobile = $hInfo["highest_level_completed"];
        $noOfLoops = count($volNames);
        for ($i = 0; $i < $noOfLoops; $i++) {
            $vltrs[$i]["name"] = $volNames[$i];
            $vltrs[$i]["age"] = $age[$i];
            $vltrs[$i]["children_number"] = $volSex[$i];
            $vltrs[$i]["ever_attended_school"] = $volCode[$i];
            $vltrs[$i]["highest_level_completed"] = $volMobile[$i];
        }
        return $vltrs;
    }

}
