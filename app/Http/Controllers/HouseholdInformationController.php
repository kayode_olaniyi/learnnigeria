<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;

class HouseholdInformationController extends Controller {

    //
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $childId = 0;
        if (Session::get("child_id") > 0) {
            $childId = Session::get("child_id") + 1;
        } else {
            $childId = 1;
        }

        $data["child_id"] = $childId;
        return view("admin.household_information", $data);
    }

    public function createInformation(Request $request) {
        $questionsWithMultipleAns = [5, 7, 12, 24, 26];
        $numberedQuestion = [8, 11, 21, 16, 18, 19, 22, 27, 17, 20];
        $totalQuestions = 38;
        $page = 9;
        $childId = $request->child_id;
        $householdId = Session::get("household_id");

        for ($i = 1; $i <= $totalQuestions; $i++) {
            $questionVar = "question" . $i;
            $answer = $request->$questionVar;
            if (!$answer) {
                continue;
            }
            $question_id = $i;
            if (is_array($answer)) {
                //loop through answer and input all result
                foreach ($answer as $ans) {
                    $theAns = '';
                    $text = '';

                    if (is_numeric($ans)) {
                        $theAns = $ans;
                    } else {
                        $theAns = 22;
                        $text = $ans;
                    }
                    $this->createAnswer($theAns, $question_id, $householdId, $childId, $text);
                }
            } else {
                $theAns = '';
                $text = '';
                //$question = \App\Model\HouseHoldQuestion::find($i);
                if (is_numeric($answer) && !in_array($i, $numberedQuestion)) {
                    $theAns = $answer;
                } else {
                    $theAns = 10;
                    $text = $answer;
                }

                $this->createAnswer($theAns, $question_id, $householdId, $childId, $text);
            }
        }
        Session::put("child_id", $childId);
        Session::put("page", $page);
        $surveyLog = \App\Model\SurveyLog::create([
                    "page" => $page,
                    "user_id" => auth()->user()->id,
                    "survey_id" => Session::get("survey_id")
        ]);
        $eaCompRoute = Input::get('ea_compilation');
        
        if (isset($eaCompRoute)) {
            return redirect()->route('survey.ea_compilation');
        }
        $householdRoute = Input::get('new_household');
        if (isset($householdRoute)) {
            return redirect()->route('survey.household.observation');
        }
        return redirect()->route('survey.household_information');
    }

    private function createAnswer($answer, $question_id, $householdId, $child_id, $text = '') {

        $result = \App\Model\HouseholdInformationResult::create([
                    'text' => $text,
                    'question_id' => $question_id,
                    'answer_id' => $answer,
                    'household_observation_sheet_id' => $householdId,
                    'child_id' => $child_id
        ]);
        return $result;
    }

}
