<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\User;

/**
 * Description of LocalGovernmentController
 *
 * @author Kayode
 */
class LocalGovernmentController extends Controller {

    //put your code here
    //
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Guard $guard) {
        if (!$this->isValidUserRole($guard, [1])) {
            return redirect()->route("dashboard");
        }
        $data = [];

        $localGovernment = \App\Model\LocalGovernment::all();
      
        $data["local_governments"] = $localGovernment;
        return view('admin.local_government', $data);
    }
    
    public function changeStatus($id) {
        $localGovernment = \App\Model\LocalGovernment::findOrFail($id);

        if ($localGovernment->status == 0) {
            $localGovernment->status = 1;
        } elseif ($localGovernment->status == 1) {
            $localGovernment->status = 0;
        }

        $localGovernment->save();
        
        return redirect(route('local_government.index'))->with('message','The user\'s status has been changed');
    }

    public function apiIndex($id){
        $localGovernments = \App\Model\LocalGovernment::findOrFail($id);
        return response()->json($localGovernments);
    }

}
