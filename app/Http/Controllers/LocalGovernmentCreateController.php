<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

/**
 * Description of LocalGovernmentCreateController
 *
 * @author Kayode
 */
class LocalGovernmentCreateController extends Controller {

    //put your code here

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(\Illuminate\Contracts\Auth\Guard $guard) {
        if (!$this->isValidUserRole($guard, [1])) {
            return redirect()->route("dashboard");
        }
        $data = array();

        $states = \App\Model\State::all();
        $localGovernments = \App\Model\LocalGovernment::all();

        $data["states"] = $states;
        $data["local_governments"] = $localGovernments;
        return view('admin.local_government_create', $data);
    }

    public function create(\Illuminate\Http\Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:local_governments|max:255',
                    'code' => 'required|unique:local_governments|max:255',
                    'state_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $LGData = [
            'name' => $request->name,
            'code' => $request->code,
            'states_id' => $request->state_id,
            'users_id' => auth()->user()->id,
            'status' => 1
        ];

        $localGovernment = \App\Model\LocalGovernment::create($LGData);

        $data = ["local_government" => $localGovernment];
        $request->session()->flash("local_government_created", true);
        $request->session()->flash('local_government', $localGovernment);

        return redirect(route('local_government.create'))->with("local_government", $localGovernment);
    }

    public function editIndex($id) {
        $data = array();

        $localGovernment = \App\Model\LocalGovernment::find($id);
        if (!$localGovernment) {
            return redirect(route('local_government.index'))->with("error", "Invalid Local Government");
        }
        $data["localGovernment"] = $localGovernment;

        $data["edit"] = true;
        $data["states"] = \App\Model\State::all();

        return view('admin.local_government_create', $data);
    }

    public function edit(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'code' => 'required',
                    'state_id' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $localGovernment = \App\Model\LocalGovernment::find($request->id);

        $localGovernment->name = $request->name;
        $localGovernment->code = $request->code;

        $localGovernment->states_id = $request->state_id;

        $localGovernment->save();
        $request->session()->flash("local_government_editted", true);
        $request->session()->flash("local_government", $localGovernment);
        return redirect(route('local_government.index'));
    }

}
