<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class PublicSchoolSurveyController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    //
    public function loadIndex() {
            if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        return view('admin.public_school_survey');
    }

    public function loadSecondIndex() {
            if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        return view('admin.public_school_survey2');
    }

    public function createPublicSurveySheet1(Request $request) {
            if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }
        
        $totalQuestions = 56;
        $page = 4;
        $exemptQuestions = [166, 167, 168, 169,184,185,188,189,190,191];
        for ($i = 1; $i <= $totalQuestions; $i++) {
            $questionVar = "question" . $i;
            $answer = $request->$questionVar;
            if (!$answer) {
                continue;
            }
            $question_id = $i;
            if (is_array($answer)) {
                //loop through answer and input all result
                foreach ($answer as $ans) {
                    $theAns = '';
                    $text = '';

                    if (is_numeric($ans)) {
                        $theAns = $ans;
                    } else {
                        $theAns = 22;
                        $text = $ans;
                    }
                    $this->createAnswer($theAns, $question_id, $text);
                }
            } else {
                $theAns = '';
                $text = '';
                $question = \App\Model\SchoolSurveyQuestion::find($i);
                if(!$question){
                    continue;
                }
                if (is_numeric($answer) && $question->type != 1) {
                    $theAns = $answer;
                } else {
                    $theAns = 10;
                    $text = $answer;
                }
                $this->createAnswer($theAns, $question_id, $text);
            }
        }

        foreach ($exemptQuestions as $i) {

            $questionVar = "question" . $i;
            $answer = $request->$questionVar;

            if (!$answer) {
                continue;
            }
            $question_id = $i;
            if (is_array($answer)) {
                //loop through answer and input all result
                foreach ($answer as $ans) {
                    $theAns = '';
                    $text = '';

                    if (is_numeric($ans)) {
                        $theAns = $ans;
                    } else {
                        $theAns = 22;
                        $text = $ans;
                    }
                    $this->createAnswer($theAns, $question_id, $text);
                }
            } else {
                $theAns = '';
                $text = '';
                $question = \App\Model\SchoolSurveyQuestion::find($i);
                if (is_numeric($answer) && $question->type != 1) {
                    $theAns = $answer;
                } else {
                    $theAns = 10;
                    $text = $answer;
                }
                $this->createAnswer($theAns, $question_id, $text);
            }
        }
        Session::put("page", $page);
        $surveyLog = \App\Model\SurveyLog::create([
                    "page" => $page,
                    "user_id" => auth()->user()->id,
                    "survey_id" => Session::get("survey_id")
        ]);

        return redirect()->route('survey.public_school_survey2');
    }

    public function createPublicSurveySheet2(Request $request) {
            if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        $page = 5;
        $exemptQuestions = [170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183];
        $totalQuestions = 165;
        for ($i = 109; $i <= $totalQuestions; $i++) {
            $questionVar = "question" . $i;
            $answer = $request->$questionVar;
            $question_id = $i;
            if (is_array($answer)) {
                //loop through answer and input all result
                foreach ($answer as $ans) {
                    $theAns = '';
                    $text = '';

                    if (is_numeric($ans)) {
                        $theAns = $ans;
                    } else {
                        $theAns = 22;
                        $text = $ans;
                    }
                    $this->createAnswer($theAns, $question_id, $text);
                }
            } else {
                $theAns = '';
                $text = '';
                $question = \App\Model\SchoolSurveyQuestion::find($i);
                if (is_numeric($answer) && $question->type != 1) {
                    $theAns = $answer;
                } else {
                    $theAns = 10;
                    $text = $answer;
                }
                $this->createAnswer($theAns, $question_id, $text);
            }
        }

        foreach ($exemptQuestions as $i) {

            $questionVar = "question" . $i;
            $answer = $request->$questionVar;

            if (!$answer) {
                continue;
            }
            $question_id = $i;
            if (is_array($answer)) {
                //loop through answer and input all result
                foreach ($answer as $ans) {
                    $theAns = '';
                    $text = '';

                    if (is_numeric($ans)) {
                        $theAns = $ans;
                    } else {
                        $theAns = 22;
                        $text = $ans;
                    }
                    $this->createAnswer($theAns, $question_id, $text);
                }
            } else {
                $theAns = '';
                $text = '';
                $question = \App\Model\SchoolSurveyQuestion::find($i);
                if (is_numeric($answer) && $question->type != 1) {
                    $theAns = $answer;
                } else {
                    $theAns = 10;
                    $text = $answer;
                }
                $this->createAnswer($theAns, $question_id, $text);
            }

            Session::put("page", $page);
            $surveyLog = \App\Model\SurveyLog::create([
                        "page" => $page,
                        "user_id" => auth()->user()->id,
                        "survey_id" => Session::get("survey_id")
            ]);
            return redirect()->route('survey.private_school_survey');
        }
    }

    private function createAnswer($answer, $school_survey_question_id, $text = '') {
        $surveyId = Session::get("survey_id");
        $enumerationAreaId = Session::get("enumeration_area_id");
        $result = \App\Model\SchoolSurveyResult::create([
                    'text' => $text,
                    'school_survey_questions_id' => $school_survey_question_id,
                    'answers_id' => $answer,
                    'surveys_enumeration_areas_id' => $enumerationAreaId,
                    'surveys_id' => $surveyId,
                    'school_type' => 1
        ]);
        return $result;
    }

}
