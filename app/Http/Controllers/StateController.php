<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\User;

/**
 * Description of StateController
 *
 * @author Kayode
 */
class StateController extends Controller {

    //put your code here
    public function apiIndex() {
        $states = \App\Model\State::all();
        return response()->json($states);
    }

    public function apiLoadLG($id) {
        $localGovernments = \App\Model\LocalGovernment::where("states_id", $id)->get();
        return response()->json($localGovernments);
    }
    
    public function loadEA($id){
        
         $ea = \App\Model\EnumerationArea::where("local_government_id", $id)->get();
        return response()->json($ea);
    }

}
