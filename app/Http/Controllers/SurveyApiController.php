<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Description of SurveyApiController
 *
 * @author Kayode
 */
class SurveyApiController extends Controller {

    //put your code here
    public function createSurvey(Request $request) {
   
        $survey = \App\Model\Survey::create([
                    "resident_name" => $request->survey["resident_name"],
                    "resident_phone_number" => $request->survey["resident_phone_number"],
                    "sector" => $request->survey["sector"],
                    "survey_date" => date("Y-m-d"),
                    "enumeration_areas_id" => $request->survey["enumeration_area_id"]
        ]);

        $supervisor = \App\Model\Supervisor::create([
                    "name" => $request->supervisor["name"],
                    "sex" => $request->supervisor["sex"],
                    "mobile" => $request->supervisor["phone_number"],
                    "code" => $request->supervisor["sector"],
                    "surveys_id" => $survey->id,
    "surveys_enumeration_areas_id" => $request->survey["enumeration_area_id"]
        ]);

        $volunteers = $request->volunteers;
             return response()->json($request);
        $volunteerIds = [];
        $count = 0;
        foreach ($volunteers as $volunteer) {
            if ($count <= 0) {
                $count++;
                continue;
            }
       
            $vltr = \App\Model\Volunteer::create([
                        "name" => $volunteer->name,
                        "sex" => $volunteer["sex"],
                        "mobile" => $volunteer["phone_number"],
                        "code" => $volunteer["sector"]
            ]);

            array_push($volunteerIds, $vltr->id);
        }
        $response = ["survey" => $survey->id, "supervisor" => $supervisor->id, "volunteers" => $volunteerIds];

        return response()->json($response);
    }

}
