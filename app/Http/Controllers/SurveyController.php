<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class SurveyController extends Controller {

    private $pagesRoute = [
    ];
    private $surveyId;

    //
    public function __construct() {
        $this->middleware('auth');
        $this->pagesRoute["1"] = route('survey.home');
        $this->pagesRoute["2"] = route('survey.questions');
        $this->pagesRoute["3"] = route('survey.household');
        $this->pagesRoute["4"] = route('survey.public_school_survey');
        $this->pagesRoute["5"] = route('survey.public_school_survey2');
        $this->pagesRoute["6"] = route('survey.private_school_survey');
        $this->pagesRoute["7"] = route('survey.private_school_survey2');
        $this->pagesRoute["8"] = route('survey.household.observation');
        $this->pagesRoute["9"] = route('survey.household_information');
        $this->pagesRoute["10"] = route('survey.ea_compilation');
    }

    public function index($id = 0) {

        $data = [];
        $states = \App\Model\State::all();
        $data["states"] = $states;
        if ($id > 0) {
            $page = $this->loadLastPage($id);
            $reRoutePage = 0;
            if ($page > 0) {
                $reRoutePage = $page + 1;

                $household = \App\Model\Household::where("surveys_id", $this->surveyId)->orderBy("id", "desc")->first();
                if ($household == null) {
                    return redirect($this->pagesRoute[8]);
                }
                Session::put("household_id", $household->id);

                return redirect($this->pagesRoute[$reRoutePage]);
            }
            return redirect(route('survey.home'));
        }
        return view('admin.survey', $data);
    }

    private function loadLastPage($surveyId) {
        $page = 0;
        $userId = auth()->user()->id;

        $surveyLog = \App\Model\SurveyLog::where("user_id", $userId)
                        ->where('survey_id', $surveyId)
                        ->orderBy("id", "desc")->first();
        if (!$surveyLog) {
            return 0;
        }

        $this->surveyId = $surveyId;

        $survey = \App\Model\Survey::find($surveyLog->survey_id);
        if ($survey->status == 0) {
            Session::put("survey_id", $this->surveyId);
            Session::put("enumeration_area_id", $survey->enumeration_areas_id);
            if ($surveyLog->page > 0) {
                $page = $surveyLog->page;
            }
        }
        return $page;
    }

    public function createSurvey(Request $request) {
        $volunteers = $this->buildVolunteers($request->volunteers);
        $page = 1;
        $survey = \App\Model\Survey::create([
                    "resident_name" => $request->survey["name"],
                    "resident_phone_number" => $request->survey["phone_number"],
                    "sector" => $request->survey["sector"],
                    "survey_date" => date("Y-m-d"),
                    "enumeration_areas_id" => $request->survey["enumeration_area_id"],
                    "users_id" => Auth::id()
        ]);


        $supervisor = \App\Model\Supervisor::create([
                    "name" => $request->supervisor["name"],
                    "sex" => $request->supervisor["sex"],
                    "mobile" => $request->supervisor["mobile"],
                    "code" => $request->supervisor["code"],
                    "surveys_id" => $survey->id,
                    "surveys_enumeration_areas_id" => $request->survey["enumeration_area_id"]
        ]);

        $volunteerIds = [];
        $count = 0;

        foreach ($volunteers as $volunteer) {
            $vltr = \App\Model\Volunteer::create([
                        "name" => $volunteer['name'],
                        "sex" => $volunteer["sex"],
                        "mobile" => $volunteer["mobile"],
                        "code" => $volunteer["code"],
                        "surveys_id" => $survey->id
            ]);

            array_push($volunteerIds, $vltr->id);
        }

        $response = ["survey" => $survey->id, "supervisor" => $supervisor->id, "volunteers" => $volunteerIds];
        Session::put("survey_id", $survey->id);
        Session::put("page", $page);
        Session::put("enumeration_area_id", $request->survey["enumeration_area_id"]);
        Session::put("page1_response", $response);

        $surveyLog = \App\Model\SurveyLog::create([
                    "page" => $page,
                    "user_id" => auth()->user()->id,
                    "survey_id" => $survey->id
        ]);
        return redirect()->route("survey.questions");
    }

    public function createQuestions(Request $request) {
        $totalQuestions = 24;
        $textQuestions = [20, 21];
        $page = 2;
        for ($i = 1; $i <= $totalQuestions; $i++) {
            $questionVar = "question" . $i;
            $answer = $request->$questionVar;
            if (!$answer) {
                continue;
            }
            $text = "";

            if (in_array($i, $textQuestions)) {
                $text = $answer;
                $answer = 10;
            }
            $eaResult = \App\Model\EAObservationResult::create([
                        "observation_sheet_questions_id" => $i,
                        "observation_sheet_answers_id" => $answer,
                        "text" => $text,
                        "surveys_id" => Session::get("survey_id"),
                        "surveys_enumeration_areas_id" => Session::get("enumeration_area_id")
            ]);
        }

        $surveyLog = \App\Model\SurveyLog::create([
                    "page" => $page,
                    "user_id" => auth()->user()->id,
                    "survey_id" => Session::get("survey_id")
        ]);

        return redirect()->to(route("survey.household"));
    }

    public function loadQuestions() {
        if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        return view('admin.survey_ea_observation_quest');
    }

    protected function buildVolunteers($volunteers) {

        $vltrs = [];
        $volNames = $volunteers["name"];
        $volSex = $volunteers["sex"];
        $volCode = $volunteers["code"];
        $volMobile = $volunteers["mobile"];
        $noOfLoops = count($volNames);
        for ($i = 0; $i < $noOfLoops; $i++) {
            $vltrs[$i]["name"] = $volNames[$i];
            $vltrs[$i]["sex"] = $volSex[$i];
            $vltrs[$i]["code"] = $volCode[$i];
            $vltrs[$i]["mobile"] = $volMobile[$i];
        }
        return $vltrs;
    }

    public function success() {
        if (!Session::get("survey_id")) {
            return redirect()->route('survey.home');
        }

        $surveyId = Session::get("survey_id");

        $survey = \App\Model\Survey::find($surveyId);
        $survey->status = 1;
        $survey->save();
        return view('admin.survey_success');
    }

}
