<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SurveyViewController extends Controller {

    //
    private $totalHouseholdResult = 0;
    private $totalHouseholdInformationResult = 0;

    public function __construct() {
        $this->middleware('auth');
    }

    public function view($id) {
        $user = auth()->user();
        $survey = \App\Model\Survey::find($id);
        $roles = $user->roles();
        $rolesId = $roles->pluck('roles_id')->toArray();
        if ($survey->users_id != $user->id && !in_array($this->defaultRoles["admin"], $rolesId)) {
            return redirect(route('survey.list'));
        }
        $survey = \App\Model\Survey::where("id", $id)->first();
        $eaObservationResult = $this->getObservationArea($survey);
        $volunteers = $this->getVolunteers($survey);
        $households = $this->getHouseholds($survey);
        $householdObservations = $this->getHouseholdObservations($survey);
        $schoolInfo = $this->getSchoolResult($survey);

        $data = ["survey" => $survey];
        $data["ea_observations"] = $eaObservationResult;
        $data["volunteers"] = $volunteers;
        $data["households"] = $households;
        $data["householdObservations"] = $householdObservations;
        $data["schoolResults"] = $schoolInfo;
        return view('admin.survey_view', $data);
    }

    private function getTotalHouseholdsDetailsCount() {
        $householdInformation = DB::select('SELECT count(*) as count FROM (   
        SELECT count(*),household_observation_sheet_id FROM household_information_results as r
        join household_observation_sheet as s on s.id = r.household_observation_sheet_id
        group by household_observation_sheet_id,r.child_id) householdInformationCount');

        $householdObservation = DB::select(' SELECT count(*) as count FROM (    
        SELECT count(*),household_observation_sheet_id FROM household_results as r
        join household_observation_sheet as s on s.id = r.household_observation_sheet_id
        group by household_observation_sheet_id) householdObservationCount');

        $result = [
            "householdObservation" => $householdObservation[0]->count,
            "householdInformation" => $householdInformation[0]->count
        ];
        return $result;
    }

    private function getObservationArea($survey) {
        $result = \App\Model\EAObservationResult::where('surveys_id', $survey->id)->get();
        return $result;
    }

    private function getVolunteers($survey) {
        $result = \App\Model\Volunteer::where("surveys_id", $survey->id)->get();
        return $result;
    }

    private function getHouseholds($survey) {
        return \App\Model\Household::where("surveys_id", $survey->id)->get();
    }

    private function getHouseholdObservations($survey) {

        $householdObservation = \App\Model\HouseholdObservation::where("surveys_id", $survey->id)->get();
        //dd($householdObservation[0]->householdInformationResult[0]);
        return $householdObservation;
    }

    private function getSchoolResult($survey) {
        $schoolResult = \App\Model\SchoolSurveyResult::where("surveys_id", $survey->id)->get();
        return $schoolResult;
    }

    public function listSurveys() {
        $user = auth()->user();
        $userRoles = $user->roles();
        $roles = $userRoles->pluck('roles_id')->toArray();

        $userHighestPermission = \App\Helpers\Functions::getHighestPermission($roles);

        $role = \App\Role::findOrFail($userHighestPermission)->first();
        $surveys = $this->getSurveys($role);
        $housholdResults = $this->getTotalHousholds($surveys);
        $totalHouseholdsDetailsCount = $this->getTotalHouseholdsDetailsCount();


        $data = [
            "surveys" => $surveys,
            "role" => $role,
            "householdResult" => $housholdResults,
            "householdsCount" => $totalHouseholdsDetailsCount
        ];

        return view('admin.survey_list', $data);
    }

    /**
     * this is an unoptimzed code
     * @param type $surveys
     * @return type
     */
    private function getTotalHousholds($surveys) {
        $houseHolds = [];

        foreach ($surveys as $survey) {

            $totalHouseResult = 0;
            $totalHouseholdSurveys = $survey->getHouseholdSurveys();
            if (!empty($totalHouseholdSurveys)) {
                $totalHouseResult = $totalHouseholdSurveys[0]->count;
            }
            $totalHouseholdInfoSurveys = $survey->getHouseholdInformationSurveys();
            $totalHouseholdInformtationResult = 0;
            if (!empty($totalHouseholdInfoSurveys)) {
                $totalHouseholdInformtationResult = $totalHouseholdInfoSurveys[0]->count;
            }
            // dd($survey->householdObservation->householdResult);
            $houseHolds[$survey->id]["household"] = $totalHouseResult;
            $houseHolds[$survey->id]["householdInformation"] = $totalHouseholdInformtationResult;
        }

        return $houseHolds;
    }

    private function getEnumerationCompilation($survey) {
        //$EA_Compilation = \App\Model\EACompilationResult::
    }

    private function getSurveys($role) {
        $query = null;

        if ($role->id == 3) {
            $user = auth()->user();
            $query = \App\Model\Survey::where('users_id', $user->id)->orderBy('id', 'desc');
        } else {
            $query = \App\Model\Survey::orderBy('id', 'desc');
        }

        return $query->paginate(10);
    }

}
