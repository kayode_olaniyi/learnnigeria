<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller {

    //
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(\Illuminate\Contracts\Auth\Guard $guard) {
        if (!$this->isValidUserRole($guard, [1])) {
            return redirect()->route("dashboard");
        }
        $users = User::all();
        $data = ["users" => $users];

        return view('admin.user', $data);
    }

    public function changeStatus($id) {
        $user = User::findOrFail($id);

        if ($user->status == 0) {
            $user->status = 1;
        } elseif ($user->status == 1) {
            $user->status = 0;
        }

        $user->save();
        
        return redirect(route('users'))->with('message','The user\'s status has been changed');
    }
    
    public function logout(){
         auth()->logout();
         return redirect(route('login'));
    }

}
