<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;
use Validator;

class UserCreateController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request,\Illuminate\Contracts\Auth\Guard $guard) {
        $roles = Role::all();
        if (!$this->isValidUserRole($guard, [1])) {
            return redirect()->route("dashboard");
        }
        $data = ["roles" => $roles];
        $edit = false;

        if (session('user')) {
            $data["user"] = session('user');
        }

        if ($request->edit) {

            $user = User::find($request->user);
            if (!$user) {
                return redirect(route('users'))->with("error", "Invalid User");
            }
            $data["user"] = $user;
            $data["edit"] = true;
        }


        return view('admin.user_create', $data);
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
                    'username' => 'required|unique:users|max:255',
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'password' => 'required',
                    'role' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::create([
                    'username' => $request->username,
                    'firstname' => $request->firstname,
                    'lastname' => $request->lastname,
                    'password' => bcrypt($request->password),
                    'status' => 1,
                    'users_id' => auth()->user()->id
        ]);

        $role = Role::findorFail($request->role);
        $user->roles()->attach($role->id);

        $data = ["user" => $user];
        $request->session()->flash("user_created", true);
        return redirect(route('user.create'))->with("user", $user);
    }

    public function edit(Request $request) {
        $validator = Validator::make($request->all(), [
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'role' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::find($request->user_id);

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;

        $user->roles()->detach();
        $role = Role::findorFail($request->role);
        $user->roles()->attach($role->id);

        $user->save();
        $request->session()->flash("user_editted", true);
        return redirect(route('users'))->with("user", $user);
    }

}
