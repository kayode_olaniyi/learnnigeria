<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;

/**
 * Description of DownloadReport
 *
 * @author Kayode
 */
class DownloadReportQueue extends Model {

    protected $table = "download_report_queue";
    protected $fillable = ['state', 'local_government', 'enumeration_area'];

}
