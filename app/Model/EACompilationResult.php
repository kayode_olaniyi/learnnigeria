<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;

/**
 * Description of SchoolSurveyResult
 *
 * @author Kayode
 */
class EACompilationResult extends Model {

    protected $table = "ea_compilation_results";
    protected $fillable = ['text', 'answer_id', 'household_id', 'question_id'];
    public $timestamps = false;

    public function question() {
        return $this->belongsTo("\App\Model\HouseholdInformationQuestion", "question_id");
    }

    public function answer() {
        return $this->belongsTo("\App\Model\Answer", "answer_id");
    }

}
