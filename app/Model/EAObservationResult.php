<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of EAObservationResult
 *
 * @author Kayode
 */
class EAObservationResult extends Model {

    //put your code here
    public $table = "ea_observation_results";
    protected $fillable = ['observation_sheet_questions_id', 'observation_sheet_answers_id', 'text', 'surveys_id', 'surveys_enumeration_areas_id'];

    public function question() {
        return $this->belongsTo("\App\Model\EAObservationSheetQuestion", "observation_sheet_questions_id");
    }

    public function answer() {
        return $this->belongsTo("\App\Model\Answer", "observation_sheet_answers_id");
    }

}
