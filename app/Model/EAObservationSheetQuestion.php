<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EAObservationSheetQuestion extends Model {

    protected $table = 'observation_sheet_questions';
    public $timestamps = false;

}
