<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
/**
 * Description of EnumerationArea
 *
 * @author Kayode
 */
class EnumerationArea extends Model{
    //put your code here
    protected $table = 'enumeration_areas';
    protected $fillable = ['name', 'code', 'local_government_id', 'users_id'];

    public function localGovernment() {
        return $this->belongsTo("\App\Model\LocalGovernment", 'local_government_id');
    }
}
