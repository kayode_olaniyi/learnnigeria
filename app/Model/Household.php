<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Household
 *
 * @author Kayode
 */
class Household extends Model {

    //put your code here
    protected $table = "households";
    protected $fillable = ['address', 'building_no', 'hh_number', 'household_head', 'surveys_enumeration_areas_id', 'surveys_id'];

 

}
