<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of HouseholdGuardian
 *
 * @author Kayode
 */
class HouseholdGuardian extends Model {

    //put your code here
    protected $table = "household_guardians";
    public $timestamps = false;
    protected $fillable = ['name', 'age', 'children_number', 'ever_attended_school', 'highest_level_completed', 'type', 'household_id'];

}
