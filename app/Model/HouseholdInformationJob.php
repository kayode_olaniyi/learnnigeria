<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of HouseholdInformationJob
 *
 * @author Kayode
 */
class HouseholdInformationJob extends Model {

    protected $table = "household_information_job";
    public $timestamps = false;
    protected $fillable = ["data", "report_id"];

}
