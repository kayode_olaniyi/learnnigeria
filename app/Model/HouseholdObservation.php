<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Description of SchoolSurveyResult
 *
 * @author Kayode
 */
class HouseholdObservation extends Model {

    protected $table = "household_observation_sheet";
    public $timestamps = false;
    protected $fillable = ['building_no', 'surveys_id', 'surveys_enumeration_areas_id'];

    public function householdResult() {
        return $this->hasMany("\App\Model\HouseholdResult", "household_observation_sheet_id");
    }

    public function householdInformationResult() {
        return $this->hasMany("\App\Model\HouseholdInformationResult", "household_observation_sheet_id");
    }

    public function householdGuardian() {
        return $this->hasMany("\App\Model\HouseholdGuardian", "household_id");
    }

    public function survey() {
        return $this->belongsTo("\App\Model\Survey", "surveys_id");
    }

}
