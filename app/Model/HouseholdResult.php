<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;

/**
 * Description of SchoolSurveyResult
 *
 * @author Kayode
 */
class HouseholdResult extends Model {

    protected $table = "household_results";
    protected $fillable = ['text', 'answer_id', 'household_observation_sheet_id', 'household_questions_id', 'household_observation_sheet_surveys_id', 'household_observation_sheet_surveys_enumeration_areas_id'];
    public $timestamps = false;

    public function question() {
        return $this->belongsTo("\App\Model\HouseholdQuestion", "household_questions_id");
    }

    public function answer() {
        return $this->belongsTo("\App\Model\Answer", "answer_id");
    }

}
