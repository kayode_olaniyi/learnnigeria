<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of LocalGovernment
 *
 * @author Kayode
 */
class LocalGovernment extends Model {

    //put your code here
    protected $table = 'local_governments';
    protected $fillable = ['name', 'code', 'states_id', 'users_id'];

    public function state() {
        return $this->belongsTo("\App\Model\State", 'states_id');
    }

}
