<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;

/**
 * Description of SchoolSurveyQuestion
 *
 * @author Kayode
 */
class SchoolSurveyQuestion extends Model {

    protected $table = "school_survey_questions";

}
