<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;

/**
 * Description of SchoolSurveyResult
 *
 * @author Kayode
 */
class SchoolSurveyResult extends Model {

    protected $table = "school_survey_results";
    protected $fillable = ['text', 'school_survey_questions_id', 'answers_id', 'surveys_id', 'surveys_enumeration_areas_id', 'school_type'];

    public function question() {
        return $this->belongsTo("\App\Model\SchoolSurveyQuestion", "school_survey_questions_id");
    }

    public function answer() {
        return $this->belongsTo("\App\Model\Answer", "answers_id");
    }

}
