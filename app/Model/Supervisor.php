<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Supervisor
 *
 * @author Kayode
 */
class Supervisor extends Model {

    //put your code here
    protected $table = 'supervisors';
    public $timestamps = false;
    protected $fillable = ['name', 'sex', 'mobile', 'code', 'surveys_id', 'surveys_enumeration_areas_id'];

}
