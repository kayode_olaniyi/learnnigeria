<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Description of Survey
 *
 * @author Kayode
 */
class Survey extends Model {

    //put your code here
    protected $table = 'surveys';
    public $timestamps = false;
    protected $fillable = ['sector', 'resident_name', 'resident_phone_number', 'survey_date', 'enumeration_areas_id', 'users_id'];

    public function enumerationArea() {
        return $this->belongsTo("\App\Model\EnumerationArea", 'enumeration_areas_id');
    }

    public function user() {
        return $this->belongsTo("\App\User", 'users_id');
    }

    public function householdObservation() {
        return $this->hasOne("\App\Model\HouseholdObservation", "surveys_id");
    }

    public function getHouseholdSurveys() {
        $totalHouseholdFilled = DB::select('
        SELECT count(*) as count FROM (    
        SELECT count(*),household_observation_sheet_id FROM household_results as r
        join household_observation_sheet as s on s.id = r.household_observation_sheet_id
        where s.surveys_id = ? group by household_observation_sheet_id) householdObservationCount', [$this->id]);
        return $totalHouseholdFilled;
    }

    public function getHouseholdInformationSurveys() {
        $totalHouseholdInformationFilled = DB::select('
        SELECT count(*) as count FROM (   
        SELECT count(*),household_observation_sheet_id FROM household_information_results as r
        join household_observation_sheet as s on s.id = r.household_observation_sheet_id
        where s.surveys_id = ? group by household_observation_sheet_id,r.child_id) householdInformationCount', [$this->id]);
        return $totalHouseholdInformationFilled;
    }

    public function getSchoolInfo() {
        return $this->belongsTo("\App\Model\SchoolSurveyResult", "surveys_id");
    }

}
