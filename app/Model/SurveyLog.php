<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use \Illuminate\Database\Eloquent\Model;

/**
 * Description of SurveyLog
 *
 * @author Kayode
 */
class SurveyLog extends Model {

    protected $table = "survey_logs";
    protected $fillable = ["page", "user_id", "survey_id"];

}
