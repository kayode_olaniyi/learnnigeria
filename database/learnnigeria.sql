-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2018 at 03:36 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `learnnigeria`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `answer` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `answer`) VALUES
(1, 'Rural'),
(2, 'Urban'),
(3, 'Male'),
(4, 'Female'),
(5, '5-10 minutes walk'),
(6, '10-15 minutes walk'),
(7, '15-30 minutes walk'),
(8, '30-45 minutes walk'),
(9, 'More than 45 minutes walk'),
(10, 'Text'),
(11, 'Yes'),
(12, 'No'),
(13, 'Day School'),
(14, 'Boarding School'),
(15, 'Boarding school with special needs'),
(16, 'Day school with special needs'),
(17, 'Day and Boarding school'),
(18, ' Government school fees'),
(19, 'Parent-Teacher Associations'),
(20, 'Private Organizations'),
(21, 'NGOs/ Foundation'),
(22, 'Others(Please Specify)'),
(23, 'Grade II'),
(24, 'NCE'),
(25, 'B.Ed'),
(26, 'PGDE/PCE'),
(27, ' M.Ed'),
(28, 'Below WASSC/SSCE'),
(29, 'WASSCE/SSCE '),
(30, 'OND /Diploma/ND'),
(31, 'HND/Diploma'),
(32, 'BA/BSc'),
(33, 'MA/MSc'),
(34, 'Phd/M.Phil'),
(35, 'Security'),
(36, 'Pregranancy'),
(37, 'No Money'),
(38, 'Death'),
(39, 'Distance'),
(40, 'Others'),
(41, 'Solar Battery'),
(42, 'Generator'),
(43, 'Primary'),
(44, 'JSS'),
(45, 'SS'),
(46, 'Tetiary Education'),
(47, 'None');

-- --------------------------------------------------------

--
-- Table structure for table `ea_compilation_questions`
--

CREATE TABLE `ea_compilation_questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ea_compilation_results`
--

CREATE TABLE `ea_compilation_results` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `household_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ea_compilation_results`
--

INSERT INTO `ea_compilation_results` (`id`, `question_id`, `answer_id`, `text`, `household_id`) VALUES
(1, 1, 10, '2', 1),
(2, 2, 10, '2', 1),
(3, 3, 10, '2', 1),
(4, 4, 10, '2', 1),
(5, 5, 10, '2', 1),
(6, 6, 10, '2', 1),
(7, 7, 10, '2', 1),
(8, 8, 10, '2', 1),
(9, 9, 10, '2', 1),
(10, 10, 10, '2', 1),
(11, 11, 10, '2', 1),
(12, 12, 10, '2', 1),
(13, 13, 10, '2', 1),
(14, 14, 10, '2', 1),
(15, 15, 10, '2', 1),
(16, 16, 10, '2', 1),
(17, 17, 10, '2', 1),
(18, 18, 10, '2', 1),
(19, 19, 10, '2', 1),
(20, 20, 10, '2', 1),
(21, 21, 10, '2', 1),
(22, 22, 10, '2', 1),
(23, 23, 10, '2', 1),
(24, 24, 10, '2', 1),
(25, 25, 10, '2', 1),
(26, 26, 10, '2', 1),
(27, 27, 10, '2', 1),
(28, 28, 10, '2', 1),
(29, 29, 10, '2', 1),
(30, 30, 10, '2', 1),
(31, 31, 10, '2', 1),
(32, 32, 10, '2', 1),
(33, 33, 10, '2', 1),
(34, 34, 10, '2', 1),
(35, 35, 10, '2', 1),
(36, 36, 10, '2', 1),
(37, 37, 10, '2', 1),
(38, 38, 10, '2', 1),
(39, 39, 10, '2', 1),
(40, 40, 10, '2', 1),
(41, 41, 10, '2', 1),
(42, 42, 10, '2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ea_observation_results`
--

CREATE TABLE `ea_observation_results` (
  `id` int(11) NOT NULL,
  `observation_sheet_questions_id` int(11) NOT NULL,
  `observation_sheet_answers_id` int(11) NOT NULL,
  `text` longtext,
  `surveys_id` int(11) NOT NULL,
  `surveys_enumeration_areas_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ea_observation_results`
--

INSERT INTO `ea_observation_results` (`id`, `observation_sheet_questions_id`, `observation_sheet_answers_id`, `text`, `surveys_id`, `surveys_enumeration_areas_id`, `created_at`, `updated_at`) VALUES
(1, 1, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(2, 2, 5, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(3, 3, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(4, 4, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(5, 5, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(6, 6, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(7, 7, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(8, 8, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(9, 9, 5, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(10, 10, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(11, 11, 5, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(12, 12, 5, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(13, 13, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(14, 14, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(15, 15, 5, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(16, 16, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(17, 17, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(18, 18, 5, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(19, 20, 10, '20', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(20, 21, 10, '090', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(21, 22, 4, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(22, 23, 11, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(23, 24, 11, '', 9, 1, '2018-03-14 06:16:09', '2018-03-14 06:16:09'),
(24, 1, 4, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(25, 2, 5, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(26, 3, 5, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(27, 4, 4, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(28, 5, 4, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(29, 6, 4, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(30, 7, 5, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(31, 8, 5, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(32, 9, 6, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(33, 10, 4, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(34, 11, 6, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(35, 12, 5, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(36, 13, 5, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(37, 14, 4, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(38, 15, 5, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(39, 16, 5, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(40, 17, 4, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(41, 18, 4, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(42, 20, 10, 'ssss', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(43, 21, 10, 'ssss', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(44, 22, 4, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(45, 23, 11, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(46, 24, 11, '', 10, 5, '2018-03-14 21:32:17', '2018-03-14 21:32:17'),
(47, 1, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(48, 2, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(49, 3, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(50, 4, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(51, 5, 7, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(52, 6, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(53, 7, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(54, 8, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(55, 9, 5, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(56, 10, 7, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(57, 11, 5, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(58, 12, 5, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(59, 13, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(60, 14, 5, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(61, 15, 5, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(62, 16, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(63, 17, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(64, 18, 4, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(65, 20, 10, 'Dev', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(66, 21, 10, 'Boss', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(67, 22, 6, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(68, 23, 11, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(69, 24, 11, '', 11, 2, '2018-03-17 11:18:31', '2018-03-17 11:18:31');

-- --------------------------------------------------------

--
-- Table structure for table `enumeration_areas`
--

CREATE TABLE `enumeration_areas` (
  `id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `name` varchar(145) DEFAULT NULL,
  `local_government_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enumeration_areas`
--

INSERT INTO `enumeration_areas` (`id`, `code`, `name`, `local_government_id`, `users_id`, `created_at`, `status`, `updated_at`) VALUES
(1, 'BNAJA', 'ANANA', 2, 1, '2018-03-06 19:00:43', 1, '2018-03-06 19:11:16'),
(2, 'AKKA', 'ANANA', 2, 1, '2018-03-06 19:02:21', 0, '2018-03-06 19:02:21'),
(3, 'BABA', 'BANSNS', 2, 1, '2018-03-06 19:05:42', 0, '2018-03-06 19:05:42'),
(4, 'AKKAas', 'Olaniyi Olukayode', 2, 1, '2018-03-06 19:07:35', 0, '2018-03-06 19:07:35'),
(5, 'EA', 'AN EA', 3, 1, '2018-03-14 21:31:03', 0, '2018-03-14 21:31:03'),
(6, 'ApA', 'APA', 4, 1, '2018-03-18 19:22:43', 1, '2018-03-18 19:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `households`
--

CREATE TABLE `households` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `building_no` varchar(45) NOT NULL,
  `hh_number` varchar(45) NOT NULL,
  `household_head` text NOT NULL,
  `surveys_id` int(11) NOT NULL,
  `surveys_enumeration_areas_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `households`
--

INSERT INTO `households` (`id`, `address`, `building_no`, `hh_number`, `household_head`, `surveys_id`, `surveys_enumeration_areas_id`, `created_at`, `updated_at`) VALUES
(1, 'asassa', 'aasassa', 'asassa', 'saasas', 9, 1, '2018-03-14 06:17:17', '2018-03-14 06:17:17'),
(2, 'A real live', 'Boss', 'HHNo', 'Baababa', 10, 5, '2018-03-14 21:32:30', '2018-03-14 21:32:30'),
(3, 'A real live', 'Losos', 'HHNo', 'Baababa', 11, 2, '2018-03-17 11:18:46', '2018-03-17 11:18:46');

-- --------------------------------------------------------

--
-- Table structure for table `household_guardians`
--

CREATE TABLE `household_guardians` (
  `id` int(11) NOT NULL,
  `name` mediumtext NOT NULL,
  `age` varchar(45) NOT NULL,
  `children_number` varchar(45) NOT NULL,
  `ever_attended_school` int(11) NOT NULL,
  `highest_level_completed` varchar(100) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `household_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `household_guardians`
--

INSERT INTO `household_guardians` (`id`, `name`, `age`, `children_number`, `ever_attended_school`, `highest_level_completed`, `type`, `household_id`) VALUES
(1, 'Kayode Olaniyi', '21', '12', 11, 'yes', 1, 7),
(2, 'Kayode Olaniyi', '21', '12', 11, 'yes', 1, 8),
(3, 'Kayode Olaniyi', '12', '20', 11, 'no', 2, 8),
(4, 'jjjj', 'aaaa', 'aaaaa', 11, 'kkkk', 1, 11),
(5, 'jjjj', 'aaaa', 'aaaaa', 11, 'kkkk', 1, 12),
(6, 'jjjj', 'aaaa', 'aaaaa', 11, 'kkkk', 1, 13),
(7, 'jjjj', 'aaaa', 'aaaaa', 11, 'kkkk', 1, 14),
(8, 'aaa', 'sss', 'aaa', 12, 'sss', 1, 20),
(9, 'jjjjj', 'ggggg', 'hhhhh', 12, 'hhhhh', 1, 21),
(10, 'jjjjj', 'ggggg', 'hhhhh', 12, 'hhhhh', 1, 22),
(11, 'jjjjj', 'ggggg', 'hhhhh', 12, 'hhhhh', 1, 23);

-- --------------------------------------------------------

--
-- Table structure for table `household_information_questions`
--

CREATE TABLE `household_information_questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `household_information_results`
--

CREATE TABLE `household_information_results` (
  `id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `household_observation_sheet_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `household_information_results`
--

INSERT INTO `household_information_results` (`id`, `answer_id`, `text`, `household_observation_sheet_id`, `child_id`, `question_id`) VALUES
(1, 10, 'asassasa', 8, 1, 1),
(2, 3, '', 8, 1, 2),
(3, 3, '', 8, 1, 3),
(4, 72, '', 8, 1, 4),
(5, 79, '', 8, 1, 5),
(6, 80, '', 8, 1, 5),
(7, 81, '', 8, 1, 5),
(8, 97, '', 8, 1, 6),
(9, 75, '', 8, 1, 7),
(10, 76, '', 8, 1, 7),
(11, 77, '', 8, 1, 7),
(12, 10, '1088', 8, 1, 8),
(13, 86, '', 8, 1, 9),
(14, 102, '', 8, 1, 10),
(15, 10, '20', 8, 1, 11),
(16, 75, '', 8, 1, 12),
(17, 76, '', 8, 1, 12),
(18, 77, '', 8, 1, 12),
(19, 78, '', 8, 1, 12),
(20, 11, '', 8, 1, 13),
(21, 103, '', 8, 1, 14),
(22, 11, '', 8, 1, 15),
(23, 10, '20', 8, 1, 16),
(24, 10, '20', 8, 1, 17),
(25, 10, '20', 8, 1, 18),
(26, 10, '20', 8, 1, 19),
(27, 10, '20', 8, 1, 20),
(28, 10, '20', 8, 1, 21),
(29, 10, '20', 8, 1, 22),
(30, 11, '', 8, 1, 23),
(31, 105, '', 8, 1, 24),
(32, 106, '', 8, 1, 24),
(33, 107, '', 8, 1, 24),
(34, 11, '', 8, 1, 25),
(35, 111, '', 8, 1, 26),
(36, 112, '', 8, 1, 26),
(37, 113, '', 8, 1, 26),
(38, 10, '20', 8, 1, 27),
(39, 116, '', 8, 1, 28),
(40, 120, '', 8, 1, 29),
(41, 120, '', 8, 1, 30),
(42, 122, '', 8, 1, 31),
(43, 116, '', 8, 1, 32),
(44, 120, '', 8, 1, 33),
(45, 120, '', 8, 1, 34),
(46, 125, '', 8, 1, 35),
(47, 132, '', 8, 1, 36),
(48, 134, '', 8, 1, 37),
(49, 135, '', 8, 1, 37),
(50, 136, '', 8, 1, 37),
(51, 10, 'asassasa', 8, 1, 1),
(52, 3, '', 8, 1, 2),
(53, 3, '', 8, 1, 3),
(54, 72, '', 8, 1, 4),
(55, 22, 'yes', 8, 1, 5),
(56, 86, '', 8, 1, 6),
(57, 78, '', 8, 1, 7),
(58, 79, '', 8, 1, 7),
(59, 80, '', 8, 1, 7),
(60, 81, '', 8, 1, 7),
(61, 22, 'no', 8, 1, 7),
(62, 10, '2018', 8, 1, 8),
(63, 87, '', 8, 1, 9),
(64, 101, '', 8, 1, 10),
(65, 10, '20', 8, 1, 11),
(66, 82, '', 8, 1, 12),
(67, 83, '', 8, 1, 12),
(68, 22, 'shhshs', 8, 1, 12),
(69, 11, '', 8, 1, 13),
(70, 103, '', 8, 1, 14),
(71, 11, '', 8, 1, 15),
(72, 10, '20', 8, 1, 16),
(73, 10, '20', 8, 1, 17),
(74, 10, '20', 8, 1, 18),
(75, 10, '20', 8, 1, 19),
(76, 10, '20', 8, 1, 20),
(77, 10, '20', 8, 1, 21),
(78, 10, '20', 8, 1, 22),
(79, 11, '', 8, 1, 23),
(80, 106, '', 8, 1, 24),
(81, 107, '', 8, 1, 24),
(82, 11, '', 8, 1, 25),
(83, 10, '20', 8, 1, 27),
(84, 115, '', 8, 1, 28),
(85, 120, '', 8, 1, 29),
(86, 120, '', 8, 1, 30),
(87, 122, '', 8, 1, 31),
(88, 115, '', 8, 1, 32),
(89, 120, '', 8, 1, 33),
(90, 120, '', 8, 1, 34),
(91, 125, '', 8, 1, 35),
(92, 132, '', 8, 1, 36),
(93, 135, '', 8, 1, 37),
(94, 136, '', 8, 1, 37),
(95, 10, 'Dev', 3, 1, 1),
(96, 4, '', 3, 1, 2),
(97, 3, '', 3, 1, 3),
(98, 72, '', 3, 1, 4),
(99, 79, '', 3, 1, 5),
(100, 80, '', 3, 1, 5),
(101, 101, '', 3, 1, 10),
(102, 10, 'jjjj', 3, 1, 11),
(103, 75, '', 3, 1, 12),
(104, 76, '', 3, 1, 12),
(105, 11, '', 3, 1, 13),
(106, 103, '', 3, 1, 14),
(107, 11, '', 3, 1, 15),
(108, 10, 'sss', 3, 1, 16),
(109, 10, 'ssss', 3, 1, 17),
(110, 10, 'sss', 3, 1, 18),
(111, 10, 'Dev', 3, 1, 19),
(112, 10, 'Dev', 3, 1, 20),
(113, 10, 'Dev', 3, 1, 21),
(114, 10, 'Dev', 3, 1, 22),
(115, 11, '', 3, 1, 23),
(116, 108, '', 3, 1, 24),
(117, 109, '', 3, 1, 24),
(118, 11, '', 3, 1, 25),
(119, 111, '', 3, 1, 26),
(120, 112, '', 3, 1, 26),
(121, 113, '', 3, 1, 26),
(122, 10, 'Dev', 3, 1, 27),
(123, 115, '', 3, 1, 28),
(124, 120, '', 3, 1, 29),
(125, 120, '', 3, 1, 30),
(126, 123, '', 3, 1, 31),
(127, 115, '', 3, 1, 32),
(128, 120, '', 3, 1, 33),
(129, 120, '', 3, 1, 34),
(130, 130, '', 3, 1, 35),
(131, 132, '', 3, 1, 36),
(132, 134, '', 3, 1, 37),
(133, 135, '', 3, 1, 37),
(134, 136, '', 3, 1, 37),
(135, 10, 'Dev', 3, 2, 1),
(136, 4, '', 3, 2, 2),
(137, 3, '', 3, 2, 3),
(138, 72, '', 3, 2, 4),
(139, 81, '', 3, 2, 5),
(140, 83, '', 3, 2, 5),
(141, 102, '', 3, 2, 10),
(142, 10, 'jjjj', 3, 2, 11),
(143, 75, '', 3, 2, 12),
(144, 76, '', 3, 2, 12),
(145, 12, '', 3, 2, 13),
(146, 103, '', 3, 2, 14),
(147, 11, '', 3, 2, 15),
(148, 10, 'sss', 3, 2, 16),
(149, 10, 'ssss', 3, 2, 17),
(150, 10, 'Dev', 3, 2, 18),
(151, 10, 'Dev', 3, 2, 19),
(152, 10, 'Dev', 3, 2, 20),
(153, 10, 'Dev', 3, 2, 21),
(154, 10, 'sss', 3, 2, 22),
(155, 11, '', 3, 2, 23),
(156, 106, '', 3, 2, 24),
(157, 107, '', 3, 2, 24),
(158, 108, '', 3, 2, 24),
(159, 11, '', 3, 2, 25),
(160, 10, 'Dev', 3, 2, 27),
(161, 116, '', 3, 2, 28),
(162, 121, '', 3, 2, 29),
(163, 120, '', 3, 2, 30),
(164, 122, '', 3, 2, 31),
(165, 115, '', 3, 2, 32),
(166, 120, '', 3, 2, 33),
(167, 121, '', 3, 2, 34),
(168, 125, '', 3, 2, 35),
(169, 133, '', 3, 2, 36),
(170, 134, '', 3, 2, 37),
(171, 135, '', 3, 2, 37),
(172, 10, 'Dev', 3, 3, 1),
(173, 3, '', 3, 3, 2),
(174, 3, '', 3, 3, 3),
(175, 72, '', 3, 3, 4),
(176, 83, '', 3, 3, 5),
(177, 102, '', 3, 3, 10),
(178, 10, 'jjjj', 3, 3, 11),
(179, 83, '', 3, 3, 12),
(180, 84, '', 3, 3, 12),
(181, 12, '', 3, 3, 13),
(182, 103, '', 3, 3, 14),
(183, 11, '', 3, 3, 15),
(184, 10, 'sss', 3, 3, 16),
(185, 10, 'ssss', 3, 3, 17),
(186, 10, 'Dev', 3, 3, 18),
(187, 10, 'Dev', 3, 3, 19),
(188, 10, 'Dev', 3, 3, 20),
(189, 10, 'Dev', 3, 3, 21),
(190, 10, 'sss', 3, 3, 22),
(191, 11, '', 3, 3, 23),
(192, 106, '', 3, 3, 24),
(193, 11, '', 3, 3, 25),
(194, 111, '', 3, 3, 26),
(195, 112, '', 3, 3, 26),
(196, 10, 'ssss', 3, 3, 27),
(197, 116, '', 3, 3, 28),
(198, 120, '', 3, 3, 29),
(199, 120, '', 3, 3, 30),
(200, 122, '', 3, 3, 31),
(201, 116, '', 3, 3, 32),
(202, 120, '', 3, 3, 33),
(203, 120, '', 3, 3, 34),
(204, 130, '', 3, 3, 35),
(205, 132, '', 3, 3, 36),
(206, 134, '', 3, 3, 37),
(207, 135, '', 3, 3, 37),
(208, 136, '', 3, 3, 37),
(209, 10, 'Dev', 3, 3, 1),
(210, 3, '', 3, 3, 2),
(211, 3, '', 3, 3, 3),
(212, 72, '', 3, 3, 4),
(213, 83, '', 3, 3, 5),
(214, 102, '', 3, 3, 10),
(215, 10, 'jjjj', 3, 3, 11),
(216, 83, '', 3, 3, 12),
(217, 84, '', 3, 3, 12),
(218, 12, '', 3, 3, 13),
(219, 103, '', 3, 3, 14),
(220, 11, '', 3, 3, 15),
(221, 10, 'sss', 3, 3, 16),
(222, 10, 'ssss', 3, 3, 17),
(223, 10, 'Dev', 3, 3, 18),
(224, 10, 'Dev', 3, 3, 19),
(225, 10, 'Dev', 3, 3, 20),
(226, 10, 'Dev', 3, 3, 21),
(227, 10, 'sss', 3, 3, 22),
(228, 11, '', 3, 3, 23),
(229, 106, '', 3, 3, 24),
(230, 11, '', 3, 3, 25),
(231, 111, '', 3, 3, 26),
(232, 112, '', 3, 3, 26),
(233, 10, 'ssss', 3, 3, 27),
(234, 116, '', 3, 3, 28),
(235, 120, '', 3, 3, 29),
(236, 120, '', 3, 3, 30),
(237, 122, '', 3, 3, 31),
(238, 116, '', 3, 3, 32),
(239, 120, '', 3, 3, 33),
(240, 120, '', 3, 3, 34),
(241, 130, '', 3, 3, 35),
(242, 132, '', 3, 3, 36),
(243, 134, '', 3, 3, 37),
(244, 135, '', 3, 3, 37),
(245, 136, '', 3, 3, 37),
(246, 10, 'Dev', 3, 4, 1),
(247, 3, '', 3, 4, 2),
(248, 4, '', 3, 4, 3),
(249, 74, '', 3, 4, 4),
(250, 86, '', 3, 4, 9),
(251, 102, '', 3, 4, 10),
(252, 10, 'jjjj', 3, 4, 11),
(253, 75, '', 3, 4, 12),
(254, 11, '', 3, 4, 13),
(255, 103, '', 3, 4, 14),
(256, 11, '', 3, 4, 15),
(257, 10, 'sss', 3, 4, 16),
(258, 10, 'ssss', 3, 4, 17),
(259, 10, 'Dev', 3, 4, 18),
(260, 10, 'Dev', 3, 4, 19),
(261, 10, 'Dev', 3, 4, 20),
(262, 10, 'Dev', 3, 4, 21),
(263, 10, 'Dev', 3, 4, 22),
(264, 11, '', 3, 4, 23),
(265, 105, '', 3, 4, 24),
(266, 12, '', 3, 4, 25),
(267, 10, 'Dev', 3, 4, 27),
(268, 116, '', 3, 4, 28),
(269, 120, '', 3, 4, 29),
(270, 120, '', 3, 4, 30),
(271, 122, '', 3, 4, 31),
(272, 115, '', 3, 4, 32),
(273, 120, '', 3, 4, 33),
(274, 120, '', 3, 4, 34),
(275, 130, '', 3, 4, 35),
(276, 133, '', 3, 4, 36),
(277, 134, '', 3, 4, 37),
(278, 135, '', 3, 4, 37);

-- --------------------------------------------------------

--
-- Table structure for table `household_observation_sheet`
--

CREATE TABLE `household_observation_sheet` (
  `id` int(11) NOT NULL,
  `building_no` varchar(245) NOT NULL,
  `surveys_id` int(11) NOT NULL,
  `surveys_enumeration_areas_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `household_observation_sheet`
--

INSERT INTO `household_observation_sheet` (`id`, `building_no`, `surveys_id`, `surveys_enumeration_areas_id`) VALUES
(1, '+2348148380710', 6, 1),
(2, '+2348148380710', 8, 1),
(3, '+2348148380710', 8, 1),
(4, '+2348148380710', 8, 1),
(5, '+2348148380710', 8, 1),
(6, '+2348148380710', 8, 1),
(7, '+2348148380710', 8, 1),
(8, '+2348148380710', 8, 1),
(9, 'Dev', 5, 10),
(10, 'Dev', 5, 10),
(11, 'Dev', 5, 10),
(12, 'Dev', 11, 2),
(13, 'Dev', 11, 2),
(14, 'Dev', 11, 2),
(15, 'Dev', 14, 6),
(16, 'Dev', 14, 6),
(17, 'Dev', 14, 6),
(18, 'Dev', 14, 6),
(19, 'Dev', 14, 6),
(20, 'A', 14, 6),
(21, 'gg', 14, 6),
(22, 'gg', 14, 6),
(23, 'gg', 14, 6);

-- --------------------------------------------------------

--
-- Table structure for table `household_questions`
--

CREATE TABLE `household_questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `type` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `household_questions`
--

INSERT INTO `household_questions` (`id`, `question`, `type`) VALUES
(1, 'Date of Survey', 1),
(2, 'Arrival Time', 1),
(3, 'Time of Departure', 1),
(4, 'HH Code ', 1),
(5, 'Building number', 1),
(6, 'Full Name of the Household Head', 1),
(7, 'Age of the Household Head', 1),
(8, 'Occupation of Household head', 1),
(9, 'Sex', 1),
(10, 'Address', 1),
(11, 'Telephone Number', 1),
(12, 'Level of Education ', 2),
(13, 'Number of household members (Ask) (who eat together from the same pot at least 4 times a week and have the same Head of household)(Male)', 1),
(14, 'Number of household members (Ask) (who eat together from the same pot at least 4 times a week and have the same Head of household)(Female)', 1),
(15, 'What kind of walls does the house have? ', 2),
(16, 'Is the house owned or rented?', 2),
(17, 'Main lighting used in the house', 2),
(18, 'Main language spoken in the household? ', 2),
(19, 'Main source of water used in the household?', 2),
(20, 'Parents’ Involvement in School Activities ', 2),
(21, 'Do you attend PTA meetings', 1),
(22, 'Are you aware of any SBMC in your neighborhood', 1),
(23, 'If yes, are you involved in SBMC activities', 2),
(24, 'How far to your home is the school of your child(ren) who attends the most distant school? ', 2),
(25, 'Who among the following is/are involved with your child(ren)’s homework', 2),
(26, 'Regularly, how many meals are cooked in the household everyday? ', 1),
(27, 'Do children eat breakfast everyday? ', 2),
(28, 'Do you eat vegetables everyday? ', 2),
(29, 'Do you eat fruits everyday?', 2),
(30, 'Do you eat meat or fish everyday?', 2),
(31, 'Does the HH have the following?(TV)', 2),
(32, 'Does the HH have the following?(Computer)', 2),
(33, 'Does the HH have the following?(Radio)', 2),
(34, 'Does the HH have the following?(Telephone)', 2),
(35, 'Does the HH have the following?(Animals)', 2),
(36, 'Does the HH have the following?(Pets)', 2),
(37, 'Does any member of the HH have the following means of transportation?(Motor Vehicle)', 2),
(38, 'Does any member of the HH have the following means of transportation?(Motor Bike)', 2),
(39, 'Does any member of the HH have the following means of transportation?(Bicycle)', 2),
(40, 'Does any member of the HH have the following means of transportation?(Motorized Three Wheeler)', 2),
(41, 'Does any member of the HH have the following means of transportation?(Camels, Donkey and Horse)', 2),
(42, 'Does any member of the HH have the following means of transportation?(Boat/Canoe)', 2),
(43, 'Name', 1),
(44, 'Total number of children', 1),
(45, 'Age', 1),
(46, 'Ever attended School', 2),
(47, 'If yes highest level completed', 1),
(48, 'Name', 1),
(49, 'Total Number of children', 1),
(50, 'Age', 1),
(51, 'Ever attended School', 2),
(52, 'If yes highest level completed', 1),
(53, 'Main source of water used in the household?(Others)', 1);

-- --------------------------------------------------------

--
-- Table structure for table `household_results`
--

CREATE TABLE `household_results` (
  `id` int(11) NOT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `household_questions_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `household_observation_sheet_id` int(11) NOT NULL,
  `household_observation_sheet_surveys_id` int(11) NOT NULL,
  `household_observation_sheet_surveys_enumeration_areas_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `household_results`
--

INSERT INTO `household_results` (`id`, `answer_id`, `household_questions_id`, `text`, `household_observation_sheet_id`, `household_observation_sheet_surveys_id`, `household_observation_sheet_surveys_enumeration_areas_id`) VALUES
(1, 10, 1, 'gg', 23, 14, 6),
(2, 10, 2, 'gg', 23, 14, 6),
(3, 10, 3, 'gg', 23, 14, 6),
(4, 10, 4, 'gg', 23, 14, 6),
(5, 10, 5, 'gg', 23, 14, 6),
(6, 10, 6, 'ggg', 23, 14, 6),
(7, 10, 7, 'gg', 23, 14, 6),
(8, 10, 8, 'gg', 23, 14, 6),
(9, 10, 9, '3', 23, 14, 6),
(10, 10, 10, 'nnnn', 23, 14, 6),
(11, 10, 11, 'jjjj', 23, 14, 6),
(12, 46, 12, '', 23, 14, 6),
(13, 10, 13, 'uuu', 23, 14, 6),
(14, 10, 14, 'uuuu', 23, 14, 6),
(15, 49, 15, '', 23, 14, 6),
(16, 53, 16, '', 23, 14, 6),
(17, 150, 17, '', 23, 14, 6),
(18, 10, 18, 'kkjj', 23, 14, 6),
(19, 59, 19, '', 23, 14, 6),
(20, 10, 21, 'Yes', 23, 14, 6),
(21, 10, 22, 'Yes', 23, 14, 6),
(22, 11, 23, '', 23, 14, 6),
(23, 12, 24, '', 23, 14, 6),
(24, 66, 25, '', 23, 14, 6),
(25, 10, 26, 'Dev', 23, 14, 6),
(26, 11, 27, '', 23, 14, 6),
(27, 12, 28, '', 23, 14, 6),
(28, 12, 29, '', 23, 14, 6),
(29, 11, 30, '', 23, 14, 6),
(30, 11, 31, '', 23, 14, 6),
(31, 11, 32, '', 23, 14, 6),
(32, 11, 33, '', 23, 14, 6),
(33, 11, 34, '', 23, 14, 6),
(34, 11, 35, '', 23, 14, 6),
(35, 11, 36, '', 23, 14, 6),
(36, 11, 37, '', 23, 14, 6),
(37, 11, 38, '', 23, 14, 6),
(38, 11, 39, '', 23, 14, 6),
(39, 11, 40, '', 23, 14, 6),
(40, 11, 41, '', 23, 14, 6),
(41, 11, 42, '', 23, 14, 6);

-- --------------------------------------------------------

--
-- Table structure for table `local_governments`
--

CREATE TABLE `local_governments` (
  `id` int(11) NOT NULL,
  `name` varchar(245) NOT NULL,
  `code` varchar(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `states_id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `status` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `local_governments`
--

INSERT INTO `local_governments` (`id`, `name`, `code`, `created_at`, `updated_at`, `states_id`, `users_id`, `status`) VALUES
(1, 'Alimosho', 'AL', '2018-03-03 14:37:29', '2018-03-06 19:07:59', 1, 1, 0),
(2, 'Ijebu North', 'IJBN', '2018-03-03 14:37:43', '2018-03-03 15:31:40', 2, 1, 1),
(3, 'Ibadan', 'IBD', '2018-03-14 21:29:59', '2018-03-14 21:30:28', 3, 1, 1),
(4, 'Apapa', 'APA', '2018-03-18 19:08:51', '2018-03-18 19:08:57', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `observation_sheet_questions`
--

CREATE TABLE `observation_sheet_questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `section` int(11) NOT NULL,
  `summary` text,
  `type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `observation_sheet_questions`
--

INSERT INTO `observation_sheet_questions` (`id`, `question`, `section`, `summary`, `type`) VALUES
(1, 'Supermarket / market center of 5 or more shops', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(2, 'Bank', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(3, 'Religious Building? (Government/Private)', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(4, 'Tarred road?', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(5, 'All weather road?', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(6, 'Access to portable water?', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(7, 'Internet Cafe?', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(8, 'Town Hall/Village Square', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(9, 'Police Station?', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(10, 'Adult Literacy Learning Center?', 1, 'General Facilities(Ask) How far are one of the following facilities to the EA?', 3),
(11, 'Public/Primary School/s?', 2, 'Education Status(Ask) How far are the following to the EA', 3),
(12, 'Private Primary Schools?', 2, 'Education Status(Ask) How far are the following to the EA', 3),
(13, 'Public Junior Secondary School/s?', 2, 'Education Status(Ask) How far are the following to the EA', 3),
(14, 'Private Junior Secondary School/s?', 2, 'Education Status(Ask) How far are the following to the EA', 3),
(15, 'Public Senior Secondary School/s?', 2, 'Education Status(Ask) How far are the following to the EA', 3),
(16, 'Private Senior Secondary School/s?', 2, 'Education Status(Ask) How far are the following to the EA', 3),
(17, 'Public Tetiary Institution?', 2, 'Education Status(Ask) How far are the following to the EA', 3),
(18, 'Private Tetiary Institution', 2, 'Education Status(Ask) How far are the following to the EA', 3),
(19, 'How far from this community is the closest health facility?(Ask) (Clinic/Hospital)', 4, 'Health Status', 3),
(20, 'What is the main economic activity in the EA', 3, '', 1),
(21, 'What is predominant local language', 3, '', 1),
(22, 'How far from this community is the closest health facility?(Ask) (Clinic/Hospital)', 4, 'Health Status', 3),
(23, 'Has any health worker visited the EA in the last 3 Months', 5, '', 2),
(24, 'Is there a trained health worker residing in the EA', 5, '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(45) NOT NULL,
  `title` varchar(145) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `school_survey_questions`
--

CREATE TABLE `school_survey_questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `type` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school_survey_questions`
--

INSERT INTO `school_survey_questions` (`id`, `question`, `type`) VALUES
(1, 'Arrival Time', 1),
(2, 'Time of Departure', 1),
(3, 'Respondent Name', 1),
(4, 'Respondent Designation', 1),
(5, 'Respondent Telephone Number', 1),
(6, 'School Name', 1),
(7, 'School Code', 1),
(8, 'School Email Address', 1),
(9, 'Headteacher’s Name', 1),
(10, 'Sex', 2),
(11, 'Is there a school-based management committee?', 2),
(12, 'Is there PTA/Parents association?', 2),
(13, 'Address of School', 1),
(14, 'Headteacher Telephone\'s Number', 1),
(15, 'Is Headteacher’s consent signature', 2),
(16, 'Type of School', 3),
(17, 'What are the school\'s sources of funding and resources? (Tick all that apply)', 4),
(18, 'Enrolment(Take from register)(Boys)', 1),
(19, 'Attendance on day of visit* (HeadCount)(Boys)', 1),
(20, 'Enrolment (Take from register)(Primary1)', 1),
(21, 'Enrolment (Take from register)(Primary2)', 1),
(22, 'Enrolment (Take from register)(Primary3)', 1),
(23, 'Attendance on day of visit(Headcount)(Primary1)', 1),
(24, 'Attendance on day of visit(Headcount)(Primary2)', 1),
(25, 'Attendance on day of visit(Headcount)(Primary3)', 1),
(26, 'Number of Streams/Arms(Primary1)', 1),
(27, 'Number of Streams/Arms(Primary2)', 1),
(28, 'Number of Streams/Arms(Primary3)', 1),
(29, 'Enrolment(Take from register)(Girls)', 1),
(30, 'Attendance on day of visit* (HeadCount)(Girls)', 1),
(31, 'Enrolment (Take from register)(Primary 4)', 1),
(32, 'Enrolment (Take from register)(Primary5)', 1),
(33, 'Enrolment (Take from register)(Primary 6)', 1),
(34, 'Attendance on day of visit(Headcount)(Primary 4)', 1),
(35, 'Attendance on day of visit(Headcount)(Primary 5)', 1),
(36, 'Attendance on day of visit(Headcount)(Priimary 6)', 1),
(37, 'Number of Streams/Arms(Primary 4)', 1),
(38, 'Number of Streams/Arms(Primary 5)', 1),
(39, 'Number of Streams/Arms(Primary 6)', 1),
(40, 'Are the pupils of this class sitting with children from another class?(Primary2)', 2),
(41, 'Are the pupils of this class sitting with children from another class?(Primary 4)', 2),
(42, 'Did most pupils (75%) have textbooks? (Primary 2)', 2),
(43, 'Did most pupils (75%) have textbooks? (Primary 4)', 2),
(44, 'Where were the children seated?(Primary 2)', 2),
(45, 'Where were the children seated?(Primary 4)', 2),
(46, 'Veranda(Primary 2)', 2),
(47, 'Veranda(Primary 4)', 2),
(48, 'Is there a usable blackboard/white board in the class (Primary 2)', 2),
(49, 'Is there a usable blackboard/white board in the class(Primary 4)', 2),
(50, 'Apart from textbooks, did you see any other supplementary material in the room? (Primary 2)', 2),
(51, 'Apart from textbooks, did you see any other supplementary material in the room?(Primary 4)', 2),
(52, 'Classroom?(Primary 2)', 2),
(53, 'Classroom?(Primary 4)', 2),
(54, 'Comments', 1),
(109, 'Professional', 4),
(110, 'Education', 4),
(111, 'Number of Teaching Staff', 1),
(112, 'Number of teachers who have attended professional trainings, workshops and seminars (Government sponsored/Private)(Within the Past 12 Months)', 1),
(113, 'Number of teachers who have attended professional trainings, workshops and seminars (Government sponsored/Private)(More than 12 Months)', 1),
(114, 'Total number of pre-primary school teachers present on day of visit  (do a self count)(Male)', 1),
(115, 'Total number of pre-primary school teachers present on day of visit  (do a self count)(Female)', 1),
(116, 'Total number of primary school teachers(Male)', 1),
(117, 'Total number of primary school teachers(Female)', 1),
(118, 'Total number of primary school teachers present on the day of the visit  (do a self count)(Male)', 1),
(119, 'Total number of primary school teachers present on the day of the visit  (do a self count)(Female)', 1),
(120, 'Number of non-teaching staff(Male)', 1),
(121, 'Number of non-teaching staff(Female)', 1),
(122, 'Number of teachers appointed by Governmnet (include the Headteacher)(Male)', 1),
(123, 'Number of teachers appointed by Governmnet (include the Headteacher)(Female)', 1),
(124, 'Number of primary teachers with no teacher training certificate(Male)', 1),
(125, 'Number of primary teachers with no teacher training certificate(Female)', 1),
(126, 'Other trained teachers employed by parents, sponsors, donors, community etc(Male)', 1),
(127, 'Other trained teachers employed by parents, sponsors, donors, community etc(Female)', 1),
(128, 'Number of NYSC members, N-Power teachers, teachers who are donor funded or community volunteers etc.(Male)', 1),
(129, 'Number of NYSC members, N-Power teachers, teachers who are donor funded or community volunteers etc.(Female)', 1),
(130, 'Does the school have a stocked first aid kit?', 2),
(131, 'Is there a school nurse?', 2),
(132, 'Is there a school feeding programme?', 2),
(133, 'How far is the nearest public health facility where children receive medical attention?', 2),
(134, 'How many children enrolled but stopped attending school?', 2),
(135, 'Identify a common reason why most of them stopped attending ', 2),
(136, 'Is there a functional computer lab in the school', 4),
(137, 'Does the school have functioning Internet access? ', 4),
(138, 'How many functional computers are available for use by the students? (Observe and write number) ', 1),
(139, 'How many teachers have basic training in computer applications? ', 1),
(140, 'Do you offer computer lessons to Pupils', 2),
(141, 'Do you offer computer lessons to Teachers', 2),
(142, 'Do you offer computer lessons to Community Members', 2),
(143, 'Source of water mainly used by the school?', 2),
(144, 'Is the water source within the school compound?', 2),
(145, 'Is there a functional hand-washing (Water and Soap) facility in your school', 2),
(146, 'Is the hand-washing facility close to the toilet?', 2),
(147, 'How many functional toilets / latrines for teachers?(Enter number)(Male)', 1),
(148, 'How many functional toilets / latrines for teachers?(Enter number)(Female)', 1),
(149, 'How many functional toilets / latrines for pupils?(Enter number)(Male)', 1),
(150, 'How many functional toilets / latrines for pupils?(Female)', 1),
(151, 'Total number of classrooms(do a self-count)', 1),
(152, 'Total number of classrooms currently being used by the pupils (do a self-count)', 1),
(153, 'Is there a complete boundary wall/fence? (The boundary wall/fence should not be broken/damaged', 2),
(154, 'Is there a playground in the school?', 2),
(155, 'Did you see a security personnel at the entrance of the school? E.g. a gateman', 2),
(156, 'Did you see a library with books in the school?', 2),
(157, 'Lightening in School', 4),
(158, 'Did you see a science laboratory in the school?', 2),
(159, 'Did you see TV(s) in the school?', 2),
(160, 'Did you see radio(s) in school?', 2),
(161, 'Does the school have a phone?', 2),
(162, 'Did you see a Videodeck/DVD player(s) in the school?', 2),
(163, 'Number of teachers who have attended professional trainings, workshops and seminars (Government sponsored/Private)(Never)', 1),
(164, 'Total number of pre-primary school teachers(Male)', 1),
(165, 'Total number of pre-primary school teachers(Female)', 1),
(166, 'Enrolment (Take from register)(Boys)', 1),
(167, 'Enrolment (Take from register)(Girls)', 1),
(168, 'Attendance on day of visit (Headcount)(Boys)', 1),
(169, 'Attendance on day of visit (Headcount)(Girls)', 1),
(170, 'Education(Below Wasce)', 1),
(171, 'WASSCE/SSCE', 1),
(172, 'OND', 1),
(173, 'HND', 1),
(174, 'BA/BSc', 1),
(175, 'MA/MSc/M.Phil', 1),
(176, 'PhD', 1),
(177, 'Others', 1),
(178, 'Grade II', 1),
(179, 'NCE', 1),
(180, 'B.Ed', 1),
(181, 'PGDE/PCE', 1),
(182, 'M.Ed', 1),
(183, 'Others', 1),
(184, 'S301. Enrolment(Take from register)(Total)', 1),
(185, 'S302. Attendance on day of visit* (HeadCount)(Total)', 1),
(186, 'Is there a functional computer lab in the school(Text)', 1),
(187, 'Does the school have functioning Internet access?(Text)', 1);

-- --------------------------------------------------------

--
-- Table structure for table `school_survey_results`
--

CREATE TABLE `school_survey_results` (
  `id` int(11) NOT NULL,
  `text` varchar(45) DEFAULT NULL,
  `school_survey_questions_id` int(11) NOT NULL,
  `answers_id` int(11) NOT NULL,
  `surveys_id` int(11) NOT NULL,
  `surveys_enumeration_areas_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `school_type` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school_survey_results`
--

INSERT INTO `school_survey_results` (`id`, `text`, `school_survey_questions_id`, `answers_id`, `surveys_id`, `surveys_enumeration_areas_id`, `created_at`, `updated_at`, `school_type`) VALUES
(1, NULL, 109, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(2, NULL, 110, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(3, NULL, 111, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(4, '010', 112, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(5, '10', 113, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(6, '10', 114, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(7, '01', 115, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(8, '010', 116, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(9, '10', 117, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(10, '10', 118, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(11, '0', 119, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(12, '10', 120, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(13, '10', 121, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(14, NULL, 122, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(15, NULL, 123, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(16, '10', 124, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(17, '0', 125, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(18, '10', 126, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(19, '10', 127, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(20, '10', 128, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(21, '10', 129, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(22, '', 130, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(23, '', 131, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(24, '', 132, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(25, '', 133, 4, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(26, '', 134, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(27, '', 135, 35, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(28, '', 136, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(29, '', 137, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(30, '20', 138, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(31, '22', 139, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(32, '', 140, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(33, '', 141, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(34, '', 142, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(35, '', 143, 142, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(36, '', 144, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(37, '', 145, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(38, '', 146, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(39, '10', 147, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(40, '10', 148, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(41, '10', 149, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(42, '10', 150, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(43, '10', 151, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(44, '10', 152, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(45, '', 153, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(46, '', 154, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(47, '', 155, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(48, '', 156, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(49, '', 157, 41, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(50, '', 157, 42, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(51, 'ssss', 157, 22, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(52, '', 158, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(53, '', 159, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(54, '', 160, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(55, '', 161, 11, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(56, '', 162, 12, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(57, '10', 163, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(58, '01', 164, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(59, '010', 165, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(60, '10', 170, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(61, '10', 171, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(62, '10', 172, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(63, '10', 173, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(64, '10', 174, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(65, '01', 175, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(66, '01', 176, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(67, '10', 177, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(68, '10', 178, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(69, '01', 179, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(70, '01', 180, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(71, '010', 181, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(72, '01', 182, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(73, '010', 183, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(74, '20', 186, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2),
(75, '20', 187, 10, 14, 6, '2018-03-24 14:25:07', '2018-03-24 14:25:07', 2);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `code` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `code`) VALUES
(1, 'Lagos', 'LAG'),
(2, 'Oyo', 'OYO'),
(3, 'Ogun', 'OG'),
(4, 'Abuja', 'ABJ');

-- --------------------------------------------------------

--
-- Table structure for table `supervisors`
--

CREATE TABLE `supervisors` (
  `id` int(11) NOT NULL,
  `name` varchar(245) NOT NULL,
  `sex` int(1) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `surveys_id` int(11) NOT NULL,
  `surveys_enumeration_areas_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supervisors`
--

INSERT INTO `supervisors` (`id`, `name`, `sex`, `mobile`, `code`, `surveys_id`, `surveys_enumeration_areas_id`) VALUES
(1, 'Love', 4, '091919111', 'Lol', 1, 2),
(2, 'ssss', 3, 'ssss', 'sss', 2, 2),
(3, 'aassasa', 3, 'sasasasa', 'asasas', 3, 2),
(4, 'A name', 3, '091919111', 'Lol', 4, 2),
(5, 'AMAMA', 3, 'aaaa', 'aaaa', 5, 1),
(6, 'assasa', 3, 'assasa', 'assasa', 6, 1),
(7, 'Kayode Olaniyi', 3, '+2348148380710', 'asassa', 7, 1),
(8, 'Kayode Olaniyi', 3, '+2348148380710', 'asas', 8, 1),
(9, 'Kayode Olaniyi', 3, '+2348148380710', '0go', 9, 1),
(10, 'A name', 3, '091919111', 'Lol', 10, 5),
(11, 'A name', 3, '091919111', 'Lol', 11, 2),
(12, 's', 4, 's', 's', 12, 2),
(13, 'aassasa', 3, '091919111', 'Lol', 13, 6),
(14, 'A name', 4, '091919111', 'Lol', 14, 6);

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE `surveys` (
  `id` int(11) NOT NULL,
  `sector` varchar(145) NOT NULL,
  `resident_name` varchar(145) NOT NULL,
  `resident_phone_number` varchar(145) NOT NULL,
  `survey_date` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `locality` varchar(45) DEFAULT NULL,
  `ric` varchar(45) DEFAULT NULL,
  `enumeration_areas_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `sector`, `resident_name`, `resident_phone_number`, `survey_date`, `status`, `locality`, `ric`, `enumeration_areas_id`, `users_id`) VALUES
(1, '1', 'Another Name', 'A Name', '2018-03-09', 1, NULL, NULL, 0, 0),
(2, '1', 'sss', 'sss', '2018-03-09', 0, NULL, NULL, 0, 0),
(3, '1', 'saassasa', 'assasas', '2018-03-09', 0, NULL, NULL, 0, 0),
(4, '1', 'Bans', 'Bans', '2018-03-11', 0, NULL, NULL, 0, 0),
(5, '1', 'sssss', 'ssss', '2018-03-12', 0, 'RIC', 'Locality', 1, 0),
(6, '1', 'asssasa', 'assasa', '2018-03-12', 0, 'asassa', 'assaas', 1, 0),
(7, '1', 'Bans', 'Bans', '2018-03-13', 0, NULL, NULL, 1, 0),
(8, '1', 'asssasa', '+2348148380710', '2018-03-13', 0, 'Bans', 'assaas', 1, 0),
(9, '1', 'Bans', '+2348148380710', '2018-03-14', 0, 'RIC', 'Locality', 1, 0),
(10, '1', 'Bans', 'Bans', '2018-03-14', 0, 'RIC', 'Locality', 5, 0),
(11, '1', 'Bab', 'Bab', '2018-03-17', 1, 'RIC', 'Locality', 2, 1),
(12, '2', 's', 's', '2018-03-18', 1, NULL, NULL, 2, 1),
(13, '1', 'Bans', 'Bans', '2018-03-19', 1, NULL, NULL, 6, 1),
(14, '1', 'Bans', 'Bans', '2018-03-19', 0, NULL, NULL, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `survey_logs`
--

CREATE TABLE `survey_logs` (
  `id` int(11) NOT NULL,
  `page` varchar(45) NOT NULL,
  `user_id` varchar(45) NOT NULL,
  `survey_id` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `survey_logs`
--

INSERT INTO `survey_logs` (`id`, `page`, `user_id`, `survey_id`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '11', '2018-03-17 11:17:41', '2018-03-17 11:17:41'),
(2, '2', '1', '11', '2018-03-17 11:18:31', '2018-03-17 11:18:31'),
(3, '3', '1', '11', '2018-03-17 11:18:46', '2018-03-17 11:18:46'),
(4, '4', '1', '11', '2018-03-17 11:20:52', '2018-03-17 11:20:52'),
(5, '8', '1', '11', '2018-03-17 11:22:19', '2018-03-17 11:22:19'),
(6, '8', '1', '11', '2018-03-17 12:19:05', '2018-03-17 12:19:05'),
(7, '9', '1', '11', '2018-03-17 18:01:43', '2018-03-17 18:01:43'),
(8, '9', '1', '11', '2018-03-17 18:26:15', '2018-03-17 18:26:15'),
(9, '9', '1', '11', '2018-03-17 18:27:49', '2018-03-17 18:27:49'),
(10, '9', '1', '11', '2018-03-17 18:27:58', '2018-03-17 18:27:58'),
(11, '9', '1', '11', '2018-03-17 18:29:05', '2018-03-17 18:29:05'),
(12, '10', '1', '11', '2018-03-18 14:54:14', '2018-03-18 14:54:14'),
(13, '10', '1', '11', '2018-03-18 14:54:39', '2018-03-18 14:54:39'),
(14, '10', '1', '11', '2018-03-18 14:55:04', '2018-03-18 14:55:04'),
(15, '1', '1', '12', '2018-03-18 16:26:17', '2018-03-18 16:26:17'),
(16, '10', '1', '12', '2018-03-18 16:26:56', '2018-03-18 16:26:56'),
(17, '1', '1', '13', '2018-03-19 09:22:30', '2018-03-19 09:22:30'),
(18, '10', '1', '13', '2018-03-19 11:49:50', '2018-03-19 11:49:50'),
(19, '1', '1', '14', '2018-03-19 19:24:31', '2018-03-19 19:24:31'),
(20, '2', '1', '14', '2018-03-20 06:26:26', '2018-03-20 06:26:26'),
(22, '4', '1', '14', '2018-03-21 05:59:57', '2018-03-21 05:59:57'),
(23, '6', '1', '14', '2018-03-24 07:53:37', '2018-03-24 07:53:37'),
(24, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(25, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(26, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(27, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(28, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(29, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(30, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(31, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(32, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(33, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(34, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(35, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(36, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(37, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(38, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(39, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(40, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(41, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(42, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(43, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(44, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(45, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(46, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(47, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(48, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(49, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(50, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(51, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(52, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(53, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(54, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(55, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(56, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(57, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(58, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(59, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(60, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(61, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(62, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(63, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(64, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(65, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(66, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(67, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(68, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(69, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(70, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(71, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(72, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(73, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(74, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(75, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(76, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(77, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(78, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(79, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(80, '7', '1', '14', '2018-03-24 08:48:19', '2018-03-24 08:48:19'),
(81, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(82, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(83, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(84, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(85, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(86, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(87, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(88, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(89, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(90, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(91, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(92, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(93, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(94, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(95, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(96, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(97, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(98, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(99, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(100, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(101, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(102, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(103, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(104, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(105, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(106, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(107, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(108, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(109, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(110, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(111, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(112, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(113, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(114, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(115, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(116, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(117, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(118, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(119, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(120, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(121, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(122, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(123, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(124, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(125, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(126, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(127, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(128, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(129, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(130, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(131, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(132, '7', '1', '14', '2018-03-24 08:50:19', '2018-03-24 08:50:19'),
(133, '7', '1', '14', '2018-03-24 08:50:20', '2018-03-24 08:50:20'),
(134, '7', '1', '14', '2018-03-24 08:50:20', '2018-03-24 08:50:20'),
(135, '7', '1', '14', '2018-03-24 08:50:20', '2018-03-24 08:50:20'),
(136, '7', '1', '14', '2018-03-24 08:50:20', '2018-03-24 08:50:20'),
(137, '7', '1', '14', '2018-03-24 08:50:20', '2018-03-24 08:50:20'),
(138, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(139, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(140, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(141, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(142, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(143, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(144, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(145, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(146, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(147, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(148, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(149, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(150, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(151, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(152, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(153, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(154, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(155, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(156, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(157, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(158, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(159, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(160, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(161, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(162, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(163, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(164, '7', '1', '14', '2018-03-24 08:53:46', '2018-03-24 08:53:46'),
(165, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(166, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(167, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(168, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(169, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(170, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(171, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(172, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(173, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(174, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(175, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(176, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(177, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(178, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(179, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(180, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(181, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(182, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(183, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(184, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(185, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(186, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(187, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(188, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(189, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(190, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(191, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(192, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(193, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(194, '7', '1', '14', '2018-03-24 08:53:47', '2018-03-24 08:53:47'),
(195, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(196, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(197, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(198, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(199, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(200, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(201, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(202, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(203, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(204, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(205, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(206, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(207, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(208, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(209, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(210, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(211, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(212, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(213, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(214, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(215, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(216, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(217, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(218, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(219, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(220, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(221, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(222, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(223, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(224, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(225, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(226, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(227, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(228, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(229, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(230, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(231, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(232, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(233, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(234, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(235, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(236, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(237, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(238, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(239, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(240, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(241, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(242, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(243, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(244, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(245, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(246, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(247, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(248, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(249, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(250, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(251, '7', '1', '14', '2018-03-24 14:23:11', '2018-03-24 14:23:11'),
(252, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(253, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(254, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(255, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(256, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(257, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(258, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(259, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(260, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(261, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(262, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(263, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(264, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(265, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(266, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(267, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(268, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(269, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(270, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(271, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(272, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(273, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(274, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(275, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(276, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(277, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(278, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(279, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(280, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(281, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(282, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(283, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(284, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(285, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(286, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(287, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(288, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(289, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(290, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(291, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(292, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(293, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(294, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(295, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(296, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(297, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(298, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(299, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(300, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(301, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(302, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(303, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(304, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(305, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(306, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(307, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(308, '7', '1', '14', '2018-03-24 14:23:39', '2018-03-24 14:23:39'),
(309, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(310, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(311, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(312, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(313, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(314, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(315, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(316, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(317, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(318, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(319, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(320, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(321, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(322, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(323, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(324, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(325, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(326, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(327, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(328, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(329, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(330, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(331, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(332, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(333, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(334, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(335, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(336, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(337, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(338, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(339, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(340, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(341, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(342, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(343, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(344, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(345, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(346, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(347, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(348, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(349, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(350, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(351, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(352, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(353, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(354, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(355, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(356, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(357, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(358, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(359, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(360, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(361, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(362, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(363, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(364, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(365, '7', '1', '14', '2018-03-24 14:25:07', '2018-03-24 14:25:07'),
(366, '8', '1', '14', '2018-03-24 14:45:24', '2018-03-24 14:45:24'),
(367, '8', '1', '14', '2018-03-24 14:46:41', '2018-03-24 14:46:41'),
(368, '8', '1', '14', '2018-03-24 14:47:26', '2018-03-24 14:47:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(150) NOT NULL,
  `lastname` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(1) NOT NULL,
  `remember_token` varchar(205) DEFAULT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `password`, `created_at`, `updated_at`, `status`, `remember_token`, `users_id`) VALUES
(1, 'Admin', 'LearnNigeria', 'learnadmin', '$2y$10$gmpbtkQEDPD4L0CcfxqTPeIgmxGKhbxkXizxddmXuKj0xmld/cy3y', '2018-03-03 00:00:00', '2018-03-03 00:00:00', 1, '1OKFmTNnjGY34KOtQ3CEwRkdObdVesXcMrj9AStWDaj3C7ofrj9ODEVH5LyO', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `volunteers`
--

CREATE TABLE `volunteers` (
  `id` int(11) NOT NULL,
  `name` varchar(245) NOT NULL,
  `sex` int(1) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `code` varchar(5) DEFAULT NULL,
  `surveys_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volunteers`
--

INSERT INTO `volunteers` (`id`, `name`, `sex`, `mobile`, `code`, `surveys_id`) VALUES
(1, 'A New One', 3, 'Banjai', 'Baoos', 1),
(2, 'sss', 3, 'boss', 'love', 2),
(3, 'lcoco', 4, 'ssss', 'ssss', 2),
(4, 'assasa', 3, 'aassasa', 'asass', 3),
(5, 'This is good', 3, '08148380710', 'ACode', 4),
(6, 'A Name', 4, '08148380719', 'CODE', 4),
(7, 'Bans', 3, 'AnANA', 'NANA', 5),
(8, 'assa', 3, 'assasa', 'assa', 6),
(9, 'Kayode Olaniyi', 3, '+2348148380710', 'assa', 8),
(10, 'Olaniyi Kayode', 3, '+2348148380710', 'assa', 9),
(11, 'A Name', 3, '08148380719', 'ACode', 10),
(12, 'Kayode', 3, '08148380719', 'OLOLO', 11),
(13, 's', 3, 's', 's', 12),
(14, 'A Name', 3, '08148380719', 'ACode', 13),
(15, 'A Name', 3, '08148380719', 'ACode', 14);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ea_compilation_questions`
--
ALTER TABLE `ea_compilation_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ea_compilation_results`
--
ALTER TABLE `ea_compilation_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ea_observation_results`
--
ALTER TABLE `ea_observation_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ea_observation_results_observation_sheet_questions1_idx` (`observation_sheet_questions_id`),
  ADD KEY `fk_ea_observation_results_observation_sheet_answers1_idx` (`observation_sheet_answers_id`),
  ADD KEY `fk_ea_observation_results_surveys1_idx` (`surveys_id`,`surveys_enumeration_areas_id`);

--
-- Indexes for table `enumeration_areas`
--
ALTER TABLE `enumeration_areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_enumeration_areas_local_government1_idx` (`local_government_id`),
  ADD KEY `fk_enumeration_areas_users1_idx` (`users_id`);

--
-- Indexes for table `households`
--
ALTER TABLE `households`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_households_surveys1_idx` (`surveys_id`,`surveys_enumeration_areas_id`);

--
-- Indexes for table `household_guardians`
--
ALTER TABLE `household_guardians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `household_information_questions`
--
ALTER TABLE `household_information_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `household_information_results`
--
ALTER TABLE `household_information_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `household_observation_sheet`
--
ALTER TABLE `household_observation_sheet`
  ADD PRIMARY KEY (`id`,`surveys_id`,`surveys_enumeration_areas_id`),
  ADD KEY `fk_household_observation_sheet_surveys1_idx` (`surveys_id`,`surveys_enumeration_areas_id`);

--
-- Indexes for table `household_questions`
--
ALTER TABLE `household_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `household_results`
--
ALTER TABLE `household_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_household_results_household_questions1_idx` (`household_questions_id`),
  ADD KEY `fk_household_results_household_observation_sheet1_idx` (`household_observation_sheet_id`,`household_observation_sheet_surveys_id`,`household_observation_sheet_surveys_enumeration_areas_id`);

--
-- Indexes for table `local_governments`
--
ALTER TABLE `local_governments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_local_government_states1_idx` (`states_id`),
  ADD KEY `fk_local_government_users1_idx` (`users_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `observation_sheet_questions`
--
ALTER TABLE `observation_sheet_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_survey_questions`
--
ALTER TABLE `school_survey_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_survey_results`
--
ALTER TABLE `school_survey_results`
  ADD PRIMARY KEY (`id`,`surveys_id`,`surveys_enumeration_areas_id`),
  ADD KEY `fk_school_survey_results_school_survey_questions1_idx` (`school_survey_questions_id`),
  ADD KEY `fk_school_survey_results_answers1_idx` (`answers_id`),
  ADD KEY `fk_school_survey_results_surveys1_idx` (`surveys_id`,`surveys_enumeration_areas_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supervisors`
--
ALTER TABLE `supervisors`
  ADD PRIMARY KEY (`id`,`surveys_id`,`surveys_enumeration_areas_id`),
  ADD KEY `fk_supervisors_surveys1_idx` (`surveys_id`,`surveys_enumeration_areas_id`);

--
-- Indexes for table `surveys`
--
ALTER TABLE `surveys`
  ADD PRIMARY KEY (`id`,`enumeration_areas_id`),
  ADD KEY `fk_surveys_enumeration_areas_idx` (`enumeration_areas_id`);

--
-- Indexes for table `survey_logs`
--
ALTER TABLE `survey_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_users_idx` (`users_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_roles_users1_idx` (`users_id`),
  ADD KEY `fk_user_roles_roles1_idx` (`roles_id`);

--
-- Indexes for table `volunteers`
--
ALTER TABLE `volunteers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_volunteers_surveys1_idx` (`surveys_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `ea_compilation_questions`
--
ALTER TABLE `ea_compilation_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ea_compilation_results`
--
ALTER TABLE `ea_compilation_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `ea_observation_results`
--
ALTER TABLE `ea_observation_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `enumeration_areas`
--
ALTER TABLE `enumeration_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `households`
--
ALTER TABLE `households`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `household_guardians`
--
ALTER TABLE `household_guardians`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `household_information_questions`
--
ALTER TABLE `household_information_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `household_information_results`
--
ALTER TABLE `household_information_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=279;
--
-- AUTO_INCREMENT for table `household_observation_sheet`
--
ALTER TABLE `household_observation_sheet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `household_questions`
--
ALTER TABLE `household_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `household_results`
--
ALTER TABLE `household_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `local_governments`
--
ALTER TABLE `local_governments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `observation_sheet_questions`
--
ALTER TABLE `observation_sheet_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `school_survey_questions`
--
ALTER TABLE `school_survey_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT for table `school_survey_results`
--
ALTER TABLE `school_survey_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `supervisors`
--
ALTER TABLE `supervisors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `surveys`
--
ALTER TABLE `surveys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `survey_logs`
--
ALTER TABLE `survey_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=369;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `volunteers`
--
ALTER TABLE `volunteers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
