@extends('admin.layout')
@section('title', 'Dashboard')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <a href="#">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Total Surveys</span>
                            <span class="info-box-number"></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>
            <!-- /.col -->
            <a href="#">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Enumeration Areas</span>
                            <span class="info-box-number">{{ $data["enumerations"] }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
            <a href="#">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Children</span>
                            <span class="info-box-number">760</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>
            <!-- /.col -->
            <a href="#">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Volunteers</span>
                            <span class="info-box-number">{{ $data["volunteers"] }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                        <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
                        <li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
                        <li class="pull-left header"><i class="fa fa-inbox"></i>Generate Report</li>
                    </ul>
                    <div class="tab-content no-padding">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Respondents Details</h3>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <form action="{{route('download.report')}}">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>State</label>
                                                {{ csrf_field() }}
                                                <select class="form-control" name="state">
                                                    <option value="0">None</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Local Government</label>
                                                <select class="form-control" name="local_government">
                                                    <option value="0">None</option>
                                                </select>
                                            </div>

                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Enumeration Area</label>
                                                <select class="form-control" name="ea">
                                                    <option value="0">None</option>
                                                </select>
                                            </div>
                                            <div class="form-group" style="float:right; padding-top: 30px;">
                                                <label></label>
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-download"></i> Download </button>
                                            </div>
                                        </div>
                                    </form>

                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


@endsection