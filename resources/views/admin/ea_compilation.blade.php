@extends('admin.layout',['hide_sidebar'=>true])
@section('title', 'Survey')
@section('content')
<style>
    .input{
        height:45px;
    }
    .form-footer{
        padding-right:19px;
    }
    .remove-bold{
        font-weight:normal;
    }
</style>


<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Take Survey
                <!--<small>Example 2.0</small>-->
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section>

        <!-- Main content -->
        <div id="survey-apps"><form method="post" action="{{route('survey.ea_compilation.post')}}">
                <section class="content">
                    @if(session('errors'))
                    <div class="alert alert-danger">

                    </div>
                    @endif
                    @if(session('successful'))
                    <div class="alert alert-sucess">
                        Data stored, loading the next page
                    </div>
                    @endif
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Enumeration Area Compilation Sheet</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Family Head Name</label>
                                        {{ csrf_field() }}
                                        <input type = "text" name = "question1" required = "true" class = "form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Age 3 & 4 years</label>
                                        <input type = "text" required = "true" name="question2"  class = "form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label>Age 5 to 15 years</label>
                                        <input type = "text" required = "true" name="question3"  class = "form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label>Never Enrolled</label>
                                        <input type = "text"  name="question4"  class = "form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label>Drop-out</label>
                                        <input type = "text" name="question5"  class = "form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label>Currently Enrolled(Govt.)</label>
                                        <input type = "text" name="question6"  class = "form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label>Currently Enrolled(Private.)</label>
                                        <input type = "text"  name="question7"  class = "form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label >Reading English</label>
                                        <div class = "row">
                                            <div class = "col-lg-4"> 
                                                <label class = "remove-bold">Beginner </label>
                                                <input type = "text" required = "true" name="question8"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">  
                                                <label class = "remove-bold">Letter</label>
                                                <input type = "text" required = "true" name="question9"  class = "form-control"  />
                                            </div>
                                            <div class = "col-lg-4">    <label class = "remove-bold">Word</label>  <input type = "text" required = "true" name="question10"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Paragraph</label> <input type = "text" required = "true" name="question11"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Story</label> <input type = "text" required = "true" name="question12"  class = "form-control"  /></div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Comprehension for those reading story only(Q1)</label>
                                        <div class = "row">
                                            <div class = "col-lg-4"> 
                                                <label class = "remove-bold">Can Do</label>
                                                <input type = "text" required = "true" name="question13"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">  
                                                <label class = "remove-bold">Cannot Do</label>
                                                <input type = "text" required = "true" name="question14"  class = "form-control"  />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Comprehension for those reading story only(Q2)</label>
                                        <div class = "row">
                                            <div class = "col-lg-4"> 
                                                <label class = "remove-bold">Can Do </label>
                                                <input type = "text" required = "true" name="question15"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">  
                                                <label class = "remove-bold">Cannot Do</label>
                                                <input type = "text" required = "true" name="question16"  class = "form-control"  />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Reading local language</label>
                                        <div class = "row">
                                            <div class = "col-lg-4"> 
                                                <label class = "remove-bold">Beginner </label>
                                                <input type = "text" required = "true" name="question17"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">  
                                                <label class = "remove-bold">Letter</label>
                                                <input type = "text" required = "true" name="question18"  class = "form-control"  />
                                            </div>
                                            <div class = "col-lg-4">    <label class = "remove-bold">Word</label>  <input type = "text" required = "true" name="question19"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Paragraph</label> <input type = "text" required = "true" name="question20"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Story</label> <input type = "text" required = "true" name="question21"  class = "form-control"  /></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Comprehension for those reading story only(Q1)</label>
                                        <div class = "row">
                                            <div class = "col-lg-4"> 
                                                <label class = "remove-bold">Can Do </label>
                                                <input type = "text" required = "true" name="question22"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">  
                                                <label class = "remove-bold">Cannot Do</label>
                                                <input type = "text" required = "true" name="question23"  class = "form-control"  />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Comprehension for those reading story only(Q2)</label>
                                        <div class = "row">
                                            <div class = "col-lg-4"> 
                                                <label class = "remove-bold">Can Do </label>
                                                <input type = "text" required = "true" name="question24"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">  
                                                <label class = "remove-bold">Cannot Do</label>
                                                <input type = "text" required = "true" name="question25"  class = "form-control"  />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Numeracy</label>
                                        <div class = "row">
                                            <div class = "col-lg-4"> 
                                                <label class = "remove-bold">Beginner </label>
                                                <input type = "text" required = "true" name="question26"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">  
                                                <label class = "remove-bold">Counting</label>
                                                <input type = "text" required = "true" name="question27"  class = "form-control"  />
                                            </div>
                                            <div class = "col-lg-4">    <label class = "remove-bold">Number Recog(0-9)</label>  <input type = "text" required = "true" name="question28"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Number Recog(10-99)</label> <input type = "text" required = "true" name="question29"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Addition</label> <input type = "text" required = "true" name="question30"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Subtraction</label> <input type = "text" required = "true" name="question31"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Multiplication</label> <input type = "text" required = "true" name="question32"  class = "form-control"  /></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Ethnomaths</label>
                                        <div class = "row">
                                            <div class = "col-lg-4"> 
                                                <label class = "remove-bold">Correct </label>
                                                <input type = "text" required = "true" name="question33"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">  
                                                <label class = "remove-bold">Incorrect</label>
                                                <input type = "text" required = "true" name="question34"  class = "form-control"  />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Bonus (Tick √ if can recognize or X if cannot</label>
                                        <br/>
                                        <div class = "row">
                                            <div class = "col-lg-4"> 
                                                <label class = "remove-bold">Foot/Leg</label>
                                                <input type = "text" required = "true" name="question35"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">  
                                                <label class = "remove-bold">Ear</label>
                                                <input type = "text" required = "true" name="question36"  class = "form-control"  />
                                            </div>
                                            <div class = "col-lg-4">    <label class = "remove-bold">Nose</label>  <input type = "text" required = "true" name="question37"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Eye</label> <input type = "text" required = "true" name="question38"  class = "form-control"  /></div>
                                            <div class = "col-lg-4">     <label class = "remove-bold">Hand\Palm</label> <input type = "text" required = "true" name="question39"  class = "form-control"  /></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                Note*** Fill this section Once
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>If the number of HH surveyed in the village is less than 20, give reasons in this box</label>
                                        <textarea class="form-control" name="question40"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Total Number of Locked HH</label>
                                        <input type="text" class="form-control" name="question41">
                                    </div>
                                    <div class="form-group">
                                        <label>Total number of No Response</label>
                                        <input type="text" class="form-control" name="question42">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-xs-4 pull-right">
                                <button type="submit" name = "include_household" class="btn btn-primary btn-md btn-block btn-flat" value="Include Another Household" id = "signin-button">Include Another Household</button>
                            </div>

                            <div class="col-xs-4 pull-right">
                                <button type="submit" name="finish" class="btn btn-primary btn-md btn-block btn-flat" id = "signin-button" value="Finish">Finish</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->

                    <!-- /.box -->
                </section>
        </div>


        <!-- /.content -->

    </div>
    <!-- /.container -->
</div>
<!-- /.content-wrapper -->
<script>
    /* */
    var vue = new Vue({
        el: '#survey-apps',
        data: {
            "localGovernments": [],
            "enumerationAreas": [],
            "iCount": 1,
            "localGovernmentId": 0,
        },
        methods: {
            loadLocalGovernment() {
                console.log(this.localGovernmentId);
                axios.get(BASE_URL + "api/local-governments/" + this.localGovernmentId).then(response => {
                    console.log(response);
                    this.localGovernments = response.data;
                })

            },
            loadEA() {
                axios.get(BASE_URL + "api/enumeration-areas/" + this.localGovernmentId).then(response => {
                    this.enumerationAreas = response.data;
                })
            }, increaseICount() {
                this.iCount = this.iCount + 1;
            },
            decreaseICount() {
                if (this.iCount > 1) {
                    this.iCount = this.iCount - 1;
                }
            }
        }});
</script>
@endsection