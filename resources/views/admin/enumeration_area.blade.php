@extends('admin.layout')
@section('title', 'See All Enumeration Area')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div style="height:0px;"></div>
    <section class="content-header">
        <h1>
            Local Government
            <small>preview of all LG</small>
        </h1>

      
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        @if(session('enumeration_area_editted'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i>Success!</h4>

            <div>The Local Government details was editted</div>

        </div>
        @endif
        @if(session('message'))
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <div>{{session('message')}}</div>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All created users</h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Local Government</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            @foreach ($enumeration_areas as $enumeration_area)
                            <tr>
                                <td>{{$enumeration_area['id']}}</td>
                                <td>{{$enumeration_area['name']}}</td>
                                <td>{{$enumeration_area['code']}}</td>
                                <td>{{$enumeration_area->localGovernment->name}}</td>
                                <td>{{$enumeration_area['created_at']}}</td>
                                <td>@if($enumeration_area['status'] == 1)<span class="label label-success">Approved</span> @elseif($enumeration_area['status'] == 0) <span class="label label-danger">Denied</span> @endif</td>
                                <td>@if($enumeration_area['status'] == 0)<a href="{{route('enumeration_area.change_status',["id"=>$enumeration_area['id']])}}">Approve</a> @elseif($enumeration_area['status'] == 1) <a href="{{route('enumeration_area.change_status',["id"=>$enumeration_area['id']])}}">Deny</a> @endif &nbsp;&nbsp;
                                   @if($enumeration_area['status'] == 1) <a href="{{route('enumeration_area.edit',["id"=>$enumeration_area['id']])}}"> Edit Info</a>@endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection