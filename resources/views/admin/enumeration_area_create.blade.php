@extends('admin.layout')
@section('title', 'Create Local Government')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div style="height:0px;"></div>
    <section class="content-header">
        <h1>
            Local Governments
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach
                </div>
                @endif

                @if(session('enumeration_area_created'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i>Success!</h4>

                    <div>The Enumeration Area {{session('enumeration_area')->name}} has been created successfully</div>

                </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Enumeration Area</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['url' => Route::current()->getName() == 'enumeration_area.edit' ? route('enumeration_area.edit.post') : route('enumeration_area.create.post'),'role'=>'form']) !!}
                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Enumeration Area</label>
                            {!! Form::text('name' , $enumerationArea['name'] ?? '' ,["class"=>"form-control","placeholder"=>"Enter Enumeration Area","id"=>"firstname"]) !!}
                             @if(Route::current()->getName() == 'enumeration_area.edit')
                            {!! Form::hidden('id' , $enumerationArea['id'] ?? '') !!}
                            @endif
                            {!! Form::hidden('state_id' , request()->id ?? '') !!}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">EA Code</label>
                              @if( Route::current()->getName() != 'enumeration_area.edit')
                             {!! Form::text('code' , $enumerationArea['code'] ?? '' ,["class"=>"form-control","placeholder"=>"Enter Enumeration Area Code","id"=>"lastname"]) !!}
                             @else
                               {!! Form::text('code' , $enumerationArea['code'] ?? '' ,["class"=>"form-control","placeholder"=>"Enter Enumeration Area Code","id"=>"lastname","disabled"=>"disabled"]) !!}
                       
                           @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Local Government</label>
                            <select name = 'local_government_id' class="form-control">
                                <option></option>
                                @foreach ($local_governments as $local_government)
                                <option value="{{$local_government['id']}}"
                                        @if(Route::current()->getName() == 'enumeration_area.edit' && $enumerationArea->localGovernment->id == $local_government['id'])
                                        selected="selected"
                                        @endif
                                        >{{$local_government['name']}}</option>
                                @endforeach
                            </select>
                        </div> 

                       <!-- @if(!request()->edit)
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>

                            {!! Form::password('password',["class"=>"form-control","placeholder"=>"Enter Password","id"=>"password"]) !!}

                        </div>
                        @endif-->
                        <!-- <div class="checkbox">
                           <label>
                             <input type="checkbox"> Check me out
                           </label>
                         </div>-->
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">

                        <button type="submit" class="btn btn-primary">
                            @if(Route::current()->getName() == 'local_government.edit')
                            Edit Local Government
                            @else
                            Submit
                            @endif
                        </button>

                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
            <!-- right column -->

            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection