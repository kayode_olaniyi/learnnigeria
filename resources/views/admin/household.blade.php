@extends('admin.layout',['hide_sidebar'=>true])
@section('title', 'Survey Household')
@section('content')
<style>
    .input{
        height:45px;
    }
    .form-footer{
        padding-right:19px;
    }
</style>


<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Selected Household Sheet
                <!--<small>Example 2.0</small>-->
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section>

        <!-- Main content -->
        <div id="survey-apps"><form method="post" action="{{route('survey.household.post')}}">
                <section class="content">
                    @if(session('errors'))
                    <div class="alert alert-danger">

                    </div>
                    @endif
                    @if(session('successful'))
                    <div class="alert alert-sucess">
                        Data stored, loading the next page
                    </div>
                    @endif
                    <div class="box box-default">
                        <div class="box-header with-border">
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Locality</label>
                                        {{ csrf_field() }}
                                        <input type = "text" name = "survey[locality]" required = "true" class = "form-control" />
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>RIC</label>
                                        <input type = "text" required = "true" name="survey[ric]"  class = "form-control"  />
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>
                        <h4 style="padding-left:9px;">Houldholds</h4>
                        <div class="box-body"  >

                            <div class="row" v-for = "i in iCount">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address of Building</label>
                                        <input type = "text" required = "true" name="household[address][]" class = "form-control" />
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Building Number</label>
                                        <input type = "text" required = "true" name="household[building_no][]" class = "form-control"  />
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Selected HH Numbers</label>
                                        <input type = "text" required = "true" name="household[hh_number][]" class = "form-control"  />
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">

                                        <label>Name of Head of Household</label>
                                        <input type = "text" required = "true" class = "form-control" name="household[household_head][]" />
                                    </div>
                                    <!-- /.form-group -->

                                </div>
                                <!-- /.col -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-footer" style="padding:9px;">
                                <button type="button" class="btn btn-danger pull-right" v-on:click= "decreaseICount()">Remove Household</button> <span class="pull-right">&nbsp;&nbsp;</span>
                                <button type="button" class="btn btn-primary pull-right" v-on:click= "iCount++">Add Household</button>
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="box-footer">
                            <div class="col-xs-4 pull-right">
                                <button type="submit" class="btn btn-primary btn-md btn-block btn-flat" id = "signin-button">Next</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->

                    <!-- /.box -->
                </section>
        </div>


        <!-- /.content -->

    </div>
    <!-- /.container -->
</div>
<!-- /.content-wrapper -->
<script>
    /* */
    var vue = new Vue({
        el: '#survey-apps',
        data: {
            "localGovernments": [],
            "enumerationAreas": [],
            "iCount": 1,
            "localGovernmentId": 0,
        },
        methods: {
            increaseICount() {
                this.iCount = this.iCount + 1;
            },
            decreaseICount() {
                if (this.iCount > 1) {
                    this.iCount = this.iCount - 1;
                }
            }
        }});
</script>
@endsection