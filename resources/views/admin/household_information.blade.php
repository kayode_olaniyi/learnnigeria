@extends('admin.layout',['hide_sidebar'=>true])
@section('title', 'Survey')
@section('content')
<style>
    .input{
        height:45px;
    }
    .form-footer{
        padding-right:19px;
    }
</style>


<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Household Information Sheet
                <!--<small>Example 2.0</small>-->
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section>

        <!-- Main content -->
        <div id="survey-apps"><form method="post" action="{{route('survey.household_information.create')}}">
                <section class="content">
                    @if(session('errors'))
                    <div class="alert alert-danger">

                    </div>
                    @endif
                    @if(session('successful'))
                    <div class="alert alert-sucess">
                        Data stored, loading the next page
                    </div>
                    @endif
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">H600.  Child's Biodata</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name of the Child</label>
                                        {{ csrf_field() }}
                                        <input type="hidden" name="child_id" value="{{$child_id}}"/>
                                        <input type = "text" name = "question1" required = "true" class = "form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Age (3-15years)</label>
                                        <select class="form-control" v-model = "age" name="question2">
                                            <option selected="selected"></option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Sex</label>
                                        <select class="form-control" name="question3" required = "true" id = "enumerationArea" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "3">Male</option>
                                            <option value = "4">Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <h4>H700.  Educational Status </h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Educational Status </label>
                                        <select class="form-control" name="question4" v-model = "ed_status" required = "true" id = "enumerationArea" style="width: 100%;">
                                            <option selected="selected"></option>

                                            <option value="72">Never Enrolled</option>
                                            <option value="73">Dropped Out</option>
                                            <option value="74">Currently Enrolled</option>  

                                        </select>
                                    </div>
                                    <div class="form-group" v-if = 'ed_status == 72'>
                                        <label>Give reason(s) </label>
                                        <br/>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="75">
                                                Not enough money to enroll
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="76">
                                                Need for labour 
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"name="question5[]" value="77">
                                                Distance of school 
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="78">
                                                Poor school quality 

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="79">
                                                Engaged, married, pregnancy 

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="80">
                                                No money to continue

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="81">
                                                Not mature enough to start school

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="82">
                                                Death of parent(s)

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="83">
                                                Parents’ lack of interest

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="84">
                                                Change of environment/transfer

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="85">
                                                Ignorance

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question5[]" value="86">
                                                Need for special education

                                            </label>
                                        </div>


                                        <label style="font-weight: normal;">Others Please Specify @{{question5_text}}</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input type="checkbox" name="question5[]" v-model = "question5_checked" v-bind:value="question5_text">
                                            </span>
                                            <input class="form-control" type="text" :disabled = "question5_checked == ''" v-model = "question5_text">
                                        </div>
                                    </div>
                                    <div class="form-group" v-if = 'ed_status == 73'>
                                        <label>At what class </label>
                                        <select class="form-control" name="question6" required = "true"  style="width: 100%;">
                                            <option value="151">Nur1</option>	
                                            <option value="87">Nur2</option>	
                                            <option value="88">KG</option>	
                                            <option value="89">Pry1</option>	
                                            <option value="90">Pry2</option>	
                                            <option value="91">Pry3</option>	
                                            <option value="92">Pry4</option>
                                            <option value="93">Pry5</option>
                                            <option value="94">Pry6</option>
                                            <option value="95">JSS1 </option>
                                            <option value="96">JSS2 </option>
                                            <option value="97">JSS3 </option>
                                            <option value="98">SSS1 </option>
                                            <option value="99">SSS2 </option>
                                            <option value="100">SSS3 </option>

                                        </select>
                                    </div>
                                    <div class="form-group" v-if = 'ed_status == 73'>
                                        <label>Reason(s) for drop out</label>
                                        <br/>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="75">
                                                Not enough money to enroll
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="76">
                                                Need for labour 
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"name="question7[]" value="77">
                                                Distance of school 
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="78">
                                                Poor school quality 

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="79">
                                                Engaged, married, pregnancy 

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="80">
                                                No money to continue

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="81">
                                                Not mature enough to start school

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="82">
                                                Death of parent(s)

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="83">
                                                Parents’ lack of interest

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="84">
                                                Change of environment/transfer

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="85">
                                                Ignorance

                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question7[]" value="86">
                                                Need for special education

                                            </label>
                                        </div>


                                        <label style="font-weight: normal;">Others Please Specify @{{question7_text}}</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input type="checkbox" name="question7[]" v-model = "question7_checked" v-bind:value="question7_text">
                                            </span>
                                            <input class="form-control" type="text" :disabled = "question7_checked == ''" v-model = "question7_text">
                                        </div>
                                    </div>
                                    <div class="form-group" v-if = 'ed_status == 73'>
                                        <label>Year of dropout</label>
                                        <input type="text" name="question8" class="form-control"/>
                                    </div>
                                    <div class="form-group" v-if = 'ed_status == 74'>
                                        <label>If the child is currently enrolled, at what class ?</label>
                                        <select class="form-control" required = "true"  style="width: 100%;" name="question9">
                                            <option value="151">Nur1</option>	
                                            <option value="87">Nur2</option>	
                                            <option value="88">KG</option>	
                                            <option value="89">Pry1</option>	
                                            <option value="90">Pry2</option>	
                                            <option value="91">Pry3</option>	
                                            <option value="92">Pry4</option>
                                            <option value="93">Pry5</option>
                                            <option value="94">Pry6</option>
                                            <option value="95">JSS1 </option>
                                            <option value="96">JSS2 </option>
                                            <option value="97">JSS3 </option>
                                            <option value="98">SSS1 </option>
                                            <option value="99">SSS2 </option>
                                            <option value="100">SSS3 </option>

                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>
                        <h4 style="padding-left:9px;"> Household Information Details</h4>
                        <div class="box-body"  >

                            <div class="row" v-for = "i in iCount">
                                <div class="col-md-6">

                                    <div class="form-group" v-if = "ed_status != 72">
                                        <label>What language is the child taught with in school?</label>
                                        <select class="form-control" required = "true" name = "question10" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="101"> Local </option>
                                            <option value="102"> English </option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-if = "ed_status != 72">
                                        <label>At what age did the child begin Primary 1?</label>
                                        <input type="text" name="question11" class="form-control"/>
                                    </div>

                                    <div class="form-group" v-if = "ed_status == 74">
                                    <label>Reason(s) for not enrolling at age 6 </label>
                                        <div class="form-group">
                                         
                                            <br/>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="75">
                                                    Not enough money to enroll
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="76">
                                                    Need for labour 
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"name="question12[]" value="77">
                                                    Distance of school 
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="78">
                                                    Poor school quality 

                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="79">
                                                    Engaged, married, pregnancy 

                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="80">
                                                    No money to continue

                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="81">
                                                    Not mature enough to start school

                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="82">
                                                    Death of parent(s)

                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="83">
                                                    Parents’ lack of interest

                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="84">
                                                    Change of environment/transfer

                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="85">
                                                    Ignorance

                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="question12[]" value="86">
                                                    Need for special education

                                                </label>
                                            </div>


                                            <label style="font-weight: normal;">Others Please Specify @{{question12_text}}</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <input type="checkbox" name="question12[]" v-model = "question12_checked" v-bind:value="question12_text">
                                                </span>
                                                <input class="form-control" type="text" :disabled = "question12_checked == ''" v-model = "question12_text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">

                                    <div class="form-group" v-if = "ed_status != 72">
                                        <label>Does the child have pre-primary education?</label>
                                        <select required = "true" class="form-control"  name = "question13">
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72">
                                        <label>What type of school is the child enrolled in?</label>
                                        <select required = "true" class="form-control" name = "question14">
                                            <option selected="selected"></option>
                                            <option value = "103">Public</option>
                                            <option value = "104">Private</option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72">
                                        <label>Do you pay any money for your child’s education?</label>
                                        <select required = "true" class="form-control" name = "question15" v-model = "question15">
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->

                                </div>
                                <!-- /.col -->
                                <hr/>
                            </div>

                        </div>
                        <hr/>

                        <div class="box-body"  >
                            <div class="row">
                                <div class="col-md-6" v-if = "ed_status != 73 && ed_status != 72">
                                    <div class="form-group" v-if = "question15 != 12">
                                        <label>Tuiton fee (term)</label>
                                        <input  name="question16" required = "true" type = "text" class = "form-control" />
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72 && question15 != 12">
                                        <label>Textbooks (session)</label>
                                        <input type = "text" required = "true"  name = "question17" class = "form-control"/>
                                    </div>
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72 && question15 != 12">
                                        <label>Transport (term/day)</label>
                                        <input type = "text" required = "true"  name = "question18" class = "form-control"/>
                                    </div>
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72 && question15 != 12">
                                        <label>Feeding (term/day)</label>
                                        <input type = "text" required = "true"  name = "question19" class = "form-control"/>
                                    </div>
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72 && question15 != 12">
                                        <label>PTA (term)</label>
                                        <input type = "text" required = "true"  name = "question20" class = "form-control"/>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6" >
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72 && question15 != 12">
                                        <label>Exam (term)</label>
                                        <input type = "text" required = "true"  name = "question38" class = "form-control"/>
                                    </div>
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72 && question15 != 12">
                                        <label>School fee</label>
                                        <input type = "text" required = "true" name = "question21" class = "form-control" />
                                    </div>
                                    
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72 && question15 != 12">
                                        <label>School Code</label>
                                        <input type = "text" required = "true" name = "question22" class = "form-control" />
                                    </div>
                                    <div class="form-group" v-if = "ed_status != 73 && ed_status != 72 ">
                                        <label>Does the child attend the school surveyed?</label>
                                        <select required = "true" class="form-control" name = "question23">
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Has the child attended/is he/she attending any of the following form(s) of education?</label>
                                        <br/>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question24[]" value="105">
                                                Almajiri
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question24[]" value="106">
                                                Nomadic Education 
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"name="question24[]" value="107">
                                                Vocational classes
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question24[]" value="108">
                                                Devpt programmes
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question24[]" value="109">
                                                Faith-based school
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question24[]" value="110">
                                                IQTE school

                                            </label>
                                        </div>
                                        <label style="font-weight: normal;">Others Please Specify@{{question24_text}}</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input type="checkbox" name="question24[]" v-model = "question24_checked" v-bind:value="question24_text">
                                            </span>
                                            <input class="form-control" type="text" :disabled = "question24_checked == ''" v-model = "question24_text">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Does the child have any known health challenge? </label>
                                        <select required = "true" class="form-control" v-model = "health_challenge" name = "question25">
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <hr/>
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>
                        <h4 style="padding-left:9px;" v-if = "age >= 5 || age == 0"> Household Information </h4>
                        <div class="box-body"  >
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-if = "health_challenge == 11">
                                        <label>State the challenge?</label>

                                        <br/>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question26[]" value="111">
                                                Visual Impairment 
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question26[]" value="112">
                                                Audio Impairment 
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"name="question26[]" value="113">
                                                Physical challenge 
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="question26[]" value="114">
                                                Mental disability 
                                            </label>
                                        </div>
                                        <label style="font-weight: normal;">Additional Sources of Income @{{question26_text}}</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input type="checkbox" name="question26[]" v-model = "question26_checked" v-bind:value="question26_text">
                                            </span>
                                            <input class="form-control" type="text" :disabled = "question26_checked == ''" v-model = "question26_text">
                                        </div>
                                    </div>

                                    <!-- /.form-group -->
                                    <div class="form-group"  v-if = "age >= 5 || age == 0" >
                                        <label>Which sample no. was used to test the child?</label>
                                        <input type = "text" required = "true"  name = "question27" class = "form-control"/>
                                    </div>
                                    <div class="form-group" v-if = "age >= 5 || age == 0" >
                                        <label>Language level: English </label>
                                        <select class="form-control"  name="question28" v-model = "question28" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="115">Beginner </option>
                                            <option value="116">Letter</option>
                                            <option value="117">Word</option>
                                            <option value="118">Paragraph</option>
                                            <option value="119">Story</option>
                                             <option value="47">None</option>
                                        </select>
                                    </div>
                                    <div class="form-group"  v-if = "(age >= 5 || age == 0) && question28 == 119" >
                                        <label>Comprehension for those reading story only Q1</label>
                                        <select class="form-control"   name="question29" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="120">Can Do </option>
                                            <option value="121"> Cannot Do</option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-if = "age >= 5 || age == 0 && question28 == 119" >
                                        <label>Comprehension for those reading story only Q2</label>
                                        <select class="form-control"  name="question30" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="120">Can Do </option>
                                            <option value="121"> Cannot Do</option>
                                        </select>
                                    </div>

                                </div>
                                <!-- /.col -->
                                <div class="col-md-6" v-if = "age >= 5 || age == 0">
                                    <div class="form-group">
                                        <label>Which Local Language was the child tested on</label>
                                        <select class="form-control" required = "true" name="question31" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="122">Yoruba</option>
                                            <option value="123">Hausa</option>
                                            <option value="124">Igbo</option>
                                             <option value="47">None</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Local language Level</label>
                                        <select class="form-control" required = "true" v-model = "question32" name="question32" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="115">Beginner </option>
                                            <option value="116">Letter</option>
                                            <option value="190">Syllable</option>
                                            <option value="117">Word</option>
                                            <option value="118">Paragraph</option>
                                            <option value="119">Story</option>
                                            <option value="47">None</option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-if = "question32 == 119">
                                        <label>Comprehension for those reading story only Q1</label>
                                        <select class="form-control" required = "true" name="question33" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="120">Can Do </option>
                                            <option value="121"> Cannot Do</option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-if = "question32 == 119">
                                        <label>Comprehension for those reading story only Q2</label>
                                        <select class="form-control" required = "true"  name="question34" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="120">Can Do </option>
                                            <option value="121"> Cannot Do</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Numeracy Level</label>
                                        <select class="form-control" required = "true" name="question35" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="125">Beginner</option>
                                            <option value="126">Counting</option>
                                            <option value="127">Number recog. (0-9)</option>
                                            <option value="128">Number recog. (10-99)</option>
                                            <option value="129">Addition</option>
                                            <option value="130">Subtraction</option>
                                            <option value="131">Multiplication</option>
                                             <option value="47">None</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Ethnomaths</label>
                                        <select class="form-control" required = "true" name="question36" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value="132">Correct</option>
                                            <option value="133"> Incorrect </option>
                                            <option value="47">None</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Bonus (Tick √ if can recognize or X if cannot</label>
                                        <br/>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="134" name="question37[]">
                                                Foot/Leg
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="135" name="question37[]">
                                                Ear
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="136" name="question37[]">
                                                Nose
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="137" name="question37[]">
                                                Eye
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="138" name="question37[]">
                                                Hand/Palm
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <hr/>
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <div class="col-xs-4 pull-right">
                                <button type="submit"  name = "ea_compilation" class="btn btn-info btn-md btn-block btn-flat" id = "signin-button" value="Compilation Page">Go to Compilation Page</button>
                            </div>
                            <div class="col-xs-4 pull-left">
                                <button type="submit" name="new_child" class = "btn btn-primary btn-md btn-block btn-flat" value = "Add Child">Add Another Child</button>
                            </div>

                            <div class="col-xs-4 pull-left">
                                <button  type="submit" name = 'new_household' class="btn btn-danger btn-md btn-block btn-flat" id = "signin-button" value="New Household">Create a New Household</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->

                    <!-- /.box -->
                </section>
        </div>


        <!-- /.content -->

    </div>
    <!-- /.container -->
</div>
<!-- /.content-wrapper -->
<script>
    /* */
    var vue = new Vue({
        el: '#survey-apps',
        data: {
            "localGovernments": [],
            "enumerationAreas": [],
            "iCount": 1,
            "localGovernmentId": 0,
            "question5_text": "",
            "question5_checked": "",
            "question7_text": "",
            "question7_checked": "",
            "question12_text": "",
            "question12_checked": "",
            "question24_text": "",
            "question24_checked": "",
            "question26_text": "",
            "question26_checked": "",
            "health_challenge": 0,
            ed_status: 0,
            "age":0,
            "question28":0,
            "question32":0,
            "question15":0,
        },
        methods: {
            loadLocalGovernment() {
                console.log(this.localGovernmentId);
                axios.get(BASE_URL + "api/local-governments/" + this.localGovernmentId).then(response => {
                    console.log(response);
                    this.localGovernments = response.data;
                })

            },
            loadEA() {
                axios.get(BASE_URL + "api/enumeration-areas/" + this.localGovernmentId).then(response => {
                    this.enumerationAreas = response.data;
                })
            }, increaseICount() {
                this.iCount = this.iCount + 1;
            },
            decreaseICount() {
                if (this.iCount > 1) {
                    this.iCount = this.iCount - 1;
                }
            }
        }});
</script>
@endsection