@extends('admin.layout',['hide_sidebar'=>true])
@section('title', 'Survey')
@section('content')
<style>
    .input{
        height:45px;
    }
    .form-footer{
        padding-right:19px;
    }
    .remove-bold{
        font-weight: normal;
    }
</style>


<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Household Observation Survey
                 <!--<small>Example 2.0</small>-->
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section>

        <!-- Main content -->
        <div id="survey-school"><form method="post" action="{{route('survey.household.observation.post')}}">
                <section class="content">
                    @if(session('errors'))
                    <div class="alert alert-danger">

                    </div>
                    @endif
                    @if(session('successful'))
                    <div class="alert alert-sucess">
                        Data stored, loading the next page
                    </div>
                    @endif
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Respondents Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Date of Survey</label>
                                        {{ csrf_field() }}
                                        <input type = "text" name = "question1" required = "true" class = "form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Arrival Time</label>
                                        <input type = "text" name = "question2" required = "true" class = "form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Time of Departure</label>
                                        <input type = "text" name = "question3" required = "true" class = "form-control" />
                                    </div>
                                    <!--<div class="form-group">
                                        <label>Time of Departure</label>
                                        <input type = "text" name = "question3" required = "true" class = "form-control" />
                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="box-body">
                            <h4>H100.  Basic Information</h4>
                            <div class="row">
                                <div class="col-md-6">        
                                    <div class="form-group">
                                        <label>H101.  HH Code </label>
                                        <input type = "text" name = "question4" required = "true" class = "form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>H102.  Building number</label>
                                        <input type = "text" name = "question5" required = "true" class = "form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>H103.  Full Name of the Household Head</label>
                                        <input type = "text" name = "question6" required = "true" class = "form-control" />
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>H104.  Age of the Household Head</label>
                                        <input type = "text" required = "true" name="question7"  class = "form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label>H105.  Occupation of Household head</label>
                                        <input type = "text" required = "true" name="question8"  class = "form-control"  />
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>H106.  Sex</label>
                                        <select  name = "question9" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "3">Male</option>
                                            <option value = "4">Female</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>H107.  Address</label>
                                        <input type = "text" name = "question10" required = "true" class = "form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>H108.  Telephone Number</label>
                                        <input type = "text" name = "question11" required = "true" class = "form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>H109.  Level of Education</label>
                                        <select  name = "question12" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "43">Primary</option>
                                            <option value = "44">JSS</option>
                                            <option value = "45">SS</option>
                                            <option value = "46">Tetiary Education</option>                                        
                                            <option value = "47">None</option>

                                        </select>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>
                        <h4 style="padding-left:9px;">H200.  Household Indicators</h4>
                        <div class="box-body"  >
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>H201.  Number of household members (Ask) (who eat together from the same pot at least 4 times a week and have the same Head of household)</label>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Male</label>
                                                <div class="input-group">
                                                    <input class="form-control" type="text" name="question13" required="true">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Female</label>
                                                <div class="input-group">
                                                    <input class="form-control" type="text" name="question14" required="true">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>H202.  What kind of walls does the house have? </label>
                                        <select  name = "question15" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "48">Mudsticks</option>
                                            <option value = "49">Polythene</option>
                                            <option value = "50">Iron Sheet</option>
                                            <option value = "51">Timber</option>
                                            <option value = "52">Stone/Bricks</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>H203.  Is the house owned or rented? </label>
                                        <select  name = "question16" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "53">Owned</option>
                                            <option value = "54">Rented</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>H204.  Main lighting used in the house  </label>
                                        <select  name = "question17" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "150">Government Source</option>
                                            <option value = "55">Lamp</option>
                                            <option value = "56">Sunlight</option>
                                            <option value = "57">Candles/Lantern</option>
                                            <option value = "58">Generator</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>H205.  Main language spoken in the household? </label>
                                        <input type = "text" name = "question18" required = "true" class = "form-control" />
                                    </div>

                                    <div class="form-group">
                                        <label>H206.  Main source of water used in the household? </label>
                                        <select  name = "question19" v-model = "question19" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "59">Borehole</option>
                                            <option value = "60">Buy Water</option>
                                            <option value = "61">Well</option>
                                            <option value = "62">Rain Water</option>
                                            <option value = "63">Pipe</option>
                                            <option value = "64">River</option>
                                            <option value = "65">Others</option>
                                        </select>
                                        <br/>
                                        <input class="form-control" class="form-control" name = "question53" type="text"  v-if = "question19 == '65'" >
                                    </div>

                                </div>

                                <div class="box-body"  >
                                    <h4>H500.  Parents’ Involvement in School Activities </h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label>H501.  Do you attend PTA meetings</label>
                                                <select  name = "question21" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                    <option selected="selected"></option>
                                                    <option value = "Yes">Yes</option>
                                                    <option value = "No">No</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>H502.  Are you aware of any SBMC in your neighborhood</label>
                                                <select  name = "question22" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                    <option selected="selected"></option>
                                                    <option value = "Yes">Yes</option>
                                                    <option value = "No">No</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>H503.  If yes, are you involved in SBMC activities</label>

                                                <select  name = "question23" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                    <option selected="selected"></option>
                                                    <option value = "11">Yes</option>
                                                    <option value = "12">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>H504.  How far to your home is the school of your child(ren) who attends the most distant school? </label>

                                                <select  name = "question24" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                    <option selected="selected"></option>
                                                    <option value = "5">5-10 minutes walk</option>
                                                    <option value = "6">10-15 minutes walk</option>
                                                    <option value = "7">15-30 minutes walk</option>
                                                    <option value="8">30-45 minutes walk</option>
                                                    <option value="9">More than 45 minutes walk</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>H505.  Who among the following is/are involved with your child(ren)’s homework</label>

                                                <select  name = "question25" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                    <option selected="selected"></option>
                                                    <option value = "66">Father</option>
                                                    <option value = "67">Mother</option>
                                                    <option value = "68">Family Members</option>
                                                    <option value = "69">Neighbours</option>
                                                    <option value = "70">Private Home Teacher</option>
                                                    <option value = "71">None</option>
                                                </select>
                                            </div>                                
                                        </div>


                                        <!-- /.col -->
                                        <hr/>
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>
                        <h4 style="padding-left:9px;"> School Information </h4>
                        <div class="box-body"  >
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>H207.  Regularly, how many meals are cooked in the household everyday? </label>
                                        <input  name="question26" required = "true" type = "text" class = "form-control" />
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>H207A.  Do children eat breakfast everyday? </label>
                                        <select  name = "question27" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>H207B.  Do you eat vegetables everyday? </label>
                                        <select  name = "question28" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>H207C.  Do you eat fruits everyday?</label>
                                        <select  name = "question29" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>H207D.  Do you eat meat or fish everyday? </label>
                                        <select  name = "question30" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>H208.  Does the HH have the following?</label>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">TV</label>
                                                <div class="input-group">
                                                    <select  name = "question31" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Computer</label>
                                                <div class="input-group">
                                                    <select  name = "question32" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Radio</label>
                                                <div class="input-group">
                                                    <select  name = "question33" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Telephone</label>
                                                <div class="input-group">
                                                    <select  name = "question34" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>

                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Animals (e.g cows, camels goats, donkeys & chickens)</label>
                                                <div class="input-group">
                                                    <select  name = "question35" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Pets</label>
                                                <div class="input-group">
                                                    <select  name = "question36" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>H209.  Does any member of the HH have the following means of transportation?</label>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Motor Vehicle</label>
                                                <div class="input-group">
                                                    <select  name = "question37" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Motor Bike</label>
                                                <div class="input-group">
                                                    <select  name = "question38" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Bicycle</label>
                                                <div class="input-group">
                                                    <select  name = "question39" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Motorized three-wheeler</label>
                                                <div class="input-group">
                                                    <select  name = "question40" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Camels, Donkeys, Horse</label>
                                                <div class="input-group">
                                                    <select  name = "question41" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Boat/Canoe</label>
                                                <div class="input-group">
                                                    <select  name = "question42" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                        <option selected="selected"></option>
                                                        <option value = "11">Yes</option>
                                                        <option value = "12">No</option>
                                                    </select>
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>

                        <hr/>
                        <div class="box-body"  >
                            <h4>H400.  Mother's/ Female Guardian</h4>
                            <div class="row" v-for = "i in iCount1">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type = "text" required = "true" name="guardian_mother[name][]" class = "form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Total number of children</label>
                                        <input type = "text" required = "true" name="guardian_mother[children_number][]" class = "form-control"  />
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Age</label>
                                        <input type = "text" required = "true" name="guardian_mother[age][]" class = "form-control"  />
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <!-- /.form-group -->
                                    <div class="form-group">

                                        <label>Ever attended School</label>
                                        <select  name = "guardian_mother[ever_attended_school][]" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>

                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">

                                        <label>If yes, highest level completed</label>
                                        <input type = "text" required = "true" class = "form-control" name="guardian_mother[highest_level_completed][]" />
                                    </div>
                                    <!-- /.form-group -->

                                </div>
                                <!-- /.col -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-footer" style="padding:9px;">
                                <button type="button" class="btn btn-danger pull-right" v-on:click= "decreaseICount1()">Remove Household</button> <span class="pull-right">&nbsp;&nbsp;</span>
                                <button type="button" class="btn btn-primary pull-right" v-on:click= "iCount1++">Add Household</button>
                            </div>
                            <div style="height:30px"></div>
                            <h4> Father's/ Male Guardian</h4>
                            <div class="row" v-for = "i in iCount2">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type = "text" required = "true" name="guardian_father[name][]" class = "form-control" />
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Total number of children</label>
                                        <input type = "text" required = "true" name="guardian_father[children_number][]" class = "form-control"  />
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Age</label>
                                        <input type = "text" required = "true" name="guardian_father[age][]" class = "form-control"  />
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">

                                        <label>Ever attended School</label>
                                        <select  name = "guardian_father[ever_attended_school][]" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>

                                    <div class="form-group">

                                        <label>If yes highest level completed</label>
                                        <input type = "text" required = "true" class = "form-control" name="guardian_father[highest_level_completed][]" />
                                    </div>
                                    <!-- /.form-group -->

                                </div>
                                <!-- /.col -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-footer" style="padding:9px;">
                                <button type="button" class="btn btn-danger pull-right" v-on:click= "decreaseICount2()">Remove Household</button> <span class="pull-right">&nbsp;&nbsp;</span>
                                <button type="button" class="btn btn-primary pull-right" v-on:click= "iCount2++">Add Guardian</button>
                            </div>
                        </div>
                        <hr/>


                        <div class="box-footer">
                            <div class="col-xs-4 pull-right">
                                <button type="submit" class="btn btn-primary btn-md btn-block btn-flat" id = "signin-button">Next</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->

                    <!-- /.box -->
                </section>
        </div>


        <!-- /.content -->

    </div>
    <!-- /.container -->
</div>
<!-- /.content-wrapper -->
<script>
    /* */
    var vue = new Vue({
        el: '#survey-school',
        data: {
            "localGovernments": [],
            "enumerationAreas": [],
            "iCount1": 1,
            "iCount2": 1,
            "localGovernmentId": 0,
            "question17_text": "",
            "question19": "",
        },
        methods: {
            increaseICount1() {
                this.iCount1 = this.iCount1 + 1;
            },
            decreaseICount1() {
                if (this.iCount1 > 1) {
                    this.iCount1 = this.iCount1 - 1;
                }
            }, increaseICount2() {
                this.iCount2 = this.iCount2 + 1;
            },
            decreaseICount2() {
                if (this.iCount2 > 0) {
                    this.iCount2 = this.iCount2 - 1;
                }
            }
        }});
</script>
@endsection