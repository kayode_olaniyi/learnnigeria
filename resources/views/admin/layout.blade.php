<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title')</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Compiled CSS -->
        <link rel="stylesheet" href="{{URL::asset('css/admin-app.css')}}">
        <link rel="stylesheet" href="{{URL::asset('admin/css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('admin/css/skins/_all-skins.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('admin/plugins/ionicons/css/ionicons.min.css')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}" id="csrf-token">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/vue"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="skin-blue  @if(empty($hide_sidebar)) hold-transition sidebar-mini fixed @else layout-top-nav @endif ">
        <!-- Navigation -->
        <header class="main-header" >
            <!-- Logo -->
            <a href="{{env('APP_URL')}}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>L</b>NG</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>LEARN</b> NIGERIA</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <div class="navbar-header">
                    <a href="{{env('APP_URL')}}" class="navbar-brand"><b>LEARN</b> NIGERIA</a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
                <!-- Sidebar toggle button-->
                @if(empty($hide_sidebar))
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                @endif
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{URL::asset('admin/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                                <span class="hidden-xs">{{auth()->user()->firstname}} {{auth()->user()->lastname}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{URL::asset('admin/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

                                    <p>
                                        {{auth()->user()->firstname}} {{auth()->user()->lastname}}
                                        <small>Member since Nov. 2012</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="{{route('user.logout')}}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->

                    </ul>
                </div>
            </nav>
        </header>
        @if(empty($hide_sidebar))
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{{URL::asset('admin/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>{{auth()->user()->firstname}} {{auth()->user()->lastname}}</p>

                    </div>
                </div>
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="{{$dashboard_menu ?? ''}}">
                        <a href="{{route('dashboard')}}">
                            <i class="fa fa-th"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="treeview {{$survey_menu ?? ''}}">
                        <a href="#">
                            <i class="fa fa-file-alt"></i> <span>Survey</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="active"><a href="{{route('survey.home')}}"><i class="fa fa-location-arrow"></i> Take Survey</a></li>
                            <li><a href="{{route('survey.list')}}"><i class="fa fa-list-ul"></i> View Surveys </a></li>
                        </ul>
                    </li>
                    <li class="{{$user_menu ?? ''}}">
                        <a href="{{route('users')}}">
                            <i class="fa fa-user"></i> <span>Manage Users</span>
                        </a>
                    </li>
                    <li class="treeview {{$populate_field_menu ?? ''}}">
                        <a href="#">
                            <i class="fa fa-code"></i> <span>Manage Codes</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="active"><a href="{{route('local_government.index')}}"><i class="fa fa-location-arrow"></i>Local Government</a></li>
                            <li><a href="{{route('enumeration_area.index')}}"><i class="fa fa-area-chart"></i>Enumeration Areas</a></li>
                        </ul>
                    </li>

  <!--<li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
  <li class="header">LABELS</li>
  <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
  <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
        @endif
        @yield('content')

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy; {{date('Y')}} <a href="https://adminlte.io">Learn Nigeria</a>.</strong> All rights
            reserved.
        </footer>
    
    </div>
    <script>
        var BASE_URL = "{{ env('APP_URL') }}/" ;
    </script>
    <script src="{{URL::asset('js/admin-app.js')}}"></script>
    <script>
        var Http = {
            GET: "GET",
            POST: "POST",
            STATUS_SUCCESSFUL: "success",
            STATUS_FAILED: "failed",
            /**
             *
             * @param {type} data
             * @param {type} callback
             * @returns {undefined}
             */
            get: function (data, callback) {
                this.request(Http.GET, data, callback);

            },
            /**
             *  post ajax
             * @param {type} data
             * @param {type} callback
             * @returns {undefined}
             */
            post: function (data, callback) {
                data.data["_token"] = $("#token").val();
                this.request(Http.POST, data, callback);
            },
            request: function (requestType, data, callback) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('#csrf-token').attr('content')
                    }
                });

                $.ajax({
                    type: requestType,
                    url: data.url,
                    data: data.data,
                    dataType: 'json',
                    success: function (data) {
                        callback(Http.STATUS_SUCCESSFUL, data);
                    }, error: function (data) {
                        callback(Http.STATUS_FAILED, data);
                    }
                })
            }

        };

        var AppManager = {
            SUCCESS_STATUS: 1,
            ERROR_STATUS: 2
        };
    </script>
    <script src="{{URL::asset('js/app.js')}}"></script>
    <script src="{{URL::asset('admin/js/adminlte.min.js')}}"></script>
    <script src="{{URL::asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{URL::asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{URL::asset('admin/js/pages/dashboard2.js')}}"></script>
    <script src="{{URL::asset('admin/js/demo.js')}}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2").select2({
                placeholder: 'Select an option'
            });
        });
    </script>
</body>

</html>
