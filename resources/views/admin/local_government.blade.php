@extends('admin.layout')
@section('title', 'See All Local Government')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div style="height:0px;"></div>
    <section class="content-header">
        <h1>
            Local Government
            <small>preview of all LG</small>
        </h1>

        <ol class="breadcrumb" style="margin-top:-10px;">
            <li><a href="{{route('local_government.create')}}" class="btn btn-primary" style="color:#ffffff !important;">Add Local Government</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        @if(session('local_government_editted'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i>Success!</h4>

            <div>The Local Government details was editted</div>

        </div>
        @endif
        @if(session('message'))
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <div>{{session('message')}}</div>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All created users</h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>State</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            @foreach ($local_governments as $local_government)
                            <tr>
                                <td>{{$local_government['id']}}</td>
                                <td>{{$local_government['name']}}</td>
                                <td>{{$local_government['code']}}</td>
                                <td>{{$local_government->state->name}}</td>
                                <td>{{$local_government['created_at']}}</td>
                                <td>@if($local_government['status'] == 1)<span class="label label-success">Approved</span> @elseif($local_government['status'] == 0) <span class="label label-danger">Denied</span> @endif</td>
                                <td>@if($local_government['status'] == 0)<a href="{{route('local_government.change_status',["id"=>$local_government['id']])}}">Approve</a> @elseif($local_government['status'] == 1) <a href="{{route('local_government.change_status',["id"=>$local_government['id']])}}">Deny</a> @endif &nbsp;&nbsp;
                                  @if($local_government['status'] == 1)  <a href="{{route('local_government.edit',["id"=>$local_government['id']])}}"> Edit Info</a> <a href="{{route('enumeration_area.create',["id"=>$local_government['id']])}}"> Create EA</a>@endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection