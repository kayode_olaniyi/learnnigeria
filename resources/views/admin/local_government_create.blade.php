@extends('admin.layout')
@section('title', 'Create Local Government')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div style="height:0px;"></div>
    <section class="content-header">
        <h1>
            Local Governments
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach
                </div>
                @endif

                @if(session('local_government_created'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i>Success!</h4>

                    <div>The Local Government {{session('local_government')->name}} has been created successfully</div>

                </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Local Government</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['url' => Route::current()->getName() == 'local_government.edit' ? route('local_government.edit.post') : route('local_government.create.post'),'role'=>'form']) !!}
                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Local Government</label>
                            {!! Form::text('name' , $localGovernment['name'] ?? '' ,["class"=>"form-control","placeholder"=>"Enter Local Government","id"=>"firstname"]) !!}
                             @if(Route::current()->getName() == 'local_government.edit')
                            {!! Form::hidden('id' , $localGovernment['id'] ?? '') !!}
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">LG Code</label>
                            {!! Form::text('code' , $localGovernment['code'] ?? '' ,["class"=>"form-control","placeholder"=>"Enter Local Government Code","id"=>"lastname"]) !!}

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">State</label>
                            <select name = 'state_id' class="form-control">
                                <option></option>
                                @foreach ($states as $state)
                                <option value="{{$state['id']}}"
                                        @if(Route::current()->getName() == 'local_government.edit' && $localGovernment->state->id == $state['id'])
                                        selected="selected"
                                        @endif
                                        >{{$state['name']}}</option>
                                @endforeach
                            </select>
                        </div> 

                       <!-- @if(!request()->edit)
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>

                            {!! Form::password('password',["class"=>"form-control","placeholder"=>"Enter Password","id"=>"password"]) !!}

                        </div>
                        @endif-->
                        <!-- <div class="checkbox">
                           <label>
                             <input type="checkbox"> Check me out
                           </label>
                         </div>-->
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">

                        <button type="submit" class="btn btn-primary">
                            @if(Route::current()->getName() == 'local_government.edit')
                            Edit Local Government
                            @else
                            Submit
                            @endif
                        </button>

                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
            <!-- right column -->

            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection