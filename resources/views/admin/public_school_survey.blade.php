@extends('admin.layout',['hide_sidebar'=>true])
@section('title', 'Survey')
@section('content')
<style>
    .input{
        height:45px;
    }
    .form-footer{
        padding-right:19px;
    }
    .remove-bold{
        font-weight: normal;
    }
</style>


<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Public School Survey
                 <!--<small>Example 2.0</small>-->
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section>

        <!-- Main content -->
        <div id="survey-school"><form method="post" action="{{route('survey.public_school_survey.post')}}">
            <section class="content">
                @if(session('errors'))
                <div class="alert alert-danger">

                </div>
                @endif
                @if(session('successful'))
                <div class="alert alert-sucess">
                    Data stored, loading the next page
                </div>
                @endif
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Respondents Details</h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Arrival Time</label>
                                    {{ csrf_field() }}
                                    <input type = "text" name = "question1" required = "true" class = "form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Time of Departure</label>
                                    <input type = "text" name = "question2" required = "true" class = "form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Respondent Name</label>
                                    <input type = "text" required = "true" name="question3"  class = "form-control"  />
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <!-- /.form-group -->
                                
                                <div class="form-group">
                                    <label>Respondent Designation</label>
                                    <input type = "text" required = "true" name="question4"  class = "form-control"  />
                                </div>
                                <div class="form-group">
                                    <label>Respondent Telephone Number</label>
                                    <input type = "text" required = "true" name="question5"  class = "form-control"  />
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <hr/>
                    <h4 style="padding-left:9px;">S100. School Information </h4>
                    <div class="box-body"  >
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>S101. School Name</label>
                                    <input  name="question6" required = "true" type = "text" class = "form-control" />
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>S102. Headteacher’s Name</label>
                                    <input type = "text" required = "true"  name = "question9" class = "form-control"/>
                                </div>
                                <div class="form-group">
                                    <label>S103. Type of School</label>
                                    <br/>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="question16[]" value="13">
                                            Day School
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="question16[]" value="14">
                                            Boarding School
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="question16[]" value="15">
                                            Boarding school with special needs
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="question16[]" value="16">
                                            Day school with special needs
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="question16[]" value="17">
                                            Day and Boarding school
                                        </label>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>S104. School Code</label>
                                    <input type = "text" required = "true"  name = "question7" class = "form-control"/>
                                </div>
                                <div class="form-group">
                                    <label>S105. School Email Address</label>
                                    <input type = "text" required = "true"  name = "question8" class = "form-control"/>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>S106. Is there a school-based management committee?</label>
                                    <select  name = "question11" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                        <option selected="selected"></option>
                                        <option value = "11">Yes</option>
                                        <option value = "12">No</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>S107. Is there PTA/Parents association?</label>
                                    <select  name = "question12" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                        <option selected="selected"></option>
                                        <option value = "11">Yes</option>
                                        <option value = "12">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>S108. Address of School</label>
                                    <input type = "text" required = "true"  name = "question13" class = "form-control"/>
                                </div>
                                <div class="form-group">
                                    <label>S109. Sex</label>
                                    <select  name = "question10" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                        <option selected="selected"></option>
                                        <option value = "3">Male</option>
                                        <option value = "4">Female</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>S110. Headteacher Telephone's Number</label>
                                    <input type = "text" required = "true"  name = "question14" class = "form-control"/>
                                </div>
                                <div class="form-group">
                                    <label>S111. Is Headteacher’s consent signature</label>

                                    <select  name = "question15" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                        <option selected="selected"></option>
                                        <option value = "11">Yes</option>
                                        <option value = "12">No</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <hr/>
                        </div>
                        <!-- /.row -->
                    </div>
                    <hr/>
                    <h4 style="padding-left:9px;">S200. What are the school's sources of funding and resources? (Tick all that apply) </h4>
                    <div class="box-body"  >
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>What are the school's sources of funding and resources? (Tick all that apply)</label>
                                    <br/>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="question17[]" value="18">
                                            Government school fees
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="question17[]" value="19">
                                            Parent-Teacher Associations 
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"name="question17[]" value="20">
                                            Private Organizations
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="question17[]" value="21">
                                            NGOs/ Foundation

                                        </label>
                                    </div>
                                    <label style="font-weight: normal;">Additional Sources of Income @{{question17_text}}</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" name="question17[]" v-model = "question17_checked" v-bind:value="question17_text">
                                        </span>
                                        <input class="form-control" type="text" :disabled = "question17_checked == ''" v-model = "question17_text">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">                                    
                            </div>
                            <!-- /.col -->
                            <hr/>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                    <hr/>
                    <h4 style="padding-left:9px;">S300. Pre-School Enrolment and Attendance </h4>
                    <div class="box-body"  >
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>S301. Enrolment(Take from register)(Girls)</label>
                                    <input  name="question29" required = "true" type = "text" class = "form-control" />
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>S301. Enrolment(Take from register)(Boys)</label>
                                    <input  name="question18" required = "true" type = "text" class = "form-control" />
                                </div>
                                <div class="form-group">
                                    <label>S301. Enrolment(Take from register)(Total)</label>
                                    <input  name="question184" required = "true" type = "text" class = "form-control" />
                                </div>
                            </div>
                                <!-- /.form-group -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>S302. Attendance on day of visit* (HeadCount)(Girls)</label>
                                    <input type = "text" required = "true"  name = "question30" class = "form-control"/>
                                </div>
                                <div class="form-group">
                                    <label>S302. Attendance on day of visit* (HeadCount)(Boys)</label>
                                    <input type = "text" required = "true"  name = "question19" class = "form-control"/>
                                </div>
                                <div class="form-group">
                                    <label>S302. Attendance on day of visit* (HeadCount)(Total)</label>
                                    <input  name="question185" required = "true" type = "text" class = "form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <h4 style="padding-left:9px;">S400. Primary School Enrolment and Attendance </h4>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>S401. Enrolment (Take from register)</label>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 1</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question20" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 2</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question21" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 3</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question22" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 4</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question31" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 5</label>
                                            <div class="input-group">
                                                <input class="form-control" name="question32" type="text" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 6</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question33" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                         <div class="col-lg-4">
                                            <label class="remove-bold">Enrolment Take From Register(Boys)</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question188" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                         <div class="col-lg-4">
                                            <label class="remove-bold">Enrolment Take From Register(Girls)</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question189" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>S402. Attendance on day of visit(Headcount)</label>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 1</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question23" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 2</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question24" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 3</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name = "question25"required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 4</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question34" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 5</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question35" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 6</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question36" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                         <div class="col-lg-4">
                                            <label class="remove-bold">Attendance on the Day of Visit(Boys)</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question190" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                         <div class="col-lg-4">
                                            <label class="remove-bold">Attendance on the Day of Visit(Girls)</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question191" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>S403. Number of Streams/Arms</label>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 1</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question26" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 2</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question27" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="remove-bold">Primary 3</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="question28" required="true">
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Primary 4</label>
                                                <div class="input-group">
                                                    <input class="form-control" type="text" name="question37" required="true">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Primary 5</label>
                                                <div class="input-group">
                                                    <input class="form-control" type="text" name="question38" required="true">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Primary 6</label>
                                                <div class="input-group">
                                                    <input class="form-control" type="text" name="question39" required="true">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <hr/>
                        </div>
                        <!-- /.row -->
                    </div>
                    <hr/>
                    <h4 style="padding-left:9px;">S500. Classroom Observation </h4>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>S501. Are the pupils of this class sitting with children from another class? </label>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 2</label>

                                            <select  name = "question40" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "11">Yes</option>
                                                <option value = "12">No</option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 6</label>

                                            <select  name = "question41" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "11">Yes</option>
                                                <option value = "12">No</option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>S502. Is there a usable blackboard/white board in the class </label>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 2</label>

                                            <select  name = "question48" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "11">Yes</option>
                                                <option value = "12">No</option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 6</label>

                                            <select  name = "question49" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "11">Yes</option>
                                                <option value = "12">No</option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>S503. Did most pupils (75%) have textbooks? </label>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 2</label>

                                            <select  name = "question42" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "11">Yes</option>
                                                <option value = "12">No</option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 6</label>

                                            <select  name = "question43" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "11">Yes</option>
                                                <option value = "12">No</option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>S504. Apart from textbooks, did you see any other supplementary material in the room? </label>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 2</label>

                                            <select  name = "question50" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "11">Yes</option>
                                                <option value = "12">No</option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 6</label>

                                            <select  name = "question51" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "11">Yes</option>
                                                <option value = "12">No</option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>S505. Where were the children seated? </label>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 2</label>

                                            <select  name = "question44" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "139">Classroom</option>
                                                <option value = "140">Veranda</option>
                                                <option value="141">Outdoors </option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="remove-bold">Primary 6</label>

                                            <select  name = "question45" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                <option selected="selected"></option>
                                                <option value = "139">Classroom</option>
                                                <option value = "140">Veranda</option>
                                                <option value="141">Outdoors </option>
                                            </select>

                                            <!-- /input-group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>S500. Comments</label>
                                    <textarea name="question54" class="form-control"></textarea>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <hr/>
                    <div class="box-footer">
                        <div class="col-xs-4 pull-right">
                            <button type="submit" class="btn btn-primary btn-md btn-block btn-flat" id = "signin-button">Next</button>
                        </div>
                    </div>
                </div>
                <!-- /.box -->

                <!-- /.box -->
            </section>
        </div>


        <!-- /.content -->

    </div>
    <!-- /.container -->
</div>
<!-- /.content-wrapper -->
<script>
    /* */
    var vue = new Vue({
        el: '#survey-school',
        data: {
            "localGovernments": [],
            "enumerationAreas": [],
            "iCount": 1,
            "localGovernmentId": 0,
            "question17_text": "",
            "question17_checked": ""
        },
        methods: {
            increaseICount() {
                this.iCount = this.iCount + 1;
            },
            decreaseICount() {
                if (this.iCount > 1) {
                    this.iCount = this.iCount - 1;
                }
            }
        }});
</script>
@endsection