@extends('admin.layout',['hide_sidebar'=>true])
@section('title', 'Survey')
@section('content')
<style>
    .input{
        height:45px;
    }
    .form-footer{
        padding-right:19px;
    }
    .remove-bold{
        font-weight: normal;
    }
</style>


<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Public School Survey
                 <!--<small>Example 2.0</small>-->
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section>

        <!-- Main content -->
        <div id="survey-apps"><form method="post" action="{{route('survey.public_school_survey2.post')}}">

                <section class="content">
                    @if(session('errors'))
                    <div class="alert alert-danger">

                    </div>
                    @endif
                    @if(session('successful'))
                    <div class="alert alert-sucess">
                        Data stored, loading the next page
                    </div>
                    @endif
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">S600. Number of Teaching Staff</h3>

                        </div>
                        <div class="box-body"  >
                            <div class="row">

                                <div class="col-md-6">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label>S601. Education </label>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Below WASSC/SSCE</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question170" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">WASSCE/SSCE</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name= "question171" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">OND/Diploma/ND</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question172" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">HND/Diploma</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question173" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">BA/BSc</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name= "question174" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">MA/MSc/M.Phil</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question175" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">PhD</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question176" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Others Please Specify</label>
                                                <div class="input-group">
                                                    <input class="form-control" name="question177" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>

                                    <!-- /.form-group -->
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>S602. Professional</label>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Grade II</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question178" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">NCE</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name= "question179" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">B.Ed</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question180" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">PGDE/PCE</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name= "question181" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">M.Ed</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question182" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Others Please Specify</label>
                                                <div class="input-group">
                                                    <input class="form-control" name="question183" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>S603. Number of teachers who have attended professional trainings, workshops and seminars (Government sponsored/Private) </label>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Within the past 12 months </label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question112" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">More than 12 months</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name= "question113" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Never</label>
                                                <div class="input-group">
                                                    <input class="form-control" required="true" name="question163" type="text">
                                                </div>
                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>                                
                                </div>
                                <!-- /.col -->
                                <hr/>
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>
                        <div class="box-body">
                            <h4>S700.  Teacher's Information  </h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>S701.  Total number of pre-primary school teachers </label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>
                                                <input class="form-control" required="true" name="question164" type="text">
                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input class="form-control" name="question165" required="true" type="text">

                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>S702.  Total number of pre-primary school teachers present on day of visit  (do a self count)</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input class="form-control" name="question114" required="true" type="text">

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input class="form-control" name= "question115" required="true" type="text">

                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>S703.  Total number of primary school teachers</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input class="form-control" name="question116" required="true" type="text">

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input class="form-control" name="question117" required="true" type="text">

                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>S704.  Total number of primary school teachers present on the day of the visit  (do a self count)</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input class="form-control" name="question118" required="true"  type="text">

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input class="form-control" name="question119" required="true" type="text">
                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>S705.  Number of non-teaching staff</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input class="form-control" name="question120" required="true"  type="text">

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input class="form-control"name="question121" required="true"  type="text">

                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>S706.  Number of teachers appointed by Governmnet (include the Headteacher)</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input class="form-control" name="question122" required="true" type="text">

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input class="form-control"name="question123"  required="true" type="text">

                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>S707.  Number of primary teachers with no teacher training certificate</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input class="form-control" name="question124"  required="true" type="text">

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input class="form-control" name="question125"  required="true" type="text">

                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>S708.  Other trained teachers employed by parents, sponsors, donors, community etc</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input class="form-control"  name="question126"  required="true" type="text">

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input class="form-control" name="question127" required="true"  type="text">

                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>S709.  Number of NYSC members, N-Power teachers, teachers who are donor funded or community volunteers etc.</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input class="form-control" name="question128" required="true"  type="text">

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input class="form-control" name="question129" required="true"  type="text">

                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="box-body">
                            <h4>S800.  Health and Nutrition</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>S801.  Does the school have a stocked first aid kit?</label>
                                        <select  name = "question130" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S802.  Is there a school nurse?</label>
                                        <select  name = "question131" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>S803.  Is there a school feeding programme?</label>
                                        <select  name = "question132" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S804.  How far is the nearest public health facility where children receive medical attention? </label>
                                        <select  name = "question133" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "4">5-10 minutes walk</option>
                                            <option value = "5">10-15 minutes walk</option>
                                            <option value = "6">15-30 minutes walk</option>
                                            <option value="7">30-45 minutes walk</option>
                                            <option value="8">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="box-body">
                                            <h4>S1200.  Stopped Attending School</h4>
                                            <div class="row">
                                                <div class="col-md-6">                            
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label>S1201.  How many children enrolled but stopped attending school?</label>
                                                        <input type="text" name = "question134" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>S1202.  Identify a common reason why most of them stopped attending </label>
                                                        <select  name = "question135" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                            <option selected="selected"></option>
                                                            <option value = "35">Security</option>
                                                            <option value = "36">Pregrnancy</option>
                                                            <option value = "37">No Money</option>
                                                            <option value = "38">Death</option>
                                                            <option value = "39">Distance</option>
                                                            <option value = "40">Others</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        <hr/>
                        <div class="box-body">
                            <h4>S900.  Information and Communications Technology</h4>
                            <div class="row">
                                <div class="col-md-6">
                                   <div class="form-group">
                                        <label>S901.  Is there a functional computer lab in the school </label>
                                        <select  name = "question136" v-model = "question136"  required = "true" class="form-control" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                        <br/>

                                        <input class="form-control" class="form-control" name="question186" type="text"  v-if = "question136 == '11'" >

                                    </div>
                                    <div class="form-group">
                                        <label>S902.  Does the school have functioning Internet access? </label>
                                        <select  name = "question137" v-model = "question137"  required = "true" class="form-control" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                        <br/>
                                        <input class="form-control" class="form-control" name = "question187" type="text"  v-if = "question137 == '11'" >
                                    </div>
                                       
                                    <div class="form-group">
                                        <label>S903.  How many functional computers are available for use by the students? (Observe and write number) </label>
                                        <input type="text" name = "question138"  required="true" class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>S904.  How many teachers have basic training in computer applications? </label>
                                        <input type="text" name = "question139" required="true" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label>S905.  Do you offer computer lessons to</label>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Pupils</label>

                                                <select  name = "question140" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                    <option selected="selected"></option>
                                                    <option value = "11">Yes</option>
                                                    <option value = "12">No</option>
                                                </select>

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Teachers</label>

                                                <select  name = "question141" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                    <option selected="selected"></option>
                                                    <option value = "11">Yes</option>
                                                    <option value = "12">No</option>
                                                </select>

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="remove-bold">Community Members</label>

                                                <select  name = "question142" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                                    <option selected="selected"></option>
                                                    <option value = "11">Yes</option>
                                                    <option value = "12">No</option>
                                                </select>

                                                <!-- /input-group -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>
                        <div class="box-body">
                            <h4>S1000.  Water and Sanitation </h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>S1001.  Source of water mainly used by the school?</label>
                                        <select  name = "question143" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "142">None</option>
                                            <option value = "143">Bore Hole</option>
                                            <option value = "144">Well</option>
                                            <option value = "145">Rain Water</option>
                                            <option value = "146">Pipe</option>
                                            <option value = "147">Buy water</option>
                                            <option value = "148">River</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1002.  Is the water source within the school compound?</label>
                                        <select  name = "question144" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1003.  Is there a functional hand-washing (Water and Soap) facility in your school? </label>
                                        <select  name = "question145" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1004.  Is the hand-washing facility close to the toilet? </label>
                                        <select  name = "question146" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label>S1005.  How many functional toilets / latrines for teachers?(Enter number)</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input type="text" name = "question147" required="true"class="form-control"/>

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input type="text" name="question148" required="true" class="form-control"/>
                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>S1006.  How many functional toilets / latrines for pupils?(Enter number)</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Male</label>

                                                <input type="text"  name="question149" required="true" class="form-control"/>

                                                <!-- /input-group -->
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="remove-bold">Female</label>

                                                <input type="text"  name="question150" required="true" class="form-control"/>

                                                <!-- /input-group -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                                                
                                
                            </div>
                        </div>
                        <hr/>
                        <div class="box-body">
                            <h4>S1100.  School Facilities </h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>S1101.  Total number of classrooms(do a self-count)</label>
                                        <input type="text" name="question151" required="true" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label>S1102.  Total number of classrooms currently being used by the pupils (do a self-count)</label>
                                        <input type="text" name="question152" required="true" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label>S1103.  Is there a complete boundary wall/fence? (The boundary wall/fence should not be broken/damaged</label>
                                        <select  name = "question153" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1104.  Is there a playground in the school?</label>
                                        <select  name = "question154" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1105.  Did you see a security personnel at the entrance of the school? E.g. a gateman</label>
                                        <select  name = "question155" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1106.  Did you see a library with books in the school?</label>
                                        <select  name = "question156" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                                                         
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>S1107.  Lightning mainly used in school</label>
                                        <br/>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name = "question157[]" value="149">
                                                Electricity
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name = "question157[]" value="41">
                                                Solar batteries
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name = "question157[]" value="42">
                                                Generator
                                            </label>
                                        </div>
                                        <label style="font-weight: normal;">Others Please specify</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input type="checkbox" name = "question157[]" v-bind:value="question157_text">
                                            </span>
                                            <input class="form-control" type="text" v-model = "question157_text">
                                        </div>
                                    </div>   
                                    <div class="form-group">
                                        <label>S1108.  Did you see a science laboratory in the school?</label>
                                        <select  name = "question158" required = "true" class="form-control" style="width: 100%;" >
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1109.  Did you see TV(s) in the school?</label>
                                        <select  name = "question159" required = "true" class="form-control" style="width: 100%;" >
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1110.  Did you see radio(s) in school?</label>
                                        <select  name = "question160" required = "true" class="form-control" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1111.  Does the school have a phone?</label>
                                        <select  name = "question161" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>S1112.  Did you see a Videodeck/DVD player(s) in the school?</label>
                                        <select  name = "question162" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <hr/>
                       
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-xs-4 pull-right">
                                <button type="submit" class="btn btn-primary btn-md btn-block btn-flat" id = "signin-button">Next</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->

                    <!-- /.box -->
                </section>
        </div>


        <!-- /.content -->

    </div>
    <!-- /.container -->
</div>
<!-- /.content-wrapper -->
<script>
    /* */
    var vue = new Vue({
        el: '#survey-apps',
        data: {
            "localGovernments": [],
            "enumerationAreas": [],
            "iCount": 1,
            "localGovernmentId": 0,
            "question109_text": "",
            "question110_text": "",
            "question157_text": "",
            "question136": "",
            "question137": ""
        },
        methods: {
            increaseICount() {
                this.iCount = this.iCount + 1;
            },
            decreaseICount() {
                if (this.iCount > 1) {
                    this.iCount = this.iCount - 1;
                }
            }
        }});
</script>
@endsection