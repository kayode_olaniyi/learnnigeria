@extends('admin.layout',['hide_sidebar'=>true])
@section('title', 'Survey')
@section('content')
<style>
.input{
    height:45px;
}
.form-footer{
    padding-right:19px;
}
</style>


<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Enumeration Area Observation Sheet
                <!--<small>Example 2.0</small>-->
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section>

        <!-- Main content -->
        <div id="survey-apps"><form method="post" action="{{route('survey.home.post')}}">
            <section class="content">
                @if(session('errors'))
                <div class="alert alert-danger">

                </div>
                @endif
                @if(session('successful'))
                <div class="alert alert-sucess">
                    Data stored, loading the next page
                </div>
                @endif
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">E100. General Information</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>State</label>
                                    <select class="form-control" v-model = "localGovernmentId"  v-on:change = "loadLocalGovernment()" style="width: 100%;">
                                        <option selected="selected"></option>
                                        @foreach($states as $state)
                                        <option value="{{$state['id']}}">{{$state['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>Local Government</label>
                                    <select class="form-control" required = "true" v-model = "eaId" v-on:change = "loadEA()" style="width: 100%;">
                                        <option selected="selected"></option>
                                        <option v-for = "lG in localGovernments" v-bind:value = "lG.id">@{{lG.name}}</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>Enumeration Area Code</label>
                                    <select class="form-control select2" name="survey[enumeration_area_id]" required = "true" id = "enumerationArea" style="width: 100%;">
                                        <option selected="selected"></option>
                                        <option  v-for = "enumerationArea in enumerationAreas" v-bind:value = "enumerationArea.id">@{{enumerationArea.code}}</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>Sector</label>
                                    <select class="form-control" required = "true"  name="survey[sector]"  style="width: 100%;">
                                        <option selected="selected"></option>
                                        <option value = "1">Rural</option>
                                        <option value = "2">Urban</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>Enumeration Area Head/Long Term Resident Name</label>
                                    {{ csrf_field() }}
                                    <input type = "text" name = "survey[name]" required = "true" class = "form-control" />
                                </div>
                                <div class="form-group">
                                    <label>EA Head/Long Term Resident Phone Number</label>
                                    <input type = "text" required = "true" name="survey[phone_number]"  class = "form-control"  />
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <hr/>
                    <h4 style="padding-left:9px;"> Volunteers </h4>
                    <div class="box-body"  >

                        <div class="row" v-for = "i in iCount">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input type = "text" required = "true" name="volunteers[name][]" class = "form-control" />
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>Sex</label>
                                    <select required = "true" class="form-control" name = "volunteers[sex][]">
                                        <option selected="selected"></option>
                                        <option value = "3">Male</option>
                                        <option value = "4">Female</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">     
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type = "text" required = "true" name="volunteers[mobile][]" class = "form-control"  />
                                </div>
                                <!-- /.form-group -->                           
                                <div class="form-group">
                          
                                    <label>Volunteer Code</label>
                                    <input type = "text" required = "true" class = "form-control" name="volunteers[code][]" />
                                </div>
                                <!-- /.form-group -->

                            </div>
                            <!-- /.col -->
                            <hr/>
                        </div>
                        <div class="form-footer" style="padding:9px;">
                            <button type="button" class="btn btn-danger pull-right" v-on:click= "decreaseICount()">Remove Volunteer</button> <span class="pull-right">&nbsp;&nbsp;</span>
                            <button type="button" class="btn btn-primary pull-right" v-on:click= "iCount++">Add Volunteer</button>
                        </div>
                        <!-- /.row -->
                    </div>
                    <hr/>
                    <h4 style="padding-left:9px;"> Supervisor </h4>
                    <div class="box-body"  >
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input  name="supervisor[name]" required = "true" type = "text" class = "form-control" />
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>Sex</label>
                                    <select  name = "supervisor[sex]" required = "true" class="form-control" style="width: 100%;" v-on:Cham>
                                        <option selected="selected"></option>
                                        <option value = "3">Male</option>
                                        <option value = "4">Female</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type = "text" required = "true"  name = "supervisor[mobile]" class = "form-control"/>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>Supervisor Code</label>
                                    <input type = "text" required = "true" name = "supervisor[code]" class = "form-control" />
                                </div>
                                <!-- /.form-group -->

                            </div>
                            <!-- /.col -->
                            <hr/>
                        </div>
                        <!-- /.row -->
                    </div>
                    <hr/>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-xs-4 pull-right">
                            <button type="submit" class="btn btn-primary btn-md btn-block btn-flat" id = "signin-button">Next</button>
                        </div>
                    </div>
                </div>
                <!-- /.box -->

                <!-- /.box -->
            </section>
        </div>


        <!-- /.content -->

    </div>
    <!-- /.container -->
</div>
<!-- /.content-wrapper -->
<script>
    /* */
    var vue = new Vue({
        el: '#survey-apps',
        data: {
            "localGovernments": [],
            "enumerationAreas": [],
            "iCount":1,
            "localGovernmentId":0,
            "eaId":0,
        },
        methods: {
            loadLocalGovernment() {
                console.log(this.localGovernmentId);
                axios.get(BASE_URL + "api/local-governments/" + this.localGovernmentId).then(response => {
                    console.log(response);
                    this.localGovernments = response.data;
                })
                
            },
            loadEA() {
                console.log(this.localGovernmentId);
                axios.get(BASE_URL + "api/enumeration-areas/" + this.eaId).then(response => {
                    this.enumerationAreas = response.data;
                })
            }, increaseICount(){
                this.iCount = this.iCount + 1;
            },
            decreaseICount(){
                if (this.iCount > 1){
                    this.iCount = this.iCount - 1;
                }
            }
        } });
    </script>
    @endsection