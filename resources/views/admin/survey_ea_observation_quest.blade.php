@extends('admin.layout',['hide_sidebar'=>true])
@section('title', 'EA Observation Sheet')
@section('content')
<style>
    .input{
        height:45px;
    }
    .form-footer{
        padding-right:19px;
    }
</style>


<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Enumeration Area Observation Sheet
                <!--<small>Example 2.0</small>-->
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section>

        <!-- Main content -->
        <div id="survey-apps">
            <form method="post" action="{{route('survey.questions.post')}}">
                {{ csrf_field() }}
                <section class="content">
                    @if(session('errors'))
                    <div class="alert alert-danger">

                    </div>
                    @endif
                    @if(session('successful'))
                    <div class="alert alert-sucess">
                        Data stored, loading the next page
                    </div>
                    @endif
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">E200. General Facilities (Ask)</h3><br>
                            <h4 class="box-title">How far are the following facilities to the EA?</h4>
                        </div>
                        <div class="box-body"  >
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E201. Supermarket / market center of 5 or more shops</label>
                                        <select class="form-control" name = "question1" required="true" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>E202. Bank</label>
                                        <select class="form-control" name = "question2" required="true" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>E203. Religious Building? (Government/Private)</label>
                                        <select class="form-control" name = "question3" required="true" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>E204. Tarred road?</label>
                                        <select class="form-control" name = "question4" required="true" style="width: 100%;">
                                             <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>E205. All weather road?</label>
                                        <select class="form-control" name = "question5" required="true" style="width: 100%;">
                                           <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E206. Access to portable water?</label>
                                        <select class="form-control" name = "question6" required="true" style="width: 100%;">
                                              <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>E207. Internet Cafe?</label>
                                        <select class="form-control" name = "question7" required="true" style="width: 100%;">
                                             <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>E208. Town Hall/Village Square?</label>
                                        <select class="form-control" name = "question8" required="true" style="width: 100%;">
                                           <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>E209. Police Station?</label>
                                        <select class="form-control"  name = "question9" required="true" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>E210. Adult Literacy Learning Center?</label>
                                        <select class="form-control" name = "question10" required="true" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>

                        </div>
                        <hr/>
                        <h4 style="padding-left:9px;">E300. Education Status </h4>
                        <h4 style="padding-left:9px;">How far are the following to the EA?</h4>
                        <div class="box-body"  >
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E301. Public Primary School/s?</label>
                                        <select class="form-control"  name = "question11" required="true" style="width: 100%;">
                                             <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>E302. Private Primary Schools?</label>
                                        <select class="form-control" name = "question12" required="true" style="width: 100%;">
                                             <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>E303. Public Junior Secondary School/s?</label>
                                        <select class="form-control" name = "question13" required="true" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "4">5-10 minutes walk</option>
                                            <option value = "5">10-15 minutes walk</option>
                                            <option value = "6">15-30 minutes walk</option>
                                            <option value="7">30-45 minutes walk</option>
                                            <option value="8">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>E304. Private Junior Secondary School/s?</label>
                                        <select class="form-control" name = "question14" required="true" style="width: 100%;">
                                             <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E305. Public Senior Secondary School/s?</label>
                                        <select class="form-control" name = "question15" required="true" style="width: 100%;">
                                             <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>E306. Private Senior Secondary School/s?</label>
                                        <select class="form-control" name = "question16" required="true" style="width: 100%;">
                                             <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>E307. Public Tetiary Institution?</label>
                                        <select class="form-control" name = "question17" required="true" style="width: 100%;">
                                              <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>E308. Private Tetiary Institution</label>
                                        <select class="form-control" name = "question18" required="true" style="width: 100%;">
                                             <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>

                        <div class="box-body"  >
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E400. What is the main economic activity in the EA</label>
                                        <input type = "text" required = "true" name="question20" class = "form-control"  />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E500. What is predominant local language</label>
                                        <input type = "text" required = "true" name="question21" class = "form-control"  />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <h4 style="padding-left:9px;">E600. Health Status </h4>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>E601. How far from this community is the closest health facility?(Ask) (Clinic/Hospital)</label>
                                        <select class="form-control" required="true" name = "question22" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "5">5-10 minutes walk</option>
                                            <option value = "6">10-15 minutes walk</option>
                                            <option value = "7">15-30 minutes walk</option>
                                            <option value="8">30-45 minutes walk</option>
                                            <option value="9">More than 45 minutes walk</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>E602. Has any health worker visited the EA in the last 3 Months</label>
                                        <select class="form-control"required="true" name = "question23" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E603. Is there a trained health worker residing in the EA</label>
                                        <select class="form-control" required="true" name = "question24" style="width: 100%;">
                                            <option selected="selected"></option>
                                            <option value = "11">Yes</option>
                                            <option value = "12">No</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <hr/>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-xs-4 pull-right">
                                <button type="submit" class="btn btn-primary btn-md btn-block btn-flat" id = "signin-button">Next</button>
                            </div>
                        </div>
                    </div>
                </section>
        </div>
    </div>
    <!-- /.container -->
</div>
@endsection