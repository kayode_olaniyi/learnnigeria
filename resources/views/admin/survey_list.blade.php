@extends('admin.layout')
@section('title', 'See All Surveys')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div style="height:0px;"></div>
    <section class="content-header">
        <h1>
            Surveys
            <small>preview of all Surveys</small>
        </h1>

        <ol class="breadcrumb" style="margin-top:-10px;">
            <li><a href="{{route('local_government.create')}}" class="btn btn-primary" style="color:#ffffff !important;">Start A New Survey</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        @if(session('local_government_editted'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i>Success!</h4>

            <div>The Local Government details was editted</div>

        </div>
        @endif
        @if(session('message'))
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <div>{{session('message')}}</div>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <p class="lead" style="padding:9px;"> Total Household Filled <strong>{{$householdsCount['householdObservation']}}</strong>
                            &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Household Information Filled <strong>{{$householdsCount['householdInformation']}}</strong></p>
                        <p class="lead"> </p>


                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover">
                            <tr>
                                <th>ID</th>
                                <th>Resident Name</th>
                                <th>Enumeration Area</th>
                                @if($role->id == 1)
                                <th>Data Entry Personnel</th>
                                @endif
                                {{--<th>Locality</th>
                                <th>RIC</th>--}}
                                <th>Date</th>
                                <th>HouseholdObservations</th>
                                <th>HouseholdInformation</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            @foreach ($surveys as $survey)
                            <tr>
                                <td>{{$survey['id']}}</td>
                                <td>{{$survey['resident_name']}}</td>
                                @if($role->id == 1)
                                <td>{{$survey->enumerationArea->name}}</td>
                                @endif
                                <td>{{$survey->user->username}}</td>
                                {{-- <td>{{$survey['locality']}}</td>
                                <td>{{$survey['ric']}}</td> --}}
                                <td>{{$survey['survey_date']}}</td>
                                @if($survey->householdObservation != null)
                                <td>{{$householdResult[$survey->id]['household']}}</td>
                                <td>{{$householdResult[$survey->id]['householdInformation']}}</td>
                                @else
                                <td>0</td>
                                <td>0</td>
                                @endif
                                <td>@if($survey['status'] == 1)<span class="label label-success">Completed</span> @elseif($survey['status'] == 0) <span class="label label-danger">Incomplete</span> @endif</td>
                                <td>@if($survey['status'] == 0)<a href="{{route('survey.home',["id"=>$survey['id']])}}">Continue Survey</a>@endif &nbsp;&nbsp;
                                </td>
                                <td> <a href="{{route('survey.view',["id"=>$survey['id']])}}">View Details</a></td>
                            </tr>
                            @endforeach
                        </table>
                        {{$surveys->links()}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection