@extends('admin.layout',['hide_sidebar'=>true])
@section('title', 'Survey')

@section('content')
<div class="container">
    <div class="row text-center">
        <div class="col-sm-6 col-sm-offset-3">
            <br><br> <h2 style="color:#0fad00">Success</h2>
            <img src="http://osmhotels.com//assets/check-true.jpg">
            <h3>Dear, {{auth()->user()->firstname}} {{auth()->user()->lastname}}</h3>
            <p style="font-size:20px;color:#5C5C5C;">Thank you for Filling this Survey</p>
            <a href="{{route('survey.home')}}" class="btn btn-success">Take Another Survey</a>
            <br><br>
        </div>

    </div>
</div>
<div style="height:280px;"></div>
@endsection
