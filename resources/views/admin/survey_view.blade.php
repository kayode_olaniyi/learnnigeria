@extends('admin.layout')
@section('title', 'See All Surveys')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div style="height:0px;"></div>
    <section class="content-header">
        <h1>
            Surveys
            <small>preview of all Surveys</small>
        </h1>

        <ol class="breadcrumb" style="margin-top:-10px;">
            <li><a href="{{route('local_government.create')}}" class="btn btn-primary" style="color:#ffffff !important;">Start A New Survey</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        @if(session('local_government_editted'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i>Success!</h4>

            <div>The Local Government details was editted</div>

        </div>
        @endif
        @if(session('message'))
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <div>{{session('message')}}</div>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <section class="invoice">
                            <!-- title row -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        <i class="fa fa-globe"></i> Survey Details
                                        <small class="pull-right">Date: 2/10/2014</small>
                                    </h2>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- info row -->
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    <p><strong>Sector: </strong> {{$survey->sector}} </p>
                                    <p><strong>Locality: </strong> {{$survey->locality}} </p>

                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <p><strong>Resident Name: </strong> {{$survey->resident_name}} </p>
                                    <p><strong>Resident Mobile: </strong> {{$survey->resident_phone_number}} </p>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <p><strong>RIC: </strong> {{$survey->ric}} </p>
                                    <p><strong>Enumeration Area: </strong> {{$survey->enumerationArea->name}}</p>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                            <!-- Table row -->
                            <div class="row">
                                <br/>
                                <strong><p class = "lead">Volunteers</p></strong>

                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Sex</th>
                                                <th>Mobile</th>
                                                <th>Code</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($volunteers as $volunteer)
                                            <tr>
                                                <td>{{$volunteer->name}}</td>
                                                <td>
                                                    @if($volunteer->sex == 3)
                                                    Male
                                                    @elseif($volunteer->sex == 4)
                                                    Female
                                                    @endif

                                                <td> {{$volunteer->mobile}} </td>
                                                <td> {{$volunteer->code}} </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- Table row -->
                            <div class="row">
                                <br/>
                                <strong><p class = "lead">Enumeration Observation Area Result</p></strong>

                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Question</th>
                                                <th>Answer</th>
                                                <th>Created At</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($ea_observations as $ea_observation)
                                            <tr>
                                                <td>{{$ea_observation->question->question}}</td>
                                                @if($ea_observation->answer->id == 10)
                                                <td>{{$ea_observation->text}}</td>
                                                @else
                                                <td>{{$ea_observation->answer->answer}}</td>
                                                @endif
                                                <td> {{$ea_observation->created_at}} </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                            <!-- /.row -->
                            <!-- Table row -->
                            <div class="row">
                                <br/>
                                <strong><p class = "lead">Households</p></strong>

                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Household Head</th>
                                                <th>HH Number</th>
                                                <th>Building No</th>
                                                <th>Address</th>
                                                <th>Created At</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($households as $household)
                                            <tr>
                                                <td>{{$household->household_head}}</td>
                                                <td>{{$household->hh_number}}</td> 
                                                <td>{{$household->building_no}} </td>
                                                <td> {{$household->address}} </td>
                                                <td> {{$household->created_at}} </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>

                            <div class="row">
                                <br/>
                                <strong><p class = "lead">School Survey</p></strong>

                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Question</th>
                                                <th>Answer</th>
                                                <th>Created At</th>
                                                <th>School Type</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($schoolResults as $schoolResult)

                                            <tr>
                                                <td>{{$schoolResult->question->question}}</td>
                                                @if($schoolResult->answer)
                                                @if($schoolResult->answer->id == 10)
                                                <td>{{$schoolResult->text}}</td> 
                                                @else
                                                <td>{{$schoolResult->answer->answer}}</td> 
                                                @endif
                                                @else
                                                <td></td>
                                                @endif
                                                <td>{{$schoolResult->created_at}} </td>
                                                <td> {{$schoolResult->address}} </td>
                                                @if($schoolResult->school_type == 1)
                                                <td> Public School</td>
                                                @elseif($schoolResult->school_type == 2)
                                                <td>Private School</td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <div class="row">
                                <h1>Household Observation</h1>
                                @foreach ($householdObservations as $householdObservation)
                                <div class="col-xs-6">
                                    <p class="lead">Household Data for Building {{$householdObservation->building_no}}</p>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Question</th>
                                                <th>Answer</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($householdObservation->householdResult as $result)
                                            <tr>
                                                <td>{{$result->question->question}}</td>
                                                @if($result->answer != null)
                                                @if($result->answer->id == 10)
                                                <td>{{$result->text}}</td>
                                                @else
                                                <td>{{$result->answer->answer}}</td> 
                                                @endif
                                                @else
                                                <td></td>
                                                @endif

                                            </tr>
                                            @endforeach
                                        </tbody> 
                                    </table>
                                    <p class="lead">Household Information</p>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Question</th>
                                                <th>Answer</th>
                                                <th> Child ID </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($householdObservation->householdInformationResult as $result)
                                            <tr>
                                                <td>{{$result->question->question}}</td>
                                                @if($result->answer != null)
                                                @if($result->answer->id == 10)
                                                <td>{{$result->text}}</td>
                                                @elseif($result->question->type == 3)
                                                <td>{{$result->answer->id}}</td>
                                                @else
                                                <td>{{$result->answer->answer}}</td> 
                                                @endif
                                                @else
                                                <td></td>
                                                @endif
                                                <td> {{$result->child_id}}</td>

                                            </tr>
                                            @endforeach
                                        </tbody> 
                                    </table>
                                    <p class="lead"> House Guardians </p>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Age</th>
                                                <th>Children Number</th>
                                                <th>Ever Attended School</th>
                                                <th>Highest Level Completed</th>
                                                <th>Father/Mother</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($householdObservation->householdGuardian as $guardian)
                                            <tr>
                                                <td>{{$guardian->name}}</td>
                                                <td>{{$guardian->age}}</td>
                                                <td>{{$guardian->children_number}}</td>
                                                @if($guardian->ever_attended_school == 11)
                                                <td>Yes</td>
                                                @elseif($guardian->ever_attended_school == 12)
                                                <td>No</td>
                                                @endif
                                                <td>{{$guardian->highest_level_completed}}</td>
                                                @if($guardian->type == 1)
                                                <td>Mother</td>
                                                @elseif($guardian->type == 2)
                                                <td>Father</td>
                                                @endif
                                            </tr>


                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                @endforeach
                                <div class="col-xs-6">
                                    <p class="lead">Enumeration Area Compilation</p>
                                    <div class="row">
                                        <div class="col-xs-6"></div>   
                                        <div class="col-xs-6"></div>
                                    </div>
                                    <img src="../../dist/img/credit/visa.png" alt="Visa">
                                    <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                                    <img src="../../dist/img/credit/american-express.png" alt="American Express">
                                    <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
                                        dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                                    </p>
                                </div>
                            </div>

                            <!-- this row will not appear when printing -->
                            <div class="row no-print">
                                <div class="col-xs-12">
                                    <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                                    <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
                                    </button>
                                    <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                                        <i class="fa fa-download"></i> Generate PDF
                                    </button>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection