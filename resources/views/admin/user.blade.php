@extends('admin.layout')
@section('title', 'Dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div style="height:0px;"></div>
    <section class="content-header">
        <h1>
            Accredited Users
            <small>preview of all users</small>
        </h1>

        <ol class="breadcrumb" style="margin-top:-10px;">
            <li><a href="{{route('user.create')}}" class="btn btn-primary" style="color:#ffffff !important;">New User</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        @if(session('user_editted'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i>Success!</h4>

            <div>The user details was editted</div>

        </div>
        @endif
        @if(session('message'))
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <div>{{session('message')}}</div>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All created users</h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>ID</th>
                                <th>Username</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{$user['id']}}</td>
                                <td>{{$user['username']}}</td>
                                <td>{{$user['firstname']}} {{$user['lastname']}}</td>
                                <td>{{$user['created_at']}}</td>
                                <td>@if($user['status'] == 1)<span class="label label-success">Approved</span> @elseif($user['status'] == 0) <span class="label label-danger">Denied</span> @endif</td>
                                <td>@if($user['status'] == 0)<a href="{{route('user.change_status',["id"=>$user['id']])}}">Approve</a> @elseif($user['status'] == 1) <a href="{{route('user.change_status',["id"=>$user['id']])}}">Deny</a> @endif &nbsp;&nbsp;
                                    <a href="{{route('user.create')}}?edit=true&user={{$user['id']}}"> Edit Info</a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection