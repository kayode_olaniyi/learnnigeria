@extends('admin.layout')
@section('title', 'Create User')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div style="height:0px;"></div>
    <section class="content-header">
        <h1>
            Accredited Users
            <small>preview of all users</small>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach
                </div>
                @endif

                @if(session('user_created'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i>Success!</h4>

                    <div>The user {{$user->username}} has been created successfully</div>

                </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['url' => request()->edit ? route('user.edit.post') : route('user.create.post'),'role'=>'form']) !!}
                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Username</label>
                            {!! Form::text('username' , $user['username'] ?? '' ,["class"=>"form-control","placeholder"=>"Enter Username","id"=>"username",!request()->edit ? "" : "disabled"=>"disabled"]) !!}
                            @if(request()->edit)
                            {!! Form::hidden('user_id' , $user['id'] ?? '') !!}
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Firstname</label>
                            {!! Form::text('firstname' , $user['firstname'] ?? '' ,["class"=>"form-control","placeholder"=>"Enter Firstname","id"=>"firstname"]) !!}

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Lastname</label>
                            {!! Form::text('lastname' , $user['lastname'] ?? '' ,["class"=>"form-control","placeholder"=>"Enter Lastname","id"=>"lastname"]) !!}

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Role</label>
                            <select name = 'role' class="form-control">
                                <option></option>
                                @foreach ($roles as $role)
                                <option value="{{$role['id']}}" 
                                        @if(request()->edit && $user->roles()->first()->id == $role['id'])
                                        selected="selected"
                                        @endif
                                        >{{$role['title']}}</option>
                                @endforeach
                            </select>
                        </div> 

                        @if(!request()->edit)
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>

                            {!! Form::password('password',["class"=>"form-control","placeholder"=>"Enter Password","id"=>"password"]) !!}

                        </div>
                        @endif
                        <!-- <div class="checkbox">
                           <label>
                             <input type="checkbox"> Check me out
                           </label>
                         </div>-->
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">

                        <button type="submit" class="btn btn-primary">
                            @if(request()->edit)
                            Edit User
                            @else
                            Submit
                            @endif
                        </button>

                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
            <!-- right column -->

            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection