@extends('layout')

@section('title', 'LEARN NIGERIA')

@section('content')

<nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation" style = "background:transparent; border-color:transparent;">
    <div class="container topnav" id="startchange">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand topnav" id="title-text" href="{{URL::to('/')}}" style="font-size:28px; font-weight: normal; color: #FFF;">Learn Nigeria</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" id="home-navbar" >

              <!--  <li>
                    <a href="#about" class="links">Blog</a>
                </li>
                <li>
                    <a href="#services" class="links">Learning Style</a>
                </li>
                <li>
                    <a href="{{URL::to('/dashboard')}}" class="links">Personality Type</a>
                </li>-->
                <li>
                    <a href="{{URL::to('/dashboard')}}" class="links"> <i class="fa fa-phone"></i>&nbsp;08148380710</a>
                </li>
                <li style="padding:9px;">
                    <a class =  "btn btn-info" href="{{ route('login') }}" style="width: 80px; height: 35px; padding: 5px !important;">Login</a>
                </li>
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Header -->
<a name="about"></a>
<div class="intro-header">
  <div class="mask"></div>
    <div class="container intro-container">
        <div class="row">
          <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="intro-message">
                    <h1 class = "main-title">Let's Engage, Assess & Report Nigeria</h1>
                    <br/>
                    <h4>Discover your personality type as a surveyor</h4>
                    <br/>
                    <a href="{{URL::to('/assessment')}}" class="btn btn-info btn-lg btn-block action-button" > 
                <span class="network-name">Begin Survey</span></a>

                        <!--<li>
                            <a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->

</div>
<!-- /.intro-header -->

<!-- Page Content -->

<a  name="services"></a>
<div class="content-section-a">

    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h2 class="section-heading">Our Goal</h2>
                <p class="lead"> To use the citizen-led assessment model 
                to strengthen citizen agency and policy advocacy in the education sector of Nigeria.</p>
            </div>
            <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                <img class="img-responsive" src="{{URL::asset('public/frontend/img/gentella.png')}}" alt="">
            </div>
        </div>

    </div>
    <!-- /.container -->

</div>
<!-- /.content-section-a -->

<div class="content-section-b">

    <div class="container">

        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h2 class="section-heading">Our Reach</h2>
                <p class="lead">LEARNigeria aims to extend its reach all over Nigeria.
                 We are currently in Ebonyi, Akwa Ibom, Plateau,Taraba, Kano and Lagos States.</div>
            <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                <img class="img-responsive" src="{{URL::asset('public/frontend/img/dog.png')}}" alt="">
            </div>
        </div>

    </div>
    <!-- /.container -->

</div>
<!-- /.content-section-a -->

<a  name="contact"></a>
<div class="banner">

    <div class="container">

        <div class="row">
            <div class="col-lg-6">
                <h2>Login to take survey</h2>
            </div>
            <div class="col-lg-6">
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                    </li>
                    <li>
                        <a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    <!-- /.container -->

</div>
<!-- /.banner -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--<ul class="list-inline">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#about">About</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#services">Services</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                </ul>-->
                <p class="copyright text-muted small">Copyright &copy; Your Company 2018. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
<script>

    $(document).ready(function () {
        var scroll_start = 0;
        var startchange = $('#startchange');
        var offset = startchange.offset();

        if (startchange.length) {
            $(document).scroll(function () {
                scroll_start = $(this).scrollTop();
                if (scroll_start > offset.top) {
                    $(".navbar-default").css('background-color', '#ffffff');
                    $("#home-navbar > li > a.links").css({"color": "#777", "font-weight": "normal"});
                    $("#title-text").css({"color": "#777"});
                } else {
                    $('.navbar-default').css('background-color', 'transparent');
                    $("#home-navbar > li > a.links").css({"color": "#FFFFFF", "font-weight": "400"});
                    $("#title-text").css({"color": "#FFFFFF"});
                }
            });
        }
    });

</script>
<!-- Footer -->
@endsection
