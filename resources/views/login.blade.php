<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>LEARNigeria | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{URL::asset('css/admin-app.css')}}">
        <link rel="stylesheet" href="{{URL::asset('admin/css/AdminLTE.min.css')}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            #signin-button{
                border-radius:20px;
            }
        </style>
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box" style = "width:450px;">
            <div class="login-logo">
                <a href="../../index2.html"><b>LEARN</b>NIGERIA</a>
            </div>
            <!-- /.login-logo -->
             @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                    <span>{{ $error }}</span>
                    @endforeach
                </div>
                @endif
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>

                {!! Form::open(['url' => route('login.update'),'role'=>'form']) !!}
                <div class="form-group has-feedback">

                    {!! Form::text('username' , '',[ "class"=>"form-control", "placeholder"=>"Enter Username","id"=>"username", "style" => "height:45px;" ]) !!}
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback" style = "height:5px;">

                </div>
                <div class="form-group has-feedback">
                    {!! Form::password('password',["class"=>"form-control","placeholder"=>"Enter Password","id"=>"password", "style" => "height:45px;" ]) !!}

                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <!--<div class="checkbox icheck">
                          <label>
                            <input type="checkbox"> Remember Me
                          </la    bel>
                        </div>-->
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-lg btn-block btn-flat" id = "signin-button">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
                {!! Form::close() !!}
               


                <!--  <div class="social-auth-links text-center">
                    <p>- OR -</p>
                    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                      Facebook</a>
                    <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                      Google+</a>
                  </div>-->
                <!-- /.social-auth-links -->

            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 3 -->
        <script src="{{URL::asset('js/app.js')}}"></script>
        <script src="{{URL::asset('js/admin-app.js')}}"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
        </script>
    </body>
</html>
