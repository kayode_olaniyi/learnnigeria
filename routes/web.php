<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

$frontendPath = public_path() . "/frontend";


Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/', function () {
        return view('home');
    });
});

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::post('/login', 'Auth\LoginController@login')->name('login.update');

Route::get('/users', 'UserController@index')->name('users');
Route::get("/user/create", "UserCreateController@index")->name('user.create');
Route::post("/user/create", "UserCreateController@create")->name('user.create.post');
Route::post("/user/edit", "UserCreateController@edit")->name('user.edit.post');
Route::get('/user/change-status/{id}', "UserController@changeStatus")->name('user.change_status');
Route::get("/logout", "UserController@logout")->name("user.logout");

Route::get('/dashboard', 'DashboardController@get')->name('dashboard');
Route::get('/download-report/{state?}{local_government?}{ea?}','DownloadReportController@get')->name('download.report');
Route::get('/survey/{id?}', 'SurveyController@index')->name('survey.home')->where('id', '[0-9]+');;


Route::get('/local-government', 'LocalGovernmentController@index')->name('local_government.index');
Route::get('/local-government/create', 'LocalGovernmentCreateController@index')->name('local_government.create');
Route::post('/local-government/create', 'LocalGovernmentCreateController@create')->name('local_government.create.post');
Route::get('/local-government/change-status/{id}', "LocalGovernmentController@changeStatus")->name('local_government.change_status');
Route::get('/local-government/edit/{id}', 'LocalGovernmentCreateController@editIndex')->name('local_government.edit');
Route::post('/local-government/edit', 'LocalGovernmentCreateController@edit')->name('local_government.edit.post');


Route::get('/enumeration-areas', 'EnumerationAreaController@index')->name('enumeration_area.index');
Route::get('/enumeration-areas/create/{id}', 'EnumerationAreaCreateController@index')->name('enumeration_area.create');
Route::post('/enumeration-areas/create}', 'EnumerationAreaCreateController@create')->name('enumeration_area.create.post');
Route::get('/enumeration-areas/change-status/{id}', "EnumerationAreaController@changeStatus")->name('enumeration_area.change_status');
Route::get('/enumeration-areas/edit/{id}', 'EnumerationAreaCreateController@editIndex')->name('enumeration_area.edit');
Route::post('/enumeration-areas/edit', 'EnumerationAreaCreateController@edit')->name('enumeration_area.edit.post');


Route::post("/survey", 'SurveyController@createSurvey')->name('survey.home.post');
Route::get("/survey/ea-observation/sheet", "SurveyController@loadQuestions")->name("survey.questions");
Route::post("/survey/ea-observation/sheet", "SurveyController@createQuestions")->name("survey.questions.post");

Route::get("survey/household-observation", "HouseholdController@loadObservationIndex")->name("survey.household.observation");

Route::post("survey/household-observation", "HouseholdController@submitHouseholdObservationSurvey")->name("survey.household.observation.post");
Route::get("survey/household", "HouseholdController@loadIndex")->name("survey.household");
Route::post("survey/household", "HouseholdController@createHousehold")->name("survey.household.post");
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route::get("/survey/public-school-survey", "PublicSchoolSurveyController@loadIndex")->name('survey.public_school_survey');
Route::post("/survey/public-school-survey", "PublicSchoolSurveyController@createPublicSurveySheet1")->name('survey.public_school_survey.post');
Route::get("/survey/public-school-survey/2", "PublicSchoolSurveyController@loadSecondIndex")->name('survey.public_school_survey2');
Route::post("/survey/public-school-survey/2", "PublicSchoolSurveyController@createPublicSurveySheet2")->name('survey.public_school_survey2.post');

Route::get("/survey/private-school-survey", "PrivateSchoolSurveyController@loadIndex")->name('survey.private_school_survey');
Route::post("/survey/private-school-survey", "PrivateSchoolSurveyController@createPrivateSurveySheet1")->name('survey.private_school_survey.post');
Route::get("/survey/private-school-survey/2", "PrivateSchoolSurveyController@loadSecondIndex")->name('survey.private_school_survey2');
Route::post("/survey/private-school-survey/2", "PrivateSchoolSurveyController@createPrivateSurveySheet2")->name('survey.private_school_survey2.post');

Route::get("/survey/household-information", "HouseholdInformationController@index")->name('survey.household_information');
Route::post("/survey/household-information", "HouseholdInformationController@createInformation")->name('survey.household_information.create');
Route::get("/survey/ea-compilation", "EACompilationController@index")->name("survey.ea_compilation");
Route::post("/survey/ea-compilation", "EACompilationController@createEACompilation")->name("survey.ea_compilation.post");

Route::get("/survey/success", "SurveyController@success")->name('survey.success');
Route::get("/survey/view/{id}", "SurveyViewController@view")->name('survey.view');
Route::get("/survey/list","SurveyViewController@listSurveys")->name('survey.list');
/*
 * Api Development
 */

Route::get("/api/states", 'StateController@apiIndex');
Route::get("/api/local-governments/{id}", "StateController@apiLoadLG");
Route::get("api/enumeration-areas/{id}", "StateController@loadEA");
Route::post("api/survey/create-survey", "SurveyApiController@createSurvey");
