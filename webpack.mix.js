let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
var styles = [
    './node_modules/bootstrap/dist/css/bootstrap.min.css',
    './node_modules/font-awesome/css/font-awesome.min.css',
    './node_modules/jvectormap/jquery-jvectormap.css',
    './node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
    './node_modules/bootstrap-daterangepicker/daterangepicker.css',
    './node_modules/select2/dist/css/select2.min.css',
];


var scripts = [
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/bootstrap/dist/js/bootstrap.min.js',
    './node_modules/fastclick/lib/fastclick.js',
    './node_modules/jquery-sparkline/dist/jquery.sparkline.min.js',
    './node_modules/jquery-slimscroll/jquery.slimscroll.min.js',
    './node_modules/chart.js/Chart.js',
    './node_modules/select2/dist/js/select2.min.js',
];

mix.js('resources/assets/js/app.js', 'public/js')
        .sass('resources/assets/sass/app.scss', 'public/css');


mix.styles(styles, 'public/css/admin-app.css');
mix.scripts(scripts, 'public/js/admin-app.js')
